import json
from shutil import copy2
import os
import random

def q_(a):
    return "\"" + str(a) + "\"" 
    
def fname_(a):
    return q_(a) + ":"
  
def l_(a):
    return "[" + str(a) + "]"

def o_(a):
    return "{" + str(a) + "}"
 
def strval_(a):
    if isinstance(a, str):
        res=q_(a)
    else:
        res = str(a)
    return res
    
def nameval_(n,a):
    return fname_(n) + strval_(a)


def get_value(v):
        if (hasattr(v, 'get')):
            return v.get()
        elif isinstance(v, list):
            return "[" + ",".join([get_value(x) for x in v]) + "]"
        return strval_(v);

class Pair:
    def __init__(self,n,v):
        self.name = n
        self.value = v
        
    
        
    def get(self):
        #if (hasattr(self.value, 'get')):
        #    return q_(self.name) + ":" + self.value.get()
        #elif isinstance(self.value, list):
        #    return q_(self.name) + ":" + "[" + ",".join([x.get() for x in self.value]) + "]"
        return q_(self.name) + ":" + get_value(self.value)
        return nameval_(self.name,get_value(self.value))
        
    #def get(self):
    #    if (hasattr(self.value, 'get')):
    #        return q_(self.name) + ":" + self.value.get()
    #    elif isinstance(self.value, list):
    #        return q_(self.name) + ":" + "[" + ",".join([x.get() for x in self.value]) + "]"
    #    return nameval_(self.name,self.value)
        
class Object:
    def __init__(self):
        self.pairs = []
        
    def ins(self,n,v):
        for p in self.pairs:
            if p.name == n:
                p.value = v
                return
        self.pairs.append(Pair(n,v))
    def get_val(self,n):
        for p in self.pairs:
            if p.name == n:
                return p.value
        
        return None
        
    def get(self):
        return o_(",".join([x.get() for x in self.pairs]))

def make_dataset(name,meta):
    ds = Object()
    ds.ins("data_size",len(meta))
    ds.ins("height",0)
    ds.ins("width",0)
    ds.ins("name",name)
    ds.ins("source_path","/opt/www/shared_data/usecases/"+name+"/data_files/" + name)
    ds.ins("rel_source_path","data_files/" + name)
    ds.ins("mask_path","")
    ds.ins("source","image")
    ds.ins("meta",meta)
    return ds
    
def make_frame(frame,objects,w,h):
    fr = Object()
    fr.ins("frame",frame)
    fr.ins("mask","")
    fr.ins("height",h)
    fr.ins("width",w)
    fr.ins("objects",objects)
    return fr
    
def make_object(bb,o_class,pol):
    obj = Object()
    obj.ins("bb",bb)
    obj.ins("object_class",o_class)
    obj.ins("polygon",pol)
    return obj

def pop_random_elements(j_dataset,rate):

    number_to_keep=int(len(j_dataset['meta'])*rate)
    random.seed(0)
    random.shuffle(j_dataset['meta'])
    j_dataset['meta']=j_dataset['meta'][0:number_to_keep]
    for p in j_dataset['meta']:
        for obj in p["objects"]:
            obj["polygon"] = []
    print("keeping ",len(j_dataset['meta']))
    return len(j_dataset['meta'])
    
    
    

def merge_json_datasets_with_rate(j_dataset1,j_dataset2,target_rate_1,target_rate_2):
    print("merging with rates")
    ds1 = pop_random_elements(j_dataset1,target_rate_1)
    ds2 = pop_random_elements(j_dataset2,target_rate_2)
    print("merging with rates: res sizes ",ds1,ds2)
    for p in j_dataset2['meta']:
        j_dataset1['meta'].append(p)
    
    
    
    j_dataset1["data_size"] = ds1 + ds2
    return j_dataset1    
    
def merge_json_datasets(j_dataset1,j_dataset2):
    
    
    data_size2 = j_dataset2["data_size"]
    for p in j_dataset2['meta']:
        #print(p)
        for obj in p["objects"]:
            obj["polygon"] = []
        j_dataset1['meta'].append(p)
            
    data_size1 = j_dataset1["data_size"]
    j_dataset1["data_size"] = data_size1 + data_size2

    return j_dataset1
    
    
def merge_json_dataset_with_file(j_dataset,json_filename_to_integrate):
    
    res = None
    with open(json_filename_to_integrate) as json_file:
        res = merge_json_datasets(j_dataset,json.load(json_file))
        
    return res
    
def merge_two_dataset_json_files(j_dataset_file_1,j_dataset_file_2,res_file_name,target_rate_1=1.0,target_rate_2 = 1.0):
    
    res = None
    with open(j_dataset_file_1) as jf1, open(j_dataset_file_2) as jf2:
        if ((target_rate_1 == 1.0 and target_rate_2 == 1.0) 
            or target_rate_1 > 1.0 
            or target_rate_2 > 1.0
            or target_rate_1 <= 0.0 
            or target_rate_2 <= 0.0):
            res = merge_json_datasets(json.load(jf1),json.load(jf2))
        else:
            res = merge_json_datasets_with_rate(json.load(jf1),json.load(jf2),target_rate_1,target_rate_2)
            
        
    if res is not None and len(res_file_name) > 0:
        with open(res_file_name,"w+") as resf:
            json.dump(res,resf)
        


        
    
def prep_dataset_files(dsname,meta,used_images,to_merge_file): 
    
    dataset = make_dataset(dsname,meta)

    
    json_dir = dsname + "/data_files/json"
    img_files_dir = dsname + "/data_files/" + dsname
    os.makedirs(dsname, exist_ok=True)         
    os.makedirs(dsname + "/data_files", exist_ok=True)
    os.makedirs(json_dir, exist_ok=True)
    os.makedirs(img_files_dir, exist_ok=True)
    
    with open(json_dir + "/" + dsname + ".json" , "w") as jsonfile:
        if to_merge_file is not None and len(to_merge_file) > 0:
            json.dump(merge_json_dataset_with_file(json.loads(dataset.get()),"test_coco_city.json") ,jsonfile)
        else:    
            jsonfile.write(dataset.get())
        
    for imgfn in used_images:
        copy2(imgfn,img_files_dir)
        
    #print(dataset.get())     

    
def test2():
            
       
    objects = []
    objects.append(make_object([0,0,10,10],0,[]))
    objects.append(make_object([20,20,210,210],0,[]))
    
    
    meta = []
    meta.append(make_frame("fr1.jpg",objects,1280,720))
    
    dataset = make_dataset("synthv1",meta) 
    
    print(dataset.get())
    
def test1():
            
    obj1=Object()
    obj1.ins("bb",[0,0,10,10])            

    obj2=Object()
    obj2.ins("bb",[5,5,40,40])

    objects=[]
    objects.append(obj1)
    objects.append(obj2)
                
    frame1=Object()
    frame1.ins("frame","f1.jpg")
    frame1.ins("objects",objects)


    meta=[]
    meta.append(frame1)
    meta.append(frame1)
    dataset=Object()
    dataset.ins("source_path","/tmp")
    dataset.ins("meta",meta)
    print(dataset.get())
    