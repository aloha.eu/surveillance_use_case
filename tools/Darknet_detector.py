
from darknet import darknet
import cv2


class Darknet_detector:
    def __init__(self, confidence_threshold = 0.5):
        self.detector_type = "Darknet"
        self.network, self.class_names, self.class_colors = darknet.load_network(
        #"darknet/yolov4-leaky-416.cfg",
        #"darknet/coco.data",
        #"darknet/yolov4-leaky-416.weights",
            "darknet/yolov3.cfg",
            "darknet/coco.data",
            "darknet/yolov3.weights",
        1)
        self.width = darknet.network_width(self.network)
        self.height = darknet.network_height(self.network)
        self.darknet_image = darknet.make_image(self.width, self.height, 3)
        self.thresh=confidence_threshold



    def det_type(self):
        return self.detector_type

    def detect(self,image):
        #print("Darknet detect")
        image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image_resized = cv2.resize(image_rgb, (self.width, self.height),
                                   interpolation=cv2.INTER_LINEAR)
        darknet.copy_image_from_bytes(self.darknet_image, image_resized.tobytes())
        detections = darknet.detect_image(self.network,self.class_names, self.darknet_image,thresh=self.thresh,
                                          ressize=image.shape)
        #print("Darknet detect",detections,self.width)
        #image = darknet.draw_boxes(detections, image_resized, self.class_colors)
        #img = image.copy()
        dets = []
        for det in detections:
            if det[0] == "person":
                x0,y0,x1,y1 = darknet.bbox2points(det[2])
                dets.append((x0,y0,x1,y1,float(det[1])/100.))
            #cv2.rectangle(img,(x0,y0),(x1,y1),(0,255,255),2)

        #cv2.imshow("dn",img)

        return dets
