#!/usr/bin/python
import kp_utils
import cv2
import os
from utils.mAP.compute_mAP import *
import torch
import numpy as np
import pke_dataset_utils as pdu
import random

class Parameters:
    def __init__(self):
        do_dataset_from_images = False
        do_dataset_from_videos = False
        test_pose_estimation = False
        test_onnx = True
        if do_dataset_from_images:
            #dataset from synth files
            self.data_path = "/home/adriano/dev/aloha/synt_datasets/720p/"
            self.imgs_data_path = self.data_path
            slef.data_type = "image_files_dir"


            ##Parameters -------------------
            # self.detector_type = "Detectron2"
            # self.detector_type = "Darknet"
            self.detector_type = "No"

            # self.to_merge_file="test_coco_city.json"
            self.to_merge_file = None

            self.do_gt = False

            self.max_number_of_images = 5000
            self.max_number_of_images_in_dataset = 5000
            self.get_each_for_dataset = 1
            #self.dataset_to_create = "synthcropv1"
            # self.dataset_to_create = None
            self.dataset_from_gt = True
            self.display = 2

            self.calc_mAP = True

            self.use_crop = True

            self.do_anyway_each = -1
            ##Parameters -------------------
        elif do_dataset_from_videos:
            # dataset from video sequence
            self.data_path = []
            #self.data_path = ["/mnt/supernas/video/motion_vectors/dataset/10.146.3.207/Cam0",
            #                  "/mnt/supernas/video/motion_vectors/dataset/10.146.3.207/Cam3"]
            self.data_path = ["/home/adriano/dev/aloha/dataset_videos/"]
            if len(self.data_path) > 0:
                dirs = self.data_path
                self.data_path = []
                for dir in dirs:
                    if os.path.isdir(dir):
                        self.data_path += [os.path.join(dir, f) for f in os.listdir(dir)
                             if os.path.isfile(os.path.join(dir, f))  and
                             (f.endswith('.mp4') or f.endswith('264'))]
                print("data_path",self.data_path)
                print("data_path",len(self.data_path))

            else:
                self.data_path = []
                #self.data_path.append("/home/adriano/dev/deep_learn/pose_estimation/people_action.mp4")
                #self.data_path.append("/home/adriano/dev/deep_learn/videos/Technikereingang_Parkplatz_2019.10.02_Telefonat.mp4")
                #self.data_path.append("/mnt/supernas/video/JVA_Traunstein/2018-02-01_Positivtests/K06_Tagtest_2018-02-01_10%3A11_000.264")
                #self.data_path.append("/mnt/supernas/video/motion_vectors/dataset/10.146.3.207/Cam0/Technikereingang_2019.09.23_16#003A30#003A00-16#003A45#003A00.mp4")
                #self.data_path.append("/mnt/supernas/video/motion_vectors/dataset/10.146.3.207/Cam3/Technikereingang_Parkplatz_2019.10.16_06#003A50#003A00-07#003A30#003A00.mp4")
                self.data_path.append("/home/adriano/dev/aloha/dataset_videos/vegetation.mp4")


            self.imgs_data_path="tmp"
            self.data_type = "video"
            # self.detector_type = "Detectron2"
            #self.detector_type = "ONNX"
            self.detector_type = "Darknet"
            # self.detector_type = "No"

            # self.to_merge_file="test_coco_city.json"
            self.to_merge_file = None

            self.do_gt = False

            self.max_number_of_images = 1000000
            self.max_number_of_images_in_dataset = 5000
            self.get_each_for_dataset = 100

            self.dataset_to_create = "from_vids_2"
            # self.dataset_to_create = None
            self.dataset_from_gt = False
            self.display = 3
            self.use_crop = True
            self.calc_mAP = False
            self.do_anyway_each = 500
        elif test_onnx:
            # dataset from video sequence
            self.data_path = []
            #self.data_path = ["/mnt/supernas/video/motion_vectors/dataset/10.146.3.207/Cam0",
            #                  "/mnt/supernas/video/motion_vectors/dataset/10.146.3.207/Cam3"]
            #self.data_path = ["/home/adriano/dev/aloha/dataset_videos/"]
            if len(self.data_path) > 0:
                dirs = self.data_path
                self.data_path = []
                for dir in dirs:
                    if os.path.isdir(dir):
                        self.data_path += [os.path.join(dir, f) for f in os.listdir(dir)
                             if os.path.isfile(os.path.join(dir, f))  and
                             (f.endswith('.mp4') or f.endswith('264'))]
                print("data_path",self.data_path)
                print("data_path",len(self.data_path))

            else:
                self.data_path = []
                self.data_path.append("/home/adriano/dev/deep_learn/pose_estimation/people_action.mp4")
                #self.data_path.append("/home/adriano/dev/deep_learn/videos/Technikereingang_Parkplatz_2019.10.02_Telefonat.mp4")
                #self.data_path.append("/mnt/supernas/video/JVA_Traunstein/2018-02-01_Positivtests/K06_Tagtest_2018-02-01_10%3A11_000.264")
                #self.data_path.append("/mnt/supernas/video/motion_vectors/dataset/10.146.3.207/Cam0/Technikereingang_2019.09.23_16#003A30#003A00-16#003A45#003A00.mp4")
                #self.data_path.append("/mnt/supernas/video/motion_vectors/dataset/10.146.3.207/Cam3/Technikereingang_Parkplatz_2019.10.16_06#003A50#003A00-07#003A30#003A00.mp4")
                #self.data_path.append("/home/adriano/dev/aloha/dataset_videos/vegetation.mp4")


            self.imgs_data_path="tmp"
            self.data_type = "video"
            # self.detector_type = "Detectron2"
            self.detector_type = "ONNX"
            #self.detector_type = "Darknet"
            # self.detector_type = "No"

            # self.to_merge_file="test_coco_city.json"
            self.to_merge_file = None

            self.do_gt = False

            self.max_number_of_images = 1000000
            self.max_number_of_images_in_dataset = 5000
            self.get_each_for_dataset = 1

            self.dataset_to_create = None
            # self.dataset_to_create = None
            self.dataset_from_gt = False
            self.display = 2
            self.use_crop = True
            self.calc_mAP = False
            self.do_anyway_each = 1
        elif test_pose_estimation:
            # dataset from synth files
            self.data_path = "/home/adriano/dev/aloha/synt_datasets/720p/"
            self.imgs_data_path = self.data_path
            self.data_type = "image_files_dir"

            ##Parameters -------------------
            self.detector_type = "Detectron2"
            self.detection_type = "kp17"
            # self.detector_type = "Darknet"
            # self.detector_type = "No"

            # self.to_merge_file="test_coco_city.json"
            self.to_merge_file = None

            self.do_gt = False

            self.max_number_of_images = 5000
            self.max_number_of_images_in_dataset = 5000
            self.get_each_for_dataset = 1
            # self.dataset_to_create = "synthcropv1"
            self.dataset_to_create = None
            self.dataset_from_gt = True
            self.display = 1     

            self.calc_mAP = True

            self.use_crop = True

            self.do_anyway_each = -1
            ##Parameters -------------------
        else:
            quit(0)
pars = Parameters()

def main():
    print("Hello World!")
    # test_mAP()
    # quit(-1)
    pdu.test1()
    pdu.test2()

    #if False:
    #    data_path = "/home/adriano/dev/aloha/synt_datasets/720p"
    #    data_type = "image_files_dir"
    #else:
    #    data_path = ["/home/adriano/dev/deep_learn/pose_estimation/people_action.mp4",
    #                 "/mnt/supernas/video/JVA_Traunstein/2018-02-01_Positivtests/K06_Tagtest_2018-02-01_10%3A11_000.264"]
    #    data_type = "video"

    process_dir_of_images()



def test_mAP():       
    detst=[[(10,10,100,100,0),(50,50,100,100,0)],[(90,00,110,110,.6)]]
    dets_to_list_of_tensor_XYWH(detst,0.5)
    
    dets=[torch.FloatTensor([(0,0,100,100,0.4),(0,1,100,100,0.6)])]      
    # tensor([[0., 0., 0.],
    #         [0., 0., 0.]])
    one = torch.ones_like(dets[0][...,4:5])
    print("one",one)

    zero = torch.zeros_like(one)
    mt=torch.FloatTensor([(0,0,100,100,0.4),(0,1,100,100,0.6)])
    mt[...,4:5] = torch.where(mt[...,4:5] > 0.5, zero, one)
    print("mt",mt)
    XYXY_to_XYWH(mt)
    print("mt",mt)
    no_det_frame=torch.FloatTensor([])
    #print("mt",mt[zero > one])
    #print(one)
    #print(dets[0][:,4:5].where(one < zero,one))
    #print(torch.where(dets[0][:,4:5] > 0.5, zero, one))


    #gts=[[(0,0,100,100,0)]]
    #dets=np.array(dets)
    #gts=np.array(gts)

    #dets=torch.FloatTensor(dets)
    #gts=torch.FloatTensor(gts)
    #print(dets)
    #print("op",torch.where(dets > 0.5),1,0)
    #print(torch.where(dets[:,:,4:5] > 0)
    #print(dets)
    resmap=mAP_metric([mt,no_det_frame], [mt,no_det_frame], n_classes=2 , iouThreshold=0.5)
    print("mAP",resmap)

    



def get_bb_from_kps_instance(kps):
    subx = [sublist[0] for sublist in kps]
    suby = [sublist[1] for sublist in kps]
    x1 = min(subx)
    y1 = min(suby)            
    x2 = max(subx)
    y2 = max(suby)
    return [int(x1),int(y1),int(x2),int(y2)]

def draw_bounding_boxes(img,dets,color=(10,125,200)):
    for d in dets:
        p1=(int(d[0]),int(d[1]))
        p2=(int(d[2]),int(d[3]))
        cv2.rectangle(img,p1,p2,color,2)
        
def draw_bounding_boxes_from_kps(img,dets):
    
    for d in dets:
        bbxyxy = get_bb_from_kps_instance(d)
        cv2.rectangle(img,(bbxyxy[0],bbxyxy[1]),(bbxyxy[2],bbxyxy[3]),(10,125,200),2)  
        
def draw_dets(img,dets,detection_type):
    if detection_type == "kp17":
        kp_utils.draw_pose_key_points(img,dets)
        draw_bounding_boxes_from_kps(img,dets)
    elif detection_type == "bb":
        draw_bounding_boxes(img,dets)
    else:
        print("draw_dets: No detector of type",detection_type)
        quit(-1)  

def dets_to_objectlist(dets,detection_type):
    objects = []
    for d in dets:
        if (detection_type == "bb") and len(d) > 4:
            objects.append(pdu.make_object([d[0],d[1],d[2],d[3]],int(d[4]),[]))
        elif (detection_type == "kp17"):
            subx = [sublist[0] for sublist in d]
            suby = [sublist[1] for sublist in d]
            x1 = min(subx)
            y1 = min(suby)            
            x2 = max(subx)
            y2 = max(suby)
            objects.append(pdu.make_object(get_bb_from_kps_instance(d),0,[]))
        else:
            print("dets_type not known or not in format [x1,y1,x2,y2,cl] ",dets_type,d)
            
    return objects   

def cropobjects(objects,crop_w, crop_h,orig_img):
    crop_rect_xyxy = [1000000,1000000,0,0]
    
    for obj in objects:
        bb = obj.get_val("bb")
        #print("------------------",bb)
        crop_rect_xyxy[0] = min(crop_rect_xyxy[0],int(bb[0]))
        crop_rect_xyxy[1] = min(crop_rect_xyxy[1],int(bb[1]))
        crop_rect_xyxy[2] = max(crop_rect_xyxy[2],int(bb[2]))
        crop_rect_xyxy[3] = max(crop_rect_xyxy[3],int(bb[3]))
        #cv2.rectangle(orig_img,(bb[0],bb[1]),(bb[2],bb[3]),(0,255,255),2)
        
    if  crop_rect_xyxy[2] == 0:
        #print("cropobjects ret 1",crop_rect_xyxy)
        return None,None

    #cv2.rectangle(orig_img,(crop_rect_xyxy[0],crop_rect_xyxy[1]),(crop_rect_xyxy[2],crop_rect_xyxy[3]),(255,255,255),2)

    #if crop_rect_xyxy[2] - crop_rect_xyxy[0] + 1 < crop_w:
    #dif = int(crop_w - (crop_rect_xyxy[2] - crop_rect_xyxy[0]))
    #    #print("dif",dif)
    #    rand_d = random.randint(0,dif - 1)
    #    crop_rect_xyxy[0] = max(0,crop_rect_xyxy[0] - rand_d)
    #    crop_rect_xyxy[2] = crop_rect_xyxy[0] + crop_w - 1
    #if crop_rect_xyxy[3] - crop_rect_xyxy[1] + 1 < crop_h:
    #    dif = int(crop_h - (crop_rect_xyxy[3] - crop_rect_xyxy[1]))
    #    rand_d = random.randint(0,dif - 1)
    #    crop_rect_xyxy[1] = max(0,crop_rect_xyxy[1] - rand_d)
    #    crop_rect_xyxy[3] = crop_rect_xyxy[1] + crop_h - 1
    #print("crop_rect_xyxy",crop_rect_xyxy)
    if crop_rect_xyxy[2] - crop_rect_xyxy[0] + 1 < crop_w:
        lmin = max(0,crop_rect_xyxy[2] - crop_w + 1)
        lmax = min(crop_rect_xyxy[0],orig_img.shape[1] - crop_w - 1)
        #print("lims",lmin,lmax)
        if lmin >= lmax:
            return None,None
        coord_1 = random.randint(lmin,lmax)
        crop_rect_xyxy[0] = coord_1
        crop_rect_xyxy[2] = crop_rect_xyxy[0] + crop_w - 1

    if crop_rect_xyxy[3] - crop_rect_xyxy[1] + 1 < crop_h:
        lmin = max(0, crop_rect_xyxy[3] - crop_h + 1)
        lmax = min(crop_rect_xyxy[1], orig_img.shape[0] - crop_h - 1)
        if lmin >= lmax:
            return None,None
        #print("lims",lmin,lmax)
        coord_1 = random.randint(lmin, lmax)
        crop_rect_xyxy[1] = coord_1
        crop_rect_xyxy[3] = crop_rect_xyxy[1] + crop_h - 1
    
    #print("crop_rect_xyxy--------",crop_rect_xyxy)
    #cv2.rectangle(orig_img,(crop_rect_xyxy[0],crop_rect_xyxy[1]),(crop_rect_xyxy[2],crop_rect_xyxy[3]),(0,255,255),2)
    #cv2.imshow("img orit",orig_img)
    #cv2.waitKey(0)

    if (crop_rect_xyxy[0] < 0 or
        crop_rect_xyxy[1] < 0 or
        crop_rect_xyxy[2] >= orig_img.shape[1] or
        crop_rect_xyxy[3] >= orig_img.shape[0]):
        #print("cropobjects ret 2", crop_rect_xyxy)
        return None,None    
    for obj in objects:
        bb = obj.get_val("bb")
        bb[0] = bb[0] - crop_rect_xyxy[0]
        bb[1] = bb[1] - crop_rect_xyxy[1]
        bb[2] = bb[2] - crop_rect_xyxy[0]
        bb[3] = bb[3] - crop_rect_xyxy[1]
        if (bb[0] < 0 or
            bb[1] < 0 or
            bb[2] >= crop_w or
            bb[3] >= crop_h):
            #print("Fatal error, object does not fit corp rect",bb,crop_rect_xyxy)
            #quit(-1)
            #print("cropobjects ret 3", crop_rect_xyxy, bb)
            return None,None
        obj.ins("bb",bb)
        #print("--------------",bb)
        #cv2.rectangle(orig_img[crop_rect_xyxy[1]:crop_rect_xyxy[3] + 1,crop_rect_xyxy[0]:crop_rect_xyxy[2] +1],(bb[0],bb[1]),(bb[2],bb[3]),(0,255,0),2)
        
    #cv2.imshow("img crop",orig_img[crop_rect_xyxy[1]:crop_rect_xyxy[3] + 1,crop_rect_xyxy[0]:crop_rect_xyxy[2] +1])
    #cv2.waitKey(0)
    return objects,crop_rect_xyxy        
    
def get_detector(detector_type):
    
    dets_type = None
    detector = None
    if detector_type == "Detectron2":
        import Detectron2_detector
        detector = Detectron2_detector.Detectron2_detector()
        dets_type = "kp17"
        #dets_type = "bb"
    elif detector_type == "Darknet":
        import Darknet_detector
        detector = Darknet_detector.Darknet_detector(0.8)
        dets_type = "bb"
    elif detector_type == "ONNX":
        import ONNX_detector
        detector = ONNX_detector.ONNX_detector()
        dets_type = "bb"
    else:
        print("get_detector: No detector of type",detector_type)
    
    return dets_type,detector


def get_darknet_format_gts(f,width,height):
    gts = []
    base,_ = os.path.splitext(f)
    #os.rename(thisFile, base + ".txt")
    gtf = base + ".txt"
    #print(base)
    gt_bbs_cls = []
    with open(gtf) as fd:
        for line in fd:
            l=list(line.split(" "))
            if len(l) > 4:
                l=[float(x.strip()) for x in l]
                wh=l[3]/2.0
                hh=l[4]/2.0
                gt_bbs_cls.append([ int((l[1] - wh)*width) , int((l[2] - hh)*height) , 
                                int((l[1] + wh)*width) , int((l[2] + hh)*height), int(l[0]) ])
                
    
    return gt_bbs_cls

def XYXY_to_XYWH(d):
    if len(d) > 0:
        d[:,2:4] = d[:,2:4] - d[:,0:2] + torch.ones_like(d[:,0:2])
    

def dets_to_list_of_tensor_XYWH(all_dets_list, thr ):
    

    all_dets_list_tensor = []
    #print(all_dets_list)
    for d in all_dets_list:
        t = torch.FloatTensor(d)
        XYXY_to_XYWH(t)
        if thr >= 0:
            one = torch.ones_like(t[...,4:5])
            zero = torch.zeros_like(one)
            t[...,4:5] = torch.where(t[...,4:5] > thr, zero, one)
        all_dets_list_tensor.append(t)
    return all_dets_list_tensor



def detect(detector,detection_type,img):
    if detector.det_type() == "Detectron2":
        if detection_type == "kp17":
            dets=detector.detect(img)
            bbs = []
            for d in dets:
                bbxyxy = get_bb_from_kps_instance(d)
                bbs.append(get_bb_from_kps_instance(d) + [0.99]) #0.99 = conf of person
            return bbs,dets
        elif detection_type == "bb":
            dets = detector.detect(img,True)
            return dets,None
    elif detector.det_type() == "Darknet":
        return detector.detect(img),None
    elif detector.det_type() == "ONNX":
        return detector.detect(img),None
    else:
        print("detect: No detector of type",detector_type)
        quit(-1)

class image_iterator:
    def __init__(self,data_path,it_type):
        
        self.it_type = it_type
        self.idx = 0
        self.vl_idx = 0
        if it_type == "video":
            self.it = None
            if isinstance(data_path, list):
                self.data_path = data_path
            else:
                self.data_path = [data_path]
                
        elif it_type == "image_files_dir":
            self.data_path = data_path
            files = [f for f in os.listdir(data_path) 
                     if os.path.isfile(os.path.join(data_path, f)) and 
                     (f.endswith('.jpg') or f.endswith('.png'))]
            random.shuffle(files)         
            self.it = files
            
    def __iter__ (self):
        return self
    
    def open_cap(self,filename):
        print("opening",filename)
        self.it = cv2.VideoCapture(filename)
        if self.it.isOpened():
            self.idx = 0
            return True
        return False    
            
    def cap(self):
        if self.it is not None and self.it.isOpened():
            ret,img = self.it.read()
            if ret:
                self.idx += 1
                return img
        return None
        
    def get_file_name(self):
        return "img_" + str(self.vl_idx).zfill(3) + "_" + str(self.idx).zfill(6) + ".jpg"
    
    def __next__(self):
        #if self.it_type == "video":
        #    img = self.cap()
        #    if img is not None:
        #        return img,self.get_file_name()
        #el
        if self.it_type == "video":
            keep_trying = True
            while keep_trying:
                img = self.cap()
                if img is None and self.vl_idx < len(self.data_path):
                    self.open_cap(self.data_path[self.vl_idx])
                    self.vl_idx += 1
                else:
                    keep_trying = False
            if img is not None:
                return img,self.get_file_name()
            
        elif self.it_type == "image_files_dir" and self.idx < len(self.it):
            f = self.it[self.idx]
            img = cv2.imread(os.path.join(self.data_path, f))
            self.idx += 1
            return img,f
       
        raise StopIteration()
            
        
def process_dir_of_images():


    dets_type, detector = get_detector(pars.detector_type)
    used_images = []
    meta = []
    counter = 0
    counter_in_dataset = 0
    counter_in_dataset_run = 0
    k = 0

    
    all_dets_list = []
    all_gts_list = []
    tmp_dir="./tmp"
    if pars.data_type == "video":
        os.makedirs(tmp_dir, exist_ok=True)

    orig = None
    dets = None
    im_it = image_iterator(pars.data_path,pars.data_type)
    for img,f in im_it:
        #print("f --------",f)
        
        counter += 1
        counter_in_dataset_run += 1
        if counter%100 == 0:
            print(counter,f)
        
        #ground_truth
        if pars.do_gt:
            imgfn=pars.imgs_data_path+"/"+f
            gt_bbs_cls = get_darknet_format_gts(imgfn,img.shape[1],img.shape[0])
            if pars.calc_mAP:
                all_gts_list.append(gt_bbs_cls)
                
            if dataset_to_create is not None and pars.dataset_from_gt:
                
                if pars.use_crop:
                    croped_objects,crop_rect_xyxy=cropobjects(dets_to_objectlist(gt_bbs_cls,"bb"),512,512,img)
                    if croped_objects is not None:
                        f_c = "crop_" + str(crop_rect_xyxy[0]) + "_" +  str(crop_rect_xyxy[0]) + "_" + f
                        imgfn_c = tmp_dir + "/" + f_c
                        meta.append(pdu.make_frame(f_c,croped_objects,512,512))
                        cv2.imwrite(imgfn_c,img[crop_rect_xyxy[1]:crop_rect_xyxy[3] + 1,crop_rect_xyxy[0]:crop_rect_xyxy[2] +1])
                        used_images.append(imgfn_c)

                    else:
                        meta.append(pdu.make_frame(f,dets_to_objectlist(gt_bbs_cls,"bb"),img.shape[1],img.shape[0]))
                        used_images.append(imgfn)
                else:
                    meta.append(pdu.make_frame(f,dets_to_objectlist(gt_bbs_cls,"bb"),img.shape[1],img.shape[0]))
                    used_images.append(imgfn)
            
            draw_bounding_boxes(img,gt_bbs_cls,(0,255,0))

        #detection
        orig = []
        dets = []
        if detector is not None and counter_in_dataset_run >= pars.get_each_for_dataset:
            dets,orig = detect(detector,dets_type,img)

            if pars.dataset_to_create is not None and not pars.dataset_from_gt and len(dets) > 0:

                if pars.data_type == "video":
                    do_full_image = True
                    if pars.use_crop:
                        croped_objects, crop_rect_xyxy = cropobjects(dets_to_objectlist(dets, "bb"), 512, 512,                                                          img)
                        if croped_objects is not None:
                            f_c = "crop_" + str(crop_rect_xyxy[0]) + "_" + str(crop_rect_xyxy[0]) + "_" + f
                            imgfn_c = tmp_dir + "/" + f_c
                            meta.append(pdu.make_frame(f_c, croped_objects, 512, 512))
                            cv2.imwrite(imgfn_c, img[crop_rect_xyxy[1]:crop_rect_xyxy[3] + 1,
                                                 crop_rect_xyxy[0]:crop_rect_xyxy[2] + 1])
                            used_images.append(imgfn_c)
                            do_full_image = False

                    if do_full_image:
                        imgfn = tmp_dir + "/" + f
                        cv2.imwrite(imgfn,img)
                        meta.append(pdu.make_frame(f, dets_to_objectlist(dets, "bb"), img.shape[1], img.shape[0]))
                        used_images.append(imgfn)

                    #print("do_full_image",do_full_image)
                else:
                    meta.append(pdu.make_frame(f, dets_to_objectlist(dets, "bb"), img.shape[1], img.shape[0]))
                    imgfn=pars.imgs_data_path+"/"+f
                    cv2.imwrite(imgfn, img)
                    used_images.append(imgfn)

                counter_in_dataset += 1
                counter_in_dataset_run = 0
            elif pars.dataset_to_create is not None and pars.do_anyway_each > 0 and counter%pars.do_anyway_each == 0:
                meta.append(pdu.make_frame(f, [], img.shape[1], img.shape[0]))
                imgfn = pars.imgs_data_path + "/" + f
                cv2.imwrite(imgfn, img)
                used_images.append(imgfn)



            if pars.calc_mAP:
                all_dets_list.append(dets)
            

        if detector is not None and pars.display > 0 and (pars.display < 3 or counter%500 == 0):
            if orig is not None and len(orig) > 0:
                draw_dets(img,orig,pars.detection_type)
            elif dets is not None and len(dets) > 0:
                draw_bounding_boxes(img,dets)
            cv2.imshow("test",img)
            k = cv2.waitKey(pars.display - 1)
            print("k",k,counter)
        if k == 27 or counter >= pars.max_number_of_images or counter_in_dataset >= pars.max_number_of_images_in_dataset:
                break
        #print("k", k)

    if pars.calc_mAP:
        all_dets_list_tensor = dets_to_list_of_tensor_XYWH(all_dets_list,0.1)
        all_gts_list_tensor = dets_to_list_of_tensor_XYWH(all_gts_list,-1)
        print("all_dets_list_tensor",all_dets_list_tensor)
        print("mAP -- ",mAP_metric(all_dets_list_tensor, all_gts_list_tensor, n_classes=None , iouThreshold=0.3))
    
    #print(resmap)
    if pars.dataset_to_create is not None and len(meta) > 0:
        pdu.prep_dataset_files(pars.dataset_to_create,meta,used_images,pars.to_merge_file)
    




    

if __name__ == "__main__":
    main()