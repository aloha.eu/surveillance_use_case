import cv2
import numpy as np

def get_int_point(fkp):

    if len(fkp) > 0:
        return (int(fkp[0]),int(fkp[1]))
    return None


def iou(boxA, boxB):
    # determine the (x, y)-coordinates of the intersection rectangle
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])

    # compute the area of intersection rectangle
    interArea = abs(max((xB - xA, 0)) * max((yB - yA), 0))
    if interArea == 0:
        return 0
    # compute the area of both the prediction and ground-truth
    # rectangles
    boxAArea = abs((boxA[2] - boxA[0]) * (boxA[3] - boxA[1]))
    boxBArea = abs((boxB[2] - boxB[0]) * (boxB[3] - boxB[1]))

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = interArea / float(boxAArea + boxBArea - interArea)

    # return the intersection over union value
    return iou

def midpoint_float(p1,p2):
    return ((p1[0]+p2[0])/2.0,(p1[1]+p2[1])/2.0)

def distance_float(p1,p2):
    return np.linalg.norm(np.array(p1) - np.array(p2))


def distance_int(p1,p2):
    return int(distance_float(p1,p2))


def distance_ok(b1,b2):
    if len(b1) == 4 and len(b2) == 4:
        dist_b1b2 = distance_float((b1[0],b1[1]),(b2[0],b2[1]))
        diag_b1 = distance_float((b1[0],b1[1]),(b1[2],b1[3]))
        diag_b2 = distance_float((b2[0], b2[1]), (b2[2], b2[3]))
        if dist_b1b2 < 2*min(diag_b1,diag_b2):
            return True
    return False


def extrapolate_boxes(b1,b2,shape):
    if len(b1)  == 4 and len(b2) == 4:
        r = [max(b2[0] + (b2[0] - b1[0]),0),
                max(b2[1] + (b2[1] - b1[1]),0),
                min(b2[2] + (b2[2] - b1[2]),shape[1] - 1),
                min(b2[3] + (b2[3] - b1[3]),shape[0] -1)]
        if r[2] >= r[0] and r[3] >= r[1]:
            return r

    return None
def interpolate_boxes(b1,b2):
    if len(b1)  == 4 and len(b2) == 4:
        return [(b1[0] + b2[0])/2,
                (b1[1] + b2[1])/2,
                (b1[2] + b2[2])/2,
                (b1[3] + b2[3])/2]
    return None

def angle_deg(p1,p2):

    t = np.arctan2(p1[0].cpu().numpy() - p2[0].cpu().numpy(),p2[1].cpu().numpy() - p1[1].cpu().numpy())
    return np.degrees(t)


def int_mid_point(p1,p2):
    return (int((p1[0] + p2[0])/2),int((p1[1] + p2[1])/2))


def draw_ellipse(im, center, l1 , l2, kp0, kp1, ws = 21):


    angle = angle_deg(kp1,kp0)
    print("angle",angle)
    cv2.line(im,kp0,kp1,(255,255,63),2)
    cv2.ellipse(im, center, (l1,l2), angle, 0, 360, (0,255,0), 2)


def blurred_ellipse(im, center, l1 , l2, kp0, kp1):
    # create a temp image and a mask to work on
    tempImg = im.copy()
    maskShape = (im.shape[0], im.shape[1], 1)
    mask = np.full(maskShape, 0, dtype=np.uint8)
    # start the face loop

    rx0 = max(center[0] - l2,0)
    rx1 = min(center[0] + l2,im.shape[1] -1)
    ry0 = max(center[1] - l2,0)
    ry1 = min(center[1] + l2,im.shape[0] - 1)
    if l2 % 2 == 0:
        ws = l2 + 1
    else:
        ws = l2
    if rx1 >= rx0 and ry1 >= ry0:
        tempImg[ry0:ry1,rx0:rx1] = cv2.blur(tempImg[ry0:ry1,rx0:rx1], (ws, ws))
        cv2.ellipse(mask, center, (l1, l2), angle_deg(kp1,kp0), 0, 360, 255, -1)


        # oustide of the loop, apply the mask and save
        mask_inv = cv2.bitwise_not(mask)

        img1_bg = cv2.bitwise_and(im, im, mask=mask_inv)
        img2_fg = cv2.bitwise_and(tempImg, tempImg, mask=mask)


        return cv2.add(img1_bg, img2_fg)

    return tempImg



def scale_bb(bb_scale_factor,x0,y0,x1,y1):
    w = x1 - x0 + 1
    h = y1 - y0 + 1

    return int(x0 - w * (bb_scale_factor - 1.)),\
           int(y0 - h * (bb_scale_factor - 1.)),\
           int(x1 + w * (bb_scale_factor - 1.)),\
           int(y1 + h * (bb_scale_factor - 1.))

def pixelate(im, x0, y0, x1, y1):

    x0 = min(max(0, x0), im.shape[1] - 1)
    y0 = min(max(0, y0), im.shape[0] - 1)
    x1 = min(max(x0, x1), im.shape[1] - 1)
    y1 = min(max(y0, y1), im.shape[0] - 1)

    if y1 >= y0 and x1 >= x0:
        s = im[y0: y1, x0:x1].shape
        im_mod = cv2.resize(im[y0: y1, x0:x1], (0, 0), fx=0.1, fy=0.1, interpolation=cv2.INTER_LINEAR)
        im_mod1 = cv2.resize(im_mod, (s[1], s[0]), interpolation=cv2.INTER_NEAREST)
        im[y0: y1, x0:x1] = im_mod1


def pixelate_blur(im, x0, y0, x1, y1,ws = 21):

    x0 = min(max(0,x0),im.shape[1] - 1)
    y0 = min(max(0,y0),im.shape[0] - 1)
    x1 = min(max(x0,x1),im.shape[1] - 1)
    y1 = min(max(y0,y1),im.shape[0] - 1)
    if y1 >= y0 and x1 >= x0:
        im_mod1 = cv2.GaussianBlur(im[y0: y1 , x0:x1 ], (ws, ws), 0)
        im[y0: y1, x0:x1] = im_mod1

