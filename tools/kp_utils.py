import numpy as np
import cv2
import a_utils

COCO_PERSON_KEYPOINT_NAMES = (
    "nose",
    "left_eye", "right_eye",
    "left_ear", "right_ear",
    "left_shoulder", "right_shoulder",
    "left_elbow", "right_elbow",
    "left_wrist", "right_wrist",
    "left_hip", "right_hip",
    "left_knee", "right_knee",
    "left_ankle", "right_ankle",
)

kp_17_lines = [
    # face
    (3, 1),
    (4, 2),
    (1, 0),
    (0, 2),
    # face -> upper-body
    (0, 0, 5, 6),
    # upper-body
    (5, 6),
    (5, 7),
    (6, 8),
    (7, 9),
    (8, 10),
    # upper-body -> lower-body
    (5, 6, 11, 12),
    # lower-body
    (11, 12),
    (11, 13),
    (12, 14),
    (13, 15),
    (14, 16),

]

def draw_angles_between_average_lists(image,instance_key_points, l1, l2,ang_th = 45):
    avr_group1 = average_list(instance_key_points, l1, 0.05)
    avr_group2 = average_list(instance_key_points, l2, 0.05)
    #print("avr_group1",avr_group1)
    #print("avr_group2",avr_group2)
    angle = angle_deg(avr_group1,avr_group2)

    det = False
    avr_conf = 0
    if angle is not None:
        avr_conf = avr([avr_group1[2], avr_group2[2]])
        if abs(angle) > ang_th and avr_conf > 0.10:
            color = (0, 0, 255)
            det = True
            cv2.rectangle(image,a_utils.get_int_point(avr_group1),a_utils.get_int_point(avr_group2),(0,0,255),4)
        else:
            color = (0, 255, 255)
        cv2.putText(image, str(int(angle)) + " " + str(int(100*(avr_conf))),
                (int(avr_group1[0]),int(avr_group1[1])), cv2.FONT_HERSHEY_PLAIN, 2,
                color, 2)

    return det,avr_conf

def draw_torso_angle(image,instance_key_points):
    shoulder_list =  [5,6]
    hip_list = [11,12]
    return draw_angles_between_average_lists(image,instance_key_points,shoulder_list,hip_list,50)


def draw_thighs_angle(image,instance_key_points):

    d1, a1 = draw_angles_between_average_lists(image,instance_key_points,[11],[13],90)
    d2, a2 = draw_angles_between_average_lists(image,instance_key_points,[12],[14],90)
    return d1 or d2,avr([a1,a2])


def draw_ankle_hip_angle(image,instance_key_points):

    d1, a1 = draw_angles_between_average_lists(image, instance_key_points, [11], [15],90)
    d2, a2 = draw_angles_between_average_lists(image, instance_key_points, [12], [16],90)
    return d1 or d2,avr([a1,a2])


def draw_pose_instance_key_points(image,instance_key_points,lines = kp_17_lines, clrs = None, idx = None):

    if len(instance_key_points) == 0:
        return
    ls = 2
    rad = 2
    kp_th = 0.05
    for i, kp in enumerate(instance_key_points):
        if kp[2] > kp_th:
            cv2.circle(image, a_utils.get_int_point(kp), ls + 1, (255, 0, 0), rad)
            # cv2.putText(image, str(i),
            #            a_utils.get_int_point(kp),cv2.FONT_HERSHEY_PLAIN, 1,
            #            (0, 255, 255), 2)
    if idx is not None:
        avrall = average_list(instance_key_points,list(range(len(instance_key_points))),0.05)
        if avrall is not None:
            cv2.putText(image, str(idx),
                    (int(avrall[0]), int(avrall[1])), cv2.FONT_HERSHEY_PLAIN, 2,
                    (100,230,50), 2)

    for i, lnp in enumerate(lines):
        if instance_key_points[lnp[0]][2] > kp_th and instance_key_points[lnp[1]][2] > kp_th:
            if len(lnp) == 2:
                cv2.line(image,
                         a_utils.get_int_point(instance_key_points[lnp[0]]),
                         a_utils.get_int_point(instance_key_points[lnp[1]]),
                         clrs[lnp[0]], ls)
            elif len(lnp) == 4 and instance_key_points[lnp[2]][2] > kp_th and \
                    instance_key_points[lnp[3]][2] > kp_th:

                cv2.line(image,
                         a_utils.get_int_point(a_utils.midpoint_float(instance_key_points[lnp[0]],
                                                                      instance_key_points[lnp[1]])),
                         a_utils.get_int_point(a_utils.midpoint_float(instance_key_points[lnp[2]],
                                                                      instance_key_points[lnp[3]])),
                         clrs[lnp[0]], 4)
    d1,_ = draw_torso_angle(image, instance_key_points)
    d2,_ = draw_thighs_angle(image, instance_key_points)
    d3,_ = draw_ankle_hip_angle(image, instance_key_points)
    return d1

def draw_pose_key_points(image,pose_key_points,lines = kp_17_lines, clrs = None):

    if clrs is None:
        max_l = 0
        for a in pose_key_points:
            max_l = max(len(a),max_l)
        clrs = [(0,165,255)]*max_l

    cv2.putText(image, str(len(pose_key_points)),
               (20,20),cv2.FONT_HERSHEY_PLAIN, 1,
                (0, 255, 255), 2)
    for instance_key_points in pose_key_points:
        draw_pose_instance_key_points(image, instance_key_points, lines, clrs)

def draw_pose_17_key_points_avr_groups(image,pose_17_key_points):
    average_lists = [[0,1,2,3,4],[5,6,7,8,9,10],[11,12,13,14,15,16]]
    group_kps = []
    for instance_kp in pose_17_key_points:
        instance_group_kps = []
        for l in average_lists:
            r = average_list(instance_kp,l,0.05)
            if r is not None:
                instance_group_kps.append(r)
            else:
                instance_group_kps.append([0.,0.,-1.])

        group_kps.append(instance_group_kps)


    draw_pose_key_points(image_proc,group_kps,[(0, 1),(1,2)])




def average_list(instance_key_points,avr_idx_list,conf_thr):

    x_acc = 0.
    y_acc = 0.
    conf_acc = 0.
    count = 0.

    for i in avr_idx_list:
        if len(instance_key_points) > i:
            if instance_key_points[i][2] >= conf_thr:
                x_acc += instance_key_points[i][0]
                y_acc += instance_key_points[i][1]
                conf_acc += instance_key_points[i][2]
                count += 1.
        else:
            print("average_list error:",i,len(instance_key_points))

    if count > 0.:
        return [x_acc/count, y_acc/count, conf_acc/count]

    return None


def test(p):
    if p is not None and len(p) == 3:
        return True
    return False

def f1_score(a,b):
    sum = a + b
    if sum > 0:
        return 2.0*(a*b)/sum
    return 0.


def higher(p1,p2):
    if test(p1) and test(p2):
        if p1[1] > p2[1]:
            return True, f1_score(p1[2],p2[2])
        return False, 0.

    print("higher error: len(p1) or len(p2) not 3")
    quit(-1)


def any_higher(l1,l2):
    max_score = 0.
    for p1 in l1:
        for p2 in l2:
            res,score = higher(p1,p2)
            if res:
                max_score = max(max_score, score)

    if max_score > 0.:
        return True,max_score

    return False, 0.


def avr(l):
    acc = 0.
    c = 0.
    for v in l:
        acc += v
        c += 1.
    if c > 0:
        return acc/c
    return 0.

def angle_deg(p1,p2):
    if test(p1) and test(p2):
        return a_utils.angle_deg(p1,p2)
    return None