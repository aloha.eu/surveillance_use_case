import onnxruntime as ort
import torch
import torch.nn as nn
import cv2
import numpy as np
import torchvision


class ONNX_detector:
    def __init__(self):
        self.detector_type = "ONNX"
        self.sess = ort.InferenceSession('onnx_aloha_200epoch_25000.onnx')
        self.input_name = self.sess.get_inputs()[0].name
        self.label_name = self.sess.get_outputs()[0].name
        self.net_width = 416
        self.net_height = 416
        self.im_norm = 2
        self.conf_thr2 = 0.3
        self.conf_thr1 = 0.3
        self.nms_thr = 0.3
        self.anchors = [1.3221,1.73145,  3.19275,4.00944  ,5.05587,8.09892 , 9.47112,4.84053 , 11.2364,10.0071]
        self.anchors = [0.57273, 0.677385, 1.87446, 2.06253, 3.33843, 5.47434, 7.88282, 3.52778, 9.77052, 9.16828]
        #self.anchors = [1.08, 1.19, 3.42, 4.41, 6.63, 11.38, 9.42, 5.11, 16.62, 10.52]
        self.inf_img = np.zeros((self.net_height,self.net_width,3), np.uint8);
        self.inf_img[::] = 127
        self.img_width = 0
        self.img_height = 0
        self.off_w = 0
        self.off_h = 0
        self.scale_factor = 1.0
        self.scale_array = [1.0 / self.scale_factor, 1.0 / self.scale_factor, 1.0 / self.scale_factor, 1.0 / self.scale_factor, 1.0, 1.0]


    def to_numpy(self,tensor):
        return tensor.detach().cpu().numpy() if tensor.requires_grad else tensor.cpu().numpy()

    def sigmoid(self,x):
        return 1 / (1 + np.e ** -x)

    def softmax(self,x):
        expvs = np.exp(x - np.max(x))
        return expvs / expvs.sum()

    def load_checkpoint(self,filepath):
        checkpoint = torch.load(filepath)
        model = checkpoint['model']
        model.load_state_dict(checkpoint['state_dict'])
        for parameter in model.parameters():
            parameter.requires_grad = False

        model.eval()
        return model

    def inference(self, in_arr_orig):

        in_arr = in_arr_orig.transpose([2, 0, 1])
        in_arr = np.expand_dims(in_arr, axis=0)
        in_arr = in_arr.astype(np.float32)

        if self.im_norm == 0:
            in_arr = in_arr - 127.0
            in_arr /= 127.0
        elif self.im_norm == 1:
            in_arr /= 127.0
        elif self.im_norm == 2:
            # in_arr = in_arr - 127.0
            in_arr /= 256.0


        # with torch.no_grad():
        #   out_ptr=model(in_arr)
        result = self.sess.run(None, {self.input_name: in_arr})
        result = result[0]
        ###print(result.shape)
        result = result.transpose([0, 2, 3, 1])
        #print(result.shape)
        result_view = result.view()
        result_view.shape = 1, 13, 13, 5, int(result.shape[3] / 5)
        #print(result.shape)
        #print(result_view.shape)

        bs = self.net_width / 13;
        boxes = np.zeros((0, 6))
        scores = np.zeros((0, 1))

        for igx, gx in zip(range(len(result_view[0])), result_view[0]):
            for igy, gy in zip(range(len(gx)), gx):
                anch = 0
                for bi in gy:
                    sigval = self.sigmoid(bi[4])
                    if sigval > self.conf_thr1:
                        sofv = self.softmax(bi[5:])
                        ibc = np.argmax(sofv)
                        score = sofv[ibc] * sigval
                        if score > self.conf_thr2:
                            w = np.exp(bi[2]) * self.anchors[2 * anch] * bs
                            h = np.exp(bi[3]) * self.anchors[2 * anch + 1] * bs
                            x = (igy + self.sigmoid(bi[0])) * bs - w / 2
                            y = (igx + self.sigmoid(bi[1])) * bs - h / 2
                            boxes = np.vstack((boxes, [x, y, x + w, y + h,score,ibc]))
                            scores = np.vstack((scores, score))
                            box = (x, y, w, h)
                            # print("box",type(box))
                            # print("box",box)

                            # print("lable ", labels[ibc])
                            # cv2.rectangle(in_arr_orig,(int(x),int(y),int(w),int(h)),(0,255,0))
                    anch += 1

        # print("boxes",boxes)
        # print("scores",scores)
        nms_out = torchvision.ops.boxes.nms(torch.tensor(boxes), torch.tensor(scores), self.nms_thr)
        print("nms_out", nms_out)
        dets = []
        for idx in nms_out:
            box = boxes[idx]
            cv2.rectangle(in_arr_orig, (int(box[0]), int(box[1])), (int(box[2]), int(box[3])), (0, 255, 0))
            dets.append((boxes[idx] - [0,self.off_h,0,self.off_h,0,0]) *self.scale_array)

        cv2.imshow("testdi", in_arr_orig)
        # cv2.imwrite("coco_s_out.png",in_arr_orig)
        # return cv2.waitKey()
        return dets
    def det_type(self):
        return self.detector_type

    def reset(self,h,w):
        self.img_width = w
        self.img_height = h
        self.scale_factor = min(self.net_width / w, self.net_height / h)
        w *= self.scale_factor
        h *= self.scale_factor
        self.inf_img[::] = 127
        self.off_w = int((self.net_width - w)/2)
        self.off_h = int((self.net_height - h)/2)
        print(h,w)
        if self.off_h > self.off_w:
            self.off_w = 0
        else:
            self.off_h = 0

        self.scale_array = [1.0 / self.scale_factor, 1.0 / self.scale_factor, 1.0 / self.scale_factor, 1.0 / self.scale_factor, 1.0, 1.0]


    def detect(self,image):
        if True:

            img = image.copy()
            if self.img_width != img.shape[1] or self.img_height != img.shape[0]:
                self.reset(img.shape[0],img.shape[1])
            img = cv2.resize(img, None, fx=self.scale_factor, fy=self.scale_factor)
            h, w, c = img.shape

            self.inf_img[self.off_h:h + self.off_h, self.off_w :w + self.off_w] = img.copy()

        else:
            # h,w,c = img.shape
            # inf_img[50:h+50, 0:w] = img
            self.inf_img = image[cy:cy + self.net_height, cx:cx + self.net_width].copy()
        dets = self.inference(self.inf_img)
        print(dets)
        return dets
