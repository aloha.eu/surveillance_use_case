#!/usr/bin/python
import sys
import pke_dataset_utils as pdu



    

def main():

    argc=len(sys.argv)
    print(argc)
    if argc == 4:
        pdu.merge_two_dataset_json_files(sys.argv[1],sys.argv[2],sys.argv[3])
    elif argc == 6:
        pdu.merge_two_dataset_json_files(sys.argv[1],sys.argv[2],sys.argv[3],float(sys.argv[4]),float(sys.argv[5]))
    else:
        print("usage python merger.py f1.json f2.json fres.json [ r1 r2 ])")
        print("example: python merger.py  test_coco_city.json test_coco_city.json mergres.json")
        print("example: python merger.py  test_coco_city.json test_coco_city.json mergres.json 0.8 0.5")
    

if __name__ == "__main__":
    main()


    
