
import json
import cv2
import os

jsonfile="synthcropv1.json"
imagespath="synthcropv1_wrong_width_height/data_files/synthcropv1/"

jsonfile="../pke_datasets/citycrops/citycropv1/data_files/json/citycropv1.json"
imagespath="../pke_datasets/citycrops/citycropv1/data_files/citycropv1"

jsonfile="../aloha_toolflow/shared_data/usecases/citycropv1/data_files/json/synthcropv1.json"
imagespath="../aloha_toolflow/shared_data/usecases/citycropv1/data_files/citycropv1/"

jsonfile="from_vids/data_files/json/from_vids.json"
imagespath="from_vids/data_files/from_vids/"


jsonfile="coco_10000.json"
imagespath="../aloha_toolflow/shared_data/usecases/citycropv1/data_files/citycropv1/"


#with open("synthcropv1.json") as inpf:
with open(jsonfile) as inpf:

    dataset=json.load(inpf)

    for idx,p in enumerate(dataset['meta']):
        imgfn=p["frame"]
        print("imgfn",imgfn)
        print("w",p["width"])
        print("h",p["height"])
        print(imagespath + "/" + imgfn)
        img = cv2.imread(imagespath + "/" + imgfn)

        for obj in p['objects']:
            bb = obj['bb']
            print(bb)
            cv2.rectangle(img,(int(bb[0]),int(bb[1])),(int(bb[2]),int(bb[3])),(0,150,200),1)
        
        cv2.imshow("img",img)
        key = cv2.waitKey(0)        
        if key == 27:
            quit(-1)
        elif key == ord('s'):
            cv2.imwrite(os.path.basename(jsonfile) + "_" + str(idx) + ".jpg",img)
            
