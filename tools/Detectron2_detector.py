from detectron2.engine.defaults import DefaultPredictor
from detectron2.config import get_cfg
import a_utils
import numpy as np

COCO_PERSON_KEYPOINT_NAMES = (
    0,
    1, 2,
    3, 4,
    5, 6,
    7, 8,
    9, 10,
    11, 12,
    13, 14,
    15, 16,
)
lines = [
    # face
    (3, 1),
    (4, 2),
    (1, 0),
    (0, 2),
    # face -> upper-body
    (0, 0, 5, 6),
    # upper-body
    (5, 6),
    (5, 7),
    (6, 8),
    (7, 9),
    (8, 10),
    # upper-body -> lower-body
    (5, 6, 11, 12),
    # lower-body
    (11, 12),
    (11, 13),
    (12, 14),
    (13, 15),
    (14, 16),

]

lines_=[(1,8),
(1,2),
(1,5),
(2,3),
(3,4),
(5,6),
(6,7),
(8,9),
(9,10),
(10,11),
(8,12),
(12,13),
(13,14),
(1,0),
(0,15),
(15,16)
]

pose_colors = [(85,0,255),
(0,0,255),
(0,85,255),
(0,170,255),
(0,255,255),
(0,255,170),
(0,255,85),
(0,255,0),
(0,0,255),
(85,255,0),
(170,255,0),
(255,255,0),
(255,170,0),
(255,85,0),
(255,0,0),
(170,0,255),
(255,0,170)]


class Detectron2_detector:
    def __init__(self):
        self.detector_type = "Detectron2"
        cfg = get_cfg()
        cfg.merge_from_file("keypoint_rcnn_R_50_FPN_3x.yaml")
        #cfg.merge_from_list([MODEL.WEIGHTS,"model_final_a6e10b.pkl"])
        print(cfg)
        self.predictor = DefaultPredictor(cfg)
        self.conf_threshold = 0.7



    def det_type(self):
        return self.detector_type

    def detect(self,image,get_boxes=False):

        predictions = self.predictor(image)
        instances = predictions["instances"]

        # predictions = self.predictor(np.array([image,image]))
        # print("scores",scores)
        # print("pred_classes", pred_classes)

        # print("boxes",boxes)
        # print("keypoints",keypoints)
        # print("test"
        # pred_classes = instances.pred_classes if instances.has("pred_classes") else None

        scores = instances.scores if instances.has("scores") else None
        res = []

        if get_boxes:
            boxes = instances.pred_boxes if instances.has("pred_boxes") else None
            pred_classes = instances.pred_classes if instances.has("pred_classes") else None
            #print("boxes", boxes)
            #print("boxes[0].tensor", boxes[0].tensor[0])
            for i,score in enumerate(scores):
                if score > self.conf_threshold:

                    res.append(np.concatenate((boxes[i].tensor[0].cpu().detach().numpy(),np.array([pred_classes.cpu().detach().numpy()[i]])),axis=0))
        else:
            keypoints = instances.pred_keypoints if instances.has("pred_keypoints") else []
            for i,score in enumerate(scores):
                if score > self.conf_threshold:
                    res.append(keypoints[i])

        #print("res",res)
        return res
