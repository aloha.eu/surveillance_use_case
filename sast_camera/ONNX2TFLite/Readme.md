# Converting ONNX yolo networks to tflite

## Setup

The recommended way for a work environment is to use VS Code Dev Containers using the provided configuration.
Else install all requirements from the `requirements.txt` with pip using:

```
pip3 -r requirements.txt
```

To download all assets execute

```
./setup.sh
```

## Converting YOLO networks

1. Put all onnx files into the *assets* folder.
2. Run `make all` to convert all onnx files to tf and tflite
