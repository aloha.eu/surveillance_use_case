#!/usr/bin/env python3

import tensorflow as tf
import argparse
import sys
from os import listdir
from os.path import isfile, join
import cv2
import os

def representative_dataset():
  baseFolder = 'assets/VOCdevkit/VOC2007/JPEGImages/'
  fimage = [f for f in listdir(baseFolder) if isfile(join(baseFolder, f))]

  # Only do first 50
  for input_value in range(50):
    if os.path.exists(fimage[input_value]):
      img = Image.open(args.image).resize((width, height))

      tmp = np.array(img)
      tmp = np.transpose(tmp, [2, 0, 1])
      print(tmp.shape)

      # add N dim
      input_data = np.float32(np.expand_dims(tmp, axis=0))
      print(input_data.shape)
      yield input_data
    else:
      continue

args = sys.argv[1:]
parser = argparse.ArgumentParser()
parser.add_argument("-i", dest="input")
parser.add_argument("-o", dest="output")
parser.add_argument("--quant", dest="quant", action="store_true")
args = parser.parse_args()

# Convert the model
print("==== Create converter ====")
converter = tf.lite.TFLiteConverter.from_saved_model(args.input) # path to the SavedModel directory
print("==== Configure converter ====")

if args.quant:
  converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS, tf.lite.OpsSet.SELECT_TF_OPS]
  converter.optimizations = [ tf.lite.Optimize.DEFAULT ]
  converter.inference_input_type = tf.int8  # or tf.uint8
  converter.inference_output_type = tf.int8  # or tf.uint8
  converter.representative_dataset = representative_dataset

else:
  converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS,
                                         tf.lite.OpsSet.SELECT_TF_OPS]
  converter.optimizations = [ tf.lite.Optimize.DEFAULT ]

print("==== Do conversion ====")
tflite_model = converter.convert()

print("==== Saving file as tflite ====")

# Save the model.
with open(args.output, 'wb') as f:
  f.write(tflite_model)
