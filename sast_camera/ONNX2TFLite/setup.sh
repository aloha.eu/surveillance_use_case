#!/bin/bash

# Download assets
pushd assets

# Download YOLOv2 (Source: https://github.com/onnx/models/tree/master/vision/object_detection_segmentation/yolov2-coco)
wget https://github.com/onnx/models/raw/master/vision/object_detection_segmentation/yolov2-coco/model/yolov2-coco-9.onnx

# Download Tiny YOLOv2 (Source: https://github.com/onnx/models/raw/master/vision/object_detection_segmentation/tiny-yolov2/model/tinyyolov2-7.onnx)
wget https://github.com/onnx/models/raw/master/vision/object_detection_segmentation/tiny-yolov2/model/tinyyolov2-7.onnx

popd
