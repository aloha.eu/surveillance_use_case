# README

This is the `MessageBroker` example that demonstrates how to use the MessageBroker API to publish/receive messages to/from other apps. It also shows how to consume these messages in VMS. 

This example contains two apps: 
1. `TFliteDetector` -  is a publisher app that publishes information (bounding boxes, labels, confidence, etc) about the objects that are detected in a frame.
2. `HelloWorld` - is a subscriber app that listens to messages published from TFLiteDetector. From the received message, it deduces the number of objects and displays it in the Web UI.

If a VMS is setup, the VMS can be configured to receive and display the metadata from the TFLiteDetector. 

This document assumes that [OpenJDK 8 is installed](https://openjdk.java.net/install/).

#### Tell gradle to use Security and Safety Things SDK

 Set the following environment variables: 
 
    export ANDROID_SDK_ROOT=<path_to_securityandsafetythingssdk>
    export ANDROID_NDK_HOME=<path_to_securityandsafetythingssdk>/ndk-bundle

#### Set up Security and Safety Things maven credentials

Create a `gradle.properties` file populated with your [developer portal](https://docs.securityandsafetythings.com/) credentials as follows:
```
sstUsername=John.Doe@example.com
sstPassword=MySecurePassword
```
Place this file in your home directory `.gradle` folder.
- On Linux, put this file under `~/.gradle/gradle.properties`
- On Windows, put this file under `%USERPROFILE%/.gradle/gradle.properties`.

### Building the MessageBroker app

From the terminal, run:

    ./gradlew assembleDebug

### Install and run on Device

 1. Obtain the generated APK files from: 
 
        ./helloworld/build/outputs/apk/debug/helloworld-debug.apk
        ./tflitedetector/build/outputs/apk/debug/tflitedetector-debug.apk
               
 2. Install the apk files on a Security and Safety Things Device using ADB
      
        adb install -r -g ./helloworld/build/outputs/apk/debug/helloworld-debug.apk
        adb install -r -g ./tflitedetector/build/outputs/apk/debug/tflitedetector-debug.apk
        
 3. From a web browser visit `https://<ip_address_of_camera>:8443`       
 4. Go to `Applications -> Overview` 
 5. `TFLite Detector (Publisher) -> Go to application website`
 6. Verify the video is being streamed and objects are being detected in the `TFlitedetector` app.
 7. Go to `Applications -> Overview` 
 8. `HelloWorld (Subscriber) -> Go to application website`
 9. Verify the number of objects being detected are displayed in the `HelloWorld` app.

### Generating a release APK

In order to generate the release APK, be sure to have your signing configuration setup as follows:

 1. The following environment variables must be defined, for example:

        export SIGNING_KEY_ALIAS=keyalias # Choose any name for the signing key
        export SIGNING_KEY_PASSWORD=signingkeypassword # Choose a password for the signing key
        export SIGNING_KEYSTORE_PATH=~/key_name.keystore # Choose a path to store your keystore
        export SIGNING_KEYSTORE_PASSWORD=keystorepassword # Choose a password for the keystore

 2. Run the following command to generate the keystore:

        keytool -genkey -v -keystore $SIGNING_KEYSTORE_PATH \
        -alias $SIGNING_KEY_ALIAS -keyalg RSA \-keysize 2048 \
        -validity 10000 -storepass $SIGNING_KEYSTORE_PASSWORD \
        -keypass $SIGNING_KEY_PASSWORD

    For more information on `keytool`, please refer to `man keytool` or [Oracle's keytool documentation](https://docs.oracle.com/javase/8/docs/technotes/tools/unix/keytool.html)

 3.  From the terminal run:

         ./gradlew assembleRelease

### Note:

 If the gradle sync does not work properly in IntelliJ IDEA and gives the following error:

    ERROR: The modules ['tflitedetector-webapp', 'tflitedetector.webapp'] point to the same directory in the file system.
    ERROR: The modules ['helloworld-webapp', 'helloworld.webapp'] point to the same directory in the file system.
    Each module must have a unique path.

 Please try the following steps:
 
  1. Delete the `helloworld-webapp.iml` and `helloworld.webapp.iml` files from the `webapp` directory within `helloworld`. 
  2. Delete the `tflitedetector-webapp.iml` and `tflitedetector.webapp.iml` files from the `webapp` directory within `tflitedetector`.
  3. Try re-syncing gradle again.