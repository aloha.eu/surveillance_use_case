/*
 * Copyright 2019-2020 by Security and Safety Things GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import static groovy.io.FileType.FILES

plugins {
    id "com.github.node-gradle.node" version "3.1.0"
}

node {
    /**
     * Version of node to use.
     */
    version = '14.8.0'

    /**
     * Version of Yarn to use.
     */
    yarnVersion = '1.22.4'

    /**
     * Base URL for fetching node distributions (change if you have a mirror).
     */
    distBaseUrl = 'https://nodejs.org/dist'

    /**
     * If true, it will download node using above parameters.
     * If false, it will try to use globally installed node.
     */
    download = true

    /**
     * Set the work directory for unpacking node
     */
    workDir = file("${project.buildDir}/nodejs")

    /**
     * Set the work directory for NPM
     */
    npmWorkDir = file("${project.buildDir}/npm")

    /**
     * Set the work directory for Yarn
     */
    yarnWorkDir = file("${project.buildDir}/yarn")

    /**
     * Set the work directory where node_modules should be located
     */
    nodeModulesDir = file("${project.projectDir}")
}


task copyNode(type: Copy) {
    if (org.gradle.internal.os.OperatingSystem.current().isLinux()) {
        from "${node.workDir}/node-v${node.version}-linux-x64/"
        into "${node.workDir}/node/"
    } else if (org.gradle.internal.os.OperatingSystem.current().isMacOsX()) {
        from "${node.workDir}/node-v${node.version}-darwin-x64/"
        into "${node.workDir}/node/"
    } else if (org.gradle.internal.os.OperatingSystem.current().isWindows()) {
        from "${node.workDir}/node-v${node.version}-win-x64/"
        into "${node.workDir}/node/"
    }
}

task copyYarn(type: Copy) {
    from file("${node.yarnWorkDir}/yarn-v${node.yarnVersion}/")
    into file("${node.yarnWorkDir}/yarn/")
}

yarn_install {
    args = ['--frozen-lockfile']
}

yarn_build {
    outputs.upToDateWhen { false }
    dependsOn yarn_install
    args = ['--frozen-lockfile']
    def files = []
    projectDir.traverse(type: FILES, nameFilter: ~/.*\.js(on)?$/) {
        files << it
    }
    inputs.files(fileTree("$projectDir/src"), fileTree("$projectDir/public"), fileTree("$projectDir/tests"), files)
    outputs.dir("$projectDir/dist")
}

yarnSetup.finalizedBy(copyYarn)
nodeSetup.dependsOn(':tflite_tinyYolov2:makeEnvFile')
nodeSetup.finalizedBy(copyNode)

task lint {
    dependsOn yarn_lintNoFix
}

task test {
    dependsOn yarn_testUnit
}

task clean(type: Delete) {
    followSymlinks = false
    delete 'build', 'dist', 'node_modules'
}
