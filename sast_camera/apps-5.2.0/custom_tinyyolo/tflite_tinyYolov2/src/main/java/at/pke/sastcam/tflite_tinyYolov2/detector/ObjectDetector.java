/*
 * Copyright 2019-2020 by Security and Safety Things GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.pke.sastcam.tflite_tinyYolov2.detector;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.RectF;
import android.graphics.Color;
import android.graphics.Matrix;
import android.support.annotation.RawRes;
import android.util.Size;
import android.util.Log;

import at.pke.sastcam.tflite_tinyYolov2.utilities.ResourceHelper;
import org.tensorflow.lite.Interpreter;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.ByteOrder;
import java.util.*;


/**
 * ObjectDetector is the class responsible for preparing and accessing our model. It is based on the example provided by
 * the TensorFlow team
 */
public class ObjectDetector {
    /**
     * Median value of 0 - 255 used to normalize inputs for non quantized models
     */
    @SuppressWarnings("MagicNumber")
    private static final float mColorMedian = 127.5f;

    @SuppressWarnings("MagicNumber")
    private static final int mBoundingBoxes = 5;
    @SuppressWarnings("MagicNumber")
    private static final int mGridSize = 13;
    @SuppressWarnings("MagicNumber")
    private static final int mClasses = 20;
    @SuppressWarnings("MagicNumber")
    private static final float[] anchors = {0.57273f, 0.677385f, 1.87446f, 2.06253f, 3.33843f, 5.47434f, 7.88282f, 3.52778f, 9.77052f, 9.16828f};

    @SuppressWarnings("MagicNumber")
    private static final float nms = 0.8f;  // overlapping threshold

    /**
     * nxn size of the image the model expects as input
     */
    private final int mInputSize;
    /**
     * Defines the maximum number of objects detected per image
     */
    private final int mMaxDetectionsPerImage;
    /**
     * Ordered list mapping from model output to string label
     */
    private final List<String> mLabels;
    /**
     * TensorFlow lite api
     */
    private final Interpreter mModel;
    /**
     * Whether the model is quantized or not. This affects how input images are processed
     */
    private final boolean mIsQuantized;
    /**
     * outputLocations: array of shape [Batchsize, mMaxDetectionsPerImage, 4]
     * contains the location of detected boxes in [top, left, bottom, right] format per detection
     */
    private float[][][] mOutputLocations;
    /**
     * outputClasses: array of shape [Batchsize, mMaxDetectionsPerImage]
     * contains the classes of detected boxes
     */
    private float[][] mOutputClasses;
    /**
     * outputScores: array of shape [Batchsize, mMaxDetectionsPerImage]
     * contains the scores of detected boxes
     */
    private float[][] mOutputScores;
    /**
     * numDetections: array of shape [Batchsize]
     * contains the number of detected boxes
     */
    private float[] mDetectionCount;
    private float[][][][] mYoloOutput;
    /**
     * Buffer for the image data that will contain bytes in RGB ordering
     */
    private ByteBuffer mImgData;
    //private Object mInputData;

    private static final String TAG = "ObjectDetector";

    /**
     * Constructs a new ObjectDetector from a specified model and label file
     * <p>
     * Determines buffer sizes and loads the specified model and labels
     *
     * @param c                         The application context
     * @param modelFileName             The name of the model to load from the app assets directory
     * @param labelFileResId            Int resource id of the label file stored in /res/raw/
     * @param maxDetectionsPerImage     The max number of detections per image
     * @param inputSize                 The size of the input the model expects should be inputSize x inputSize
     * @param numThreads                The number of threads TensorFlow should be instructed to use
     * @param allowFp16PrecisionForFp32 When set, optimizes memory at the cost of accuracy by using 16 bit floating
     *                                  point numbers rather than 32 bit
     * @param useNNAPI                  When set TensorFlow will be configured to use the Android neural network api
     *                                  that attempts to run the model on the best available hardware for any system.
     *                                  Has limited support on the Security and Safety Things platform
     * @param isQuantized               Defines whether the input model is quantized (lossy compressed) or not
     *                                  this is a property of the model and should be set accordingly
     */
    @SuppressWarnings("MagicNumber")
    ObjectDetector(final Context c,
                   final String modelFileName,
                   final @RawRes int labelFileResId,
                   final int maxDetectionsPerImage,
                   final int inputSize,
                   final int numThreads,
                   final boolean allowFp16PrecisionForFp32,
                   final boolean useNNAPI,
                   final boolean isQuantized) {
        mInputSize = inputSize;
        mMaxDetectionsPerImage = maxDetectionsPerImage;
        mIsQuantized = isQuantized;
        final int numBytesPerChannel = mIsQuantized ? 1 : 4;
        /*
         * Allocate image buffer using height x width x 3 (from RGB channels) x <size of data>
         */
        mImgData = ByteBuffer.allocateDirect(3 * mInputSize * mInputSize * numBytesPerChannel);
        /*
         * Use endianness of the hardware for the buffer
         */
        mImgData.order(ByteOrder.nativeOrder());
        /*
         * Configure TensorFlow interpreter options from parameters
         */
        final Interpreter.Options options = new Interpreter.Options();
        options.setNumThreads(numThreads)
                .setAllowFp16PrecisionForFp32(allowFp16PrecisionForFp32)
                .setUseNNAPI(useNNAPI);
        try {
            /* Load the model */
            mModel = new Interpreter(ResourceHelper.loadModelFile(c.getAssets(), modelFileName), options);
            /* Prepare the labels */
            mLabels = ResourceHelper.loadLabels(c, labelFileResId);
        } catch (IOException e) {
            throw new ExceptionInInitializerError("Unable to create ObjectDetector");
        }
    }

    /**
     * Runs inference on a bitmap
     * 1. Performs some data preprocessing
     * Populates the `imgData` input array with bytes from the bitmap in RGB order
     * Normalizes data if necessary
     * <p>
     * 2. Inference
     * Sets up inputs and outputs for the TensorFlow lite api `runForMultipleInputsOutputs`
     * https://www.tensorflow.org/lite/guide/inference
     * <p>
     * Inputs
     * Object[1]: our imgData array is the sole element
     * <p>
     * Outputs (in index order)
     * Location array: format is [top, left, bottom, right]
     * Object class id: mapped to human readable label using the label map
     * Confidence: float in range from 0 to 1
     * Detection Count: how many objects were detected in the frame
     * <p>
     * 3. Maps outputs to
     * {@link Recognition} objects
     * for easier use.
     *
     * @param input Image bitmap to run inference on
     * @return List of recognized objects
     */
    @SuppressWarnings("MagicNumber")
    public List<Recognition> recognizeImage(final Bitmap input) {
        Log.e(TAG, "Quantized: " + mIsQuantized);
        Log.e(TAG, "Input size of bitmap (W*H): " + input.getWidth() + " * " + input.getHeight());

        final Bitmap bitmap = Bitmap.createScaledBitmap(input, mInputSize, mInputSize, true);

        Log.e(TAG, "Scaled size of bitmap (W*H): " + bitmap.getWidth() + " * " + bitmap.getHeight());

        /*
         * Preprocess the image data from 0-255 int to normalized value based on the provided parameters.
         */
        mImgData.rewind();
        for (int y = 0; y < mInputSize; ++y) {
            for (int x = 0; x < mInputSize; ++x) {
                final int pixel = bitmap.getPixel(x, y);
                if (mIsQuantized) {
                    /* Quantized model */
                    mImgData.put((byte) Color.red(pixel));
                } else {
                    /* Float model */
                    mImgData.putFloat(Color.red(pixel));
                }
            }
        }
        for (int y = 0; y < mInputSize; ++y) {
            for (int x = 0; x < mInputSize; ++x) {
                final int pixel = bitmap.getPixel(x, y);
                if (mIsQuantized) {
                    /* Quantized model */
                    mImgData.put((byte) Color.green(pixel));
                } else {
                    /* Float model */
                    mImgData.putFloat(Color.green(pixel));
                }
            }
        }
        for (int y = 0; y < mInputSize; ++y) {
            for (int x = 0; x < mInputSize; ++x) {
                final int pixel = bitmap.getPixel(x, y);
                if (mIsQuantized) {
                    /* Quantized model */
                    mImgData.put((byte) Color.blue(pixel));
                } else {
                    /* Float model */
                    mImgData.putFloat(Color.blue(pixel));
                }
            }
        }

        final Object[] inputArray = {mImgData};

        /* Allocate buffers */
        final Map<Integer, Object> outputMap = new HashMap<>();

        mYoloOutput = new float[1][mBoundingBoxes * (mClasses + 4 + 1)][mGridSize][mGridSize];
        ByteBuffer tmpOutput = ByteBuffer.allocateDirect(mBoundingBoxes * (mClasses + 4 + 1) * mGridSize * mGridSize * 4);
        tmpOutput.order(ByteOrder.nativeOrder());

        FloatBuffer output = tmpOutput.asFloatBuffer();

        /*
         * Build output map to reflect the tensors trained in the model. This model has the order locations, classes,
         * scores, and count.
         */
        outputMap.put(0, output);

        /*
         * Accepts the formatted ByteBuffer as an inputArray, and runs an inference to populate detection data in the
         * outputMap
         */

        Log.e(TAG, "--> Run inference");
        mModel.runForMultipleInputsOutputs(inputArray, outputMap);
        Log.e(TAG, "<-- Finished inference");

        return convertOutputs(output);
    }

    private class PairF {
        public float first;
        public float second;

        public PairF() {
        }

        public PairF(float first, float second) {
            this.first = first;
            this.second = second;
        }
    }

    private int bufferPos(int color, int x, int y) {
        final int numBytesPerChannel = mIsQuantized ? 1 : 1;
        // final int numBytesPerChannel = mIsQuantized ? 1 : 4;
        final int rowStride = mInputSize * numBytesPerChannel;
        final int colorStride = mInputSize * rowStride;
        return color * colorStride + y * rowStride + x * numBytesPerChannel;
    }

    // the logistic sigmoid turns a value into a percentage
    private float sigmoid(float x) {
        return 1.0f / (1.0f + (float) Math.exp(-x));
    }

    private int maxIdx(float[] arr) {
        int idx = 0;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > arr[idx]) {
                idx = i;
            }
        }
        return idx;
    }

    private float max(float[] arr) {
        float max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        return max;
    }

    private float[] softmax(float[] values) {
        float maxVal = max(values);
        float[] exp = new float[values.length];
        float expSum = 0;
        for (int i = 0; i < values.length; i++) {
            exp[i] = (float) Math.exp(values[i]);
            expSum += exp[i];
        }

        for (int i = 0; i < exp.length; i++) {
            exp[i] = exp[i] / expSum;
        }

        return exp;
    }

    private float getResultValue(int x, int y, int boundingBox, int offset) {
        return mYoloOutput[0][boundingBox * (mClasses + 5) + offset][x][y];
    }

    private float getClassResult(int x, int y, int boundingBox, int classId) {
        final int offsetClasses = 5;
        return getResultValue(x, y, boundingBox, offsetClasses + classId);
    }

    private int getResultIdx(int x, int y, int boundingBox, int offset) {
        int stride1 = mGridSize;
        int stride2 = stride1 * mGridSize;
        int stride3 = stride2 * (mClasses + 4 + 1);

        return boundingBox * stride3 + offset * stride2 + x * stride1 + y;
    }

    private int getClassResultIdx(int x, int y, int boundingBox, int classId) {
        final int offsetClasses = 5;
        return getResultIdx(x, y, boundingBox, offsetClasses + classId);
    }

    private float calcArea(RectF a) {
        return a.width() * a.height();
    }

    private float calcIOU(RectF a, RectF b) {
        RectF overlap = new RectF(a);
        if (!overlap.intersect(b))
            return 0.0f;
        float area = calcArea(overlap);

        return area / (calcArea(a) + calcArea(b) - area);
    }

    private float normalizeCoord(float x) {
        return 2.0f / mInputSize * x - 1;
    }

    private List<Recognition> convertOutputs(FloatBuffer output) {
        int id = 0;
        float maxConfidence = 0.0f;

        final int offsetX = 0;
        final int offsetY = 1;
        final int offsetW = 2;
        final int offsetH = 3;
        final int offsetObjectness = 4;

        Log.e(TAG, "Start parsing of output");

        List<Recognition> recognitions = new ArrayList<>(mMaxDetectionsPerImage);
        for (int cy = 0; cy < mGridSize; cy++) {                         // grid columns
            for (int cx = 0; cx < mGridSize; cx++) {                     // grid rows
                for (int b = 0; b < mBoundingBoxes; b++) {               // 5 bounding boxes per grid cell
                    // compute the bounding box b with the highest confidence for grid cell (cy, cx)
                    // fetch raw data for the current grid cell

                    // box centre x, y relative to the grid element
                    PairF boxCentre_raw = new PairF(output.get(getResultIdx(cy, cx, b, offsetX)), output.get(getResultIdx(cy, cx, b, offsetY))); // {X, Y}
                    PairF boxWidhtAndHeight_raw = new PairF(output.get(getResultIdx(cy, cx, b, offsetW)), output.get(getResultIdx(cy, cx, b, offsetH))); // {W, H}
                    float objectness_raw = output.get(getResultIdx(cy, cx, b, offsetObjectness));

                    float[] probability_raw = new float[mClasses];
                    for (int i = 0; i < mClasses; i++)
                        probability_raw[i] = output.get(getClassResultIdx(cy, cx, b, i)); // probability of all classes

                    // We still need to do some processing on these five numbers tx, ty, tw, th, tc
                    // as they are in a bit of a weird format.
                    // blocksize = 32; gridWidth = 416 / blockSize; -> grid with = 13, blocksize = 32
                    int blocksize = mInputSize / mGridSize;
                    PairF boxCentre = new PairF((cx + sigmoid(boxCentre_raw.first)) * blocksize,
                            (cy + sigmoid(boxCentre_raw.second)) * blocksize);  // box centre x, y in the 416x416 image
                    PairF boxWidhtAndHeight = new PairF((float) Math.exp(boxWidhtAndHeight_raw.first) * anchors[2 * b] * blocksize,
                            (float) Math.exp(boxWidhtAndHeight_raw.second) * anchors[2 * b + 1] * blocksize); // box height and width w, h in the same image space                    // confidence whether this BB contains an object (in percent)

                    float objectness = sigmoid(objectness_raw);

                    float[] classes = softmax(probability_raw); // make probability distribution of the values

                    // find class with maximum probability
                    int maxProbIdx = maxIdx(classes);
                    // sigmoid turns negative weird values in probabilities ranging from 0 to 1
                    float confidence = objectness * classes[maxProbIdx]; // final confidence whether this BB contains an object (in percent)

                    if (confidence > maxConfidence) {
                        maxConfidence = confidence;
                    }

                    // confidence is high enough -> add it to result list of objectDetections
                    RectF boundingBox = new RectF(
                            normalizeCoord(Math.max(0.0f, boxCentre.first - (boxWidhtAndHeight.first / 2))),
                            normalizeCoord(Math.max(0.0f, boxCentre.second - (boxWidhtAndHeight.second / 2))),
                            normalizeCoord(Math.min(mInputSize - 1.0f, boxCentre.first + (boxWidhtAndHeight.first / 2))),
                            normalizeCoord(Math.min(mInputSize - 1.0f, boxCentre.second + (boxWidhtAndHeight.second / 2)))
                    );

                    if (confidence > 0.5f) {
                        final int labelOffset = 0;
                        Recognition result = new Recognition(
                                String.valueOf(id++),
                                mLabels.get(maxProbIdx + labelOffset),
                                confidence,
                                boundingBox);

                        boolean foundOverlap = false;

                        for (Recognition r : recognitions) {
                            if (r.getLabel() == result.getLabel() && calcIOU(boundingBox, r.getLocation()) > nms) {
                                foundOverlap = true;
                            }
                        }

                        if (foundOverlap) {
                            Log.e(TAG, "Skipping: " + result.toString());
                        } else {
                            Log.e(TAG, "Found: " + result.toString());
                            recognitions.add(result);
                        }
                    }
                }
            }
        }
        Log.e(TAG, "Max Confidence: " + maxConfidence);


        return recognitions;
    }
}
