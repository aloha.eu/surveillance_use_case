/*
 * Copyright 2019-2020 by Security and Safety Things GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.pke.sastcam.tflite_tinyYolov2.detector;

import android.content.Context;
import at.pke.sastcam.tflite_tinyYolov2.R;

/**
 * Configures and builds an object detector
 */
public class ObjectDetectorBuilder {
    private final Context mContext;
    private String mModelFileName = "tinyyolov2-op7.tflite";
    private int mLabelFileResId = R.raw.labelmap;
    @SuppressWarnings("MagicNumber")
    private int mMaxDetectionsPerImage = 10;
    @SuppressWarnings("MagicNumber")
    private int mInputSize = 416;
    @SuppressWarnings("MagicNumber")
    private int mNumThreads = 4;
    private boolean mAllowFp16PrecisionForFp32 = false;
    private boolean mUseNNAPI = true;
    private boolean mIsQuantized = false;

    /**
     * Creates a new detector builder
     *
     * @param context App context
     */
    public ObjectDetectorBuilder(final Context context) {
        mContext = context;
    }

    /**
     * Builds the detector with the current configuration
     *
     * @return A ready to use object detector
     * @throws ExceptionInInitializerError On failure to initialize due to error in IO
     */
    public ObjectDetector build() throws ExceptionInInitializerError {
        /* Additional error and exception handling can be done here */
        return new ObjectDetector(mContext,
            mModelFileName,
            mLabelFileResId,
            mMaxDetectionsPerImage,
            mInputSize,
            mNumThreads,
            mAllowFp16PrecisionForFp32,
            mUseNNAPI,
            mIsQuantized);
    }

    /**
     * Sets model name
     *
     * @param name Filename of the model
     * @return This builder
     */
    public ObjectDetectorBuilder setModelFileName(final String name) {
        mModelFileName = name;
        return this;
    }

    /**
     * Sets label file resource to use
     *
     * @param resId Resource id of the label file
     * @return This builder
     */
    public ObjectDetectorBuilder setLabelFileResourceId(final int resId) {
        mLabelFileResId = resId;
        return this;
    }

    /**
     * Sets the maximum number of detections for a single image
     * @param n The maximum
     * @return This builder
     */
    public ObjectDetectorBuilder setMaxDetectionsPerImage(final int n) {
        mMaxDetectionsPerImage = n;
        return this;
    }

    /**
     * Sets input size the detector expects
     *
     * @param size nxn size value for n
     * @return This builder
     */
    public ObjectDetectorBuilder setInputSize(final int size) {
        mInputSize = size;
        return this;
    }

    /**
     * Sets the number of threads to use
     *
     * @param n Number of threads
     * @return This builder
     */
    public ObjectDetectorBuilder setNumThreads(final int n) {
        mNumThreads = n;
        return this;
    }

    /**
     * Configures the detector to use 16 bit precision for 32 bit values to save on space
     *
     * @return This builder
     */
    public ObjectDetectorBuilder allowFp16PrecisionForFp32() {
        mAllowFp16PrecisionForFp32 = true;
        return this;
    }

    /**
     * Configures the detector to use the neural network processing API
     *
     * @return This builder
     */
    public ObjectDetectorBuilder useNNAPI() {
        mUseNNAPI = true;
        return this;
    }

    /**
     * Sets whether the model in question is quantized (lossy compressed) or not
     *
     * @param isQuantized Boolean specifying whether the model is quantized
     * @return This builder
     */
    public ObjectDetectorBuilder setIsQuantized(final boolean isQuantized) {
        mIsQuantized = isQuantized;
        return this;
    }
}
