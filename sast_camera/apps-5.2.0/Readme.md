# Readme

The tflite files for custom and custom_tinyyolo can be created using the scripts and readme in the ONNX2TFLite folder.

The resulting tflite files need to be copied to the coresponding folders:

- `yolov2-coco.tflite` to `custom/tfliteyolo2_graph/src/main/assets`
- `tinyyolov2-op7.tflite` to `custom_tinyyolo/tflite_tinyYolov2/src/main/assets`

The apps can be built using 

```
./gradlew assembleDebug
```

and installed using

```
adb install -r custom/tfliteyolo2_graph/build/outputs/apk/debug/tfliteyolo2_graph-debug.apk
adb install -r custom_tinyyolo/tflite_tinyYolov2/build/outputs/apk/debug/tflite_tinyYolov2-debug.apk
```
