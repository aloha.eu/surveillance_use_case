/*
 * Copyright 2019-2020 by Security and Safety Things GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.pke.sastcam.tfliteyolo2_graph.detector;

import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.Locale;
import java.util.StringJoiner;

/** An immutable result returned by a Classifier describing what was recognized. */
public class Recognition implements Parcelable {

    /**
     * Creator class for Parceling Recognition
     */
    public static final Creator<Recognition> CREATOR = new Creator<Recognition>() {
        @Override
        public Recognition createFromParcel(final Parcel source) {
            return new Recognition(source);
        }

        @Override
        public Recognition[] newArray(final int size) {
            return new Recognition[size];
        }
    };

    private final String mId;
    private final String mLabel;
    private final float mConfidence;
    private RectF mLocation;

    /**
     * Constructor used for parcel
     *
     * @param parcel containing number of objects detected values
     */
    public Recognition(final Parcel parcel) {
        mId = parcel.readString();
        mLabel = parcel.readString();
        mConfidence = parcel.readFloat();
        mLocation = parcel.readParcelable(RectF.class.getClassLoader());
    }

    /**
     * A single recognized object
     * @param id Identifier for the object
     * @param label The name of the object recognized
     * @param confidence Value from 0-1 how strong the confidence is for the detection
     * @param location Bounding box for the object
     */
    public Recognition(
        final String id, final String label, final float confidence, final RectF location) {
        mId = id;
        mLabel = label;
        mConfidence = confidence;
        mLocation = location;
    }

    /**
     * Gets recognition id
     * @return String id of the object
     */
    public String getId() {
        return mId;
    }

    /**
     * Gets the label for the object
     * @return Object class
     */
    public String getLabel() {
        return mLabel;
    }

    /**
     * Gets object confidence score. A sortable score for how good the recognition is relative to others. Higher is better.
     * @return 0-1 value indicating confidence
     */
    public float getConfidence() {
        return mConfidence;
    }

    /**
     * Gets a bounding box specifying the detection location
     * @return Bounding box
     */
    public RectF getLocation() {
        return new RectF(mLocation);
    }

    /**
     * Sets the location for a detection
     * @param location Rectangle specifying the location
     */
    public void setLocation(final RectF location) {
        mLocation = location;
    }

    @Override
    @SuppressWarnings("MagicNumber")
    public String toString() {
        final StringJoiner resultString = new StringJoiner(" ", "[", "]");
        if (mId != null) {
            resultString.add(mId);
        }

        if (mLabel != null) {
            resultString.add(mLabel);
        }

        resultString.add(String.format(Locale.US, "(%.1f%%)", mConfidence * 100.0f));

        if (mLocation != null) {
            resultString.add(String.valueOf(mLocation));
        }

        return resultString.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel parcel, final int i) {
        parcel.writeString(mId);
        parcel.writeString(mLabel);
        parcel.writeFloat(mConfidence);
        parcel.writeParcelable(mLocation, i);
    }
}
