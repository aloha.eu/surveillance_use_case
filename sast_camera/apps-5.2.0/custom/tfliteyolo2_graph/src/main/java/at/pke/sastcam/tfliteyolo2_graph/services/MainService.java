/*
 * Copyright 2019-2020 by Security and Safety Things GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.pke.sastcam.tfliteyolo2_graph.services;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.Size;

import com.securityandsafetythings.Build;
import com.securityandsafetythings.app.VideoService;
import at.pke.sastcam.tfliteyolo2_graph.BuildConfig;
import at.pke.sastcam.tfliteyolo2_graph.RestEndPoint;
import at.pke.sastcam.tfliteyolo2_graph.detector.ObjectDetector;
import at.pke.sastcam.tfliteyolo2_graph.detector.ObjectDetectorBuilder;
import at.pke.sastcam.tfliteyolo2_graph.detector.Recognition;
import at.pke.sastcam.tfliteyolo2_graph.utilities.BitmapUtilities;
import at.pke.sastcam.tfliteyolo2_graph.utilities.Renderer;
import com.securityandsafetythings.jumpsuite.datatrolley.constants.MetadataChannel;
import com.securityandsafetythings.jumpsuite.datatrolley.interfaces.MetadataTrolley;
import com.securityandsafetythings.jumpsuite.datatrolley.interfaces.data.ObjectPayload;
import com.securityandsafetythings.jumpsuite.datatrolley.interfaces.data.Payload;
import com.securityandsafetythings.jumpsuite.datatrolley.trolleys.DataTrolleyProvider;
import com.securityandsafetythings.video.RefreshRate;
import com.securityandsafetythings.video.VideoCapture;
import com.securityandsafetythings.video.VideoManager;
import com.securityandsafetythings.video.VideoSession;
import com.securityandsafetythings.web_components.webserver.RestHandler;
import com.securityandsafetythings.web_components.webserver.WebServerConnector;

import java.util.ArrayList;
import java.util.List;

/**
 * This class responds to {@link #onCreate()} and {@link #onDestroy()} methods of the application
 * lifecycle. In order to receive images from the Video Pipeline, {@link MainService} extends
 * {@link VideoService} and implements its callbacks.
 *
 * The three callbacks are:
 *
 * 1. {@link #onVideoAvailable(VideoManager)} - Triggered when the Video Pipeline is ready to begin transferring {@link Image} objects.
 * 2. {@link #onImageAvailable(ImageReader)} - Triggered when the {@link ImageReader} acquires a new {@link Image}.
 * 3. {@link #onVideoClosed(VideoSession.CloseReason)} - Triggered for one of the four close reasons mentioned in method's JavaDoc.
 */
public class MainService extends VideoService {
    private static final String LOGTAG = MainService.class.getSimpleName();
    /*
     * When the video session is restarted due to base camera configuration changed,
     * this Handler is used to post messages to the UI thread/main thread
     * for proper rendering
     */
    private static final Handler UI_HANDLER = new Handler(Looper.getMainLooper());
    @SuppressWarnings("MagicNumber")
    private static final float MIN_CONFIDENCE = 0.5f;
    private WebServerConnector mWebServerConnector;
    private RestEndPoint mRestEndPoint;
    private ObjectDetector mDetector;
    private MetadataTrolley mDataTrolley;
    private VideoManager mVideoManager;

    /**
     * {@link #onCreate()} initializes our {@link WebServerConnector}, {@link RestEndPoint}, {@link MetadataTrolley} and
     * {@link RestHandler}.
     *
     * The {@link WebServerConnector} acts as the bridge between our application and the webserver
     * which is contained in the Security and Safety Things SDK.
     *
     * The {@link RestEndPoint} is a class annotated with JaxRs endpoint annotations. This is the class
     * that we interact with via HTTP on the front end.
     *
     * The {@link RestHandler} acts as a wrapper class for our {@link RestEndPoint}. The Handler registers our
     * {@link RestEndPoint}, and connects it to the WebServer.
     *
     * The {@link MetadataTrolley} acts as a wrapper to handle interactions with the MessageBroker API.
     */

    @Override
    public void onCreate() {
        super.onCreate();
        /*
         * Creates a RestHandler with a base path of 'app/getPackageName()'.
         */
        final RestHandler restHandler = new RestHandler(this, BuildConfig.WEBSITE_ASSET_PATH);
        /*
         * Causes static context serving to serve index.html for any path.
         */
        restHandler.serveIndexOnAnyRoute();
        /*
         * Registers the RestEndPoint with the server via the RestHandler class. The RestHandler
         * is just a wrapper for the RestEndPoint's JaxRs annotated functions.
         */
        mRestEndPoint = new RestEndPoint();
        restHandler.register(mRestEndPoint);
        /*
         * Connects the RestHandler with the WebServerConnector.
         */
        mWebServerConnector = new WebServerConnector(this);
        mWebServerConnector.connect(restHandler);
        /*
         * Creates a MetadataTrolley to publish metadata to other apps, as well as VMS.
         */
        mDataTrolley = DataTrolleyProvider.createMetadataTrolley(this);
        mDataTrolley.connect();
        /*
         * Get a detector builder and use it to configure the detector
         */
        mDetector = new ObjectDetectorBuilder(this)
        //    .setIsQuantized(true)
            .build();
    }

    /**
     * This callback is triggered when the Video pipeline is available to begin capturing images.
     *
     * @param manager Is the {@link VideoManager} object we use to obtain access to the Video Pipeline
     */
    @SuppressWarnings("MagicNumber")
    @Override
    protected void onVideoAvailable(final VideoManager manager) {
        /**
         * Stores {@link VideoManager} for subscribing to video streams from the video pipeline
         */
        mVideoManager = manager;
        /*
         * Gets the default video capture and starts video session
         */
        startVideoSession();
    }

    /**
     * Callback is triggered when an image is obtained, here images can be stored for retrieval from the exposed
     * methods of the {@link RestEndPoint}.
     *
     * The example model operates on 300x300 pixel images. You may or may not recall that the image preview used in
     * Helloworld is full HD 1920x1080. We would not want to use just a small 300x300 crop of our preview image. Instead what
     * we can do is crop the full resolution image to the same aspect ratio as our model (1:1) and then rescale the cropped
     * image to the size our detector accepts. This way the resize operation does not warp our input, though this warping is
     * valid for some models, we won't use it here.
     *
     * @param reader Is the {@link ImageReader} object we use to obtain still frames from the Video Pipeline
     */
    @Override
    protected void onImageAvailable(final ImageReader reader) {
        /*
         * Gets the latest image from the Video Pipeline and stores it in the RestEndPoint class so the
         * frontend can retrieve it via a GET call to rest/example/live.
         */
        try (Image image = reader.acquireLatestImage()) {
            // ImageReader may sometimes return a null image.
            if (image == null) {
                Log.e("onImageAvailable()", "ImageReader returned null image.");
                return;
            }
            long start = System.currentTimeMillis();

            final Bitmap imageBmp = BitmapUtilities.imageToBitmap(image);
            // Perform classification using the detector
            final List<Recognition> detectionResults = mDetector.recognizeImage(imageBmp);
            // Filter detections that meet the specified minimum confidence threshold {@link #MIN_CONFIDENCE}.
            final List<Recognition> filteredDetections = new ArrayList<>();
            for (Recognition recognition : detectionResults) {
                if (recognition.getConfidence() >= MIN_CONFIDENCE) {
                    filteredDetections.add(recognition);
                }
            }
            /*
             * Utility method for publishing results with the help of the MessageBroker to another app and to the VMS.
             */
            publishResults(filteredDetections, image.getTimestamp());
            /*
             * Utility class renders the detections on imageBmp. We pass crop size, and base image size along with the
             * margins so that we know where to draw the relative coordinates from the detector on the base image
             */
            Renderer.render(new Canvas(imageBmp),
                filteredDetections,
                new Size(imageBmp.getWidth(), imageBmp.getHeight()),
                imageBmp.getWidth(),
                imageBmp.getHeight());
            // Send the image to the rest endpoint just as before
            mRestEndPoint.setImage(imageBmp);
            image.close();
            long end = System.currentTimeMillis();
            Log.e("Detection Main Service", "=====> : Took " + (end - start) + " ms to process frame = " + (1000.0/(end - start)) + " fps");
        }
    }

    private void publishResults(final List<Recognition> detectionResults, final long timestamp) {
        if (!mDataTrolley.isConnected()) {
            Log.e(LOGTAG, "MetadataTrolley is not connected. Cannot publish results.");
            return;
        }
        /*
         * Converts {@link Recognition}s to {@link Payload}s to be sent through the MetadataTrolley, which
         * encapsulates marshalling and unmarshalling data to send over the MessageBroker API.
         */
        final List<Payload> payloadList = recognitionsToPayloads(detectionResults, timestamp);
        /*
         * Sends the detections to VMS via a specialized ONVIF channel. The MetadataTrolley handles all the
         * necessary encoding needed, including conversion to an ONVIF-compliant XML.
         */
        mDataTrolley.send(MetadataChannel.METADATA_CHANNEL_ONVIF, payloadList);
    }

    /**
     * Converts {@link Recognition}s to {@link Payload}s to send over a {@link MetadataTrolley}.
     *
     * @param detectionResults list of {@link Recognition}s to convert
     * @return list of converted {@link Payload}s
     */
    private List<Payload> recognitionsToPayloads(final List<Recognition> detectionResults, final long timestamp) {
        int id = 0;
        final List<Payload> payloadList = new ArrayList<>();
        for (Recognition recognition : detectionResults) {
            final ObjectPayload objectPayload =
                new ObjectPayload(
                        timestamp,
                        "",                         // Source of the image
                        String.valueOf(id++),
                        recognition.getConfidence(),
                        recognition.getLabel(),
                        recognition.getLocation());
            payloadList.add(objectPayload);
        }
        return payloadList;
    }

    /**
     * This callback would handle all tear-down logic, and is called when the {@link VideoSession} is stopped.
     * Five possible ways for the video session to be stopped are:
     *
     * 1. {@link VideoSession.CloseReason#SESSION_CLOSED}
     * 2. {@link VideoSession.CloseReason#VIRTUAL_CAMERA_CONFIGURATION_CHANGED}
     * 3. {@link VideoSession.CloseReason#VIRTUAL_CAMERA_CONFIGURATION_REMOVED}
     * 4. {@link VideoSession.CloseReason#RENDERING_FAILED}
     * 5. {@link VideoSession.CloseReason#BASE_CAMERA_CONFIGURATION_CHANGED}
     *
     * @param reason is the reason for closing the video pipeline
     */
    @Override
    @SuppressWarnings("MagicNumber")
    protected void onVideoClosed(final VideoSession.CloseReason reason) {
        Log.i(LOGTAG, "onVideoClosed(): reason " + reason.name());
        /*
         * BASE_CAMERA_CONFIGURATION_CHANGED reason is only available in API v5 and above.
         */
        if (Build.VERSION.MAX_API >= 5) {
            /*
             * Checks whether the video session closing reason was BASE_CAMERA_CONFIGURATION_CHANGED.If yes, the video session is restarted.
             */
            if (reason == VideoSession.CloseReason.BASE_CAMERA_CONFIGURATION_CHANGED) {
                /*
                 * The video session is restarted due to base camera configuration changes
                 * in the main thread for proper rendering
                 */
                UI_HANDLER.post(new Runnable() {
                    @Override
                    public void run() {
                        /*
                         * Restarts the video session from the main thread/UI thread
                         */
                        startVideoSession();
                    }
                });
            }
        }
    }

    /**
     * Gets the default videoCapture and starts the video session.
     */
    @SuppressWarnings("MagicNumber")
    private void startVideoSession() {
        /*
         * Gets a default VideoCapture instance which does not scale, rotate, or modify the images received from the Video Pipeline.
         */
        final VideoCapture capture = mVideoManager.getDefaultVideoCapture();
        Log.d(LOGTAG, String.format("getDefaultVideoCapture() with width %d and height %d",
            capture.getWidth(), capture.getHeight()));
        /*
         * RefreshRate specifies how often the image should be updated, in this case 15 frames per second (FPS_15).
         */
        openVideo(capture, capture.getWidth() / 2, capture.getHeight() / 2, RefreshRate.FPS_15, false);
        Log.d(LOGTAG, "startVideoSession(): openVideo() is called and video session is started");
    }

    /**
     * Disconnects the {@link WebServerConnector} when service is destroyed. This method is triggered when the application
     * is stopped.
     */
    @Override
    public void onDestroy() {
        mWebServerConnector.disconnect();
        /*
         * Disconnects the MetadataTrolley
         */
        mDataTrolley.disconnect();
        super.onDestroy();
    }
}
