#include "Movidius_SSD.h"
#include <chrono>
#include<thread>
#include <future>

typedef std::chrono::high_resolution_clock::time_point t_point;

Movidius_SSD::Movidius_SSD()
{
    //init_pars();
    
}
void Movidius_SSD::set_detector_type(int type)
{
    m_detector_type = type;
    if (m_detector_type == 0)
        m_graph_file_name = "/home/ribeiro/sharedev/ncsdk/ncappzoo/caffe/SSD_MobileNet/graph";
    else  if (m_detector_type == 1)
    {
        m_graph_file_name = "/home/ribeiro/sharedev/ncsdk/ncappzoo/caffe/TinyYolo/graph";
        m_detector_type = 1;
    }
    else
    {
        m_detector_type = -1;
    }
}

void Movidius_SSD::init_pars()
{
    auto pars_map  = pars_file_reader()("movidius.pars");
    int detector_type = 0;
    if (pars_map.count("type"))
    {
        try 
        {
            detector_type = std::stoi(pars_map.at("type"));
        }
        catch (...)
        {
            detector_type = 0;
           //std::cout << "Movidius_SSD::init_pars could not convert par type "  << std::endl;
        }
    }
   //std::cout << "Movidius_SSD::init_pars pars_map.size " << pars_map.size() << " " << detector_type << std::endl;
    set_detector_type(detector_type);
    
}
void Movidius_SSD::init_thr(bool try_to_destroy)
{
    
   //std::cout << "Movidius_SSD::init lock mutex" << std::endl;
    
    std::this_thread::sleep_for(std::chrono::milliseconds(3000));
    m_initializing = true;
    if (try_to_destroy && m_graph_handle)
        destroy();
    //if (m_device_handle)
    //{
    //    ncDeviceClose(m_device_handle);
    //    ncDeviceDestroy(&m_device_handle);
        //m_device_handle = nullptr;
    //}
    //init_pars();
    if (m_detector_type != -1)
    {
        open_device();
        load_graph();
    }
    else
    {
       //std::cout << "Movidius_SSD::init_thr  m_detector_type not valid " << m_detector_type << std::endl;
    }
    m_initializing = false;
    /*if (!m_graph_load)
        return Movidius_SSD_INIT_ERROR;
     
    return Movidius_SSD_INITIALIZED;*/
}
void Movidius_SSD::reset()
{
   //std::cout << "Movidius_SSD::reset " << std::endl;
    std::lock_guard<std::mutex> lock(m_mutex);
    destroy();
    //m_init_attempts = 1;
   //std::cout << "Movidius_SSD::reset done " << std::endl;
    
    //init();
}

void Movidius_SSD::reset_to(int type)
{
   //std::cout << "Movidius_SSD::reset_to " << type << std::endl;
    reset();
    set_detector_type(type);
   //std::cout << "Movidius_SSD::reset_to done" << std::endl;
}

Movidius_SSD::~Movidius_SSD()
{
    destroy();
}

Movidius_SSD_status Movidius_SSD::is_init()
{
    if (m_initializing)
        return Movidius_SSD_INITIALIZING;
    else if (!m_initializing && m_graph_load)
    {
        return Movidius_SSD_INITIALIZED;
    }
    return Movidius_SSD_INIT_NOT_INITIALIZED;
}

void * Movidius_SSD::load_file(const char *path, unsigned int  & length)
{
    FILE *fp;
    char *buf;
    
    fp = fopen(path, "rb");
    if(fp == NULL)
        return 0;
    fseek(fp, 0, SEEK_END);
    length = ftell(fp);
    rewind(fp);
    if(!(buf = (char*) malloc(length)))
    {
        fclose(fp);
        return 0;
    }
    if(fread(buf, 1, length, fp) != length)
    {
        fclose(fp);
        free(buf);
        return 0;
    }
    fclose(fp);
    return buf;
}
void Movidius_SSD::load_image(std::string const & file_name)
{
    m_cv_orig_image = cv::imread(file_name);
    load_image(m_cv_orig_image);
}

void Movidius_SSD::load_image(cv::Mat const & cv_img)
{
    m_cv_orig_image = cv_img;
    
    if (m_graph_load)
    {   
        // LoadImage will read image from disk, convert channels to floats
        // subtract network mean for each value in each channel.
    
        unsigned int imageSize = sizeof(float) * m_tensor_dimension * m_tensor_dimension * 3;
       //std::cout << "Movidius_SSD::load_image " << m_tensor_dimension << std::endl;
        if (m_float_image_size != imageSize || m_float_image_fp32 == nullptr)
        {
            if (m_float_image_fp32 != nullptr)
                free(m_float_image_fp32);
            m_float_image_fp32 = (float*) malloc(imageSize);
            m_float_image_size = imageSize;
        }
        if(m_float_image_fp32 == nullptr)
        {
            std::cerr << " erro load_image_from_cv malloc" << std::endl;
            m_float_image_size = imageSize;
            return;
        }

        
        cv::Mat cvimgres;
        cv::resize(cv_img,cvimgres,cv::Size(m_tensor_dimension,m_tensor_dimension));
        
        int i = 0;
        for (int y = 0;  y < cvimgres.rows; ++y)
        {
            auto imgptr = cvimgres.ptr<cv::Vec3b>(y);
            for (int x = 0; x < cvimgres.cols; ++x, ++imgptr, i+=3)
            {
                m_float_image_fp32[i] = ((*imgptr)[2] - 127.5)*0.007843;
                m_float_image_fp32[i+1] = ((*imgptr)[1] - 127.5)*0.007843;
                m_float_image_fp32[i+2] = ((*imgptr)[0] - 127.5)*0.007843;
            }
        }
    }
    
}

void Movidius_SSD::open_device()
{
   //std::cout << "Movidius_SSD::open_device " << std::endl;
    
    m_device_open = false;
    
    int loglevel = NC_LOG_INFO;
    ncStatus_t retCode = ncGlobalSetOption(NC_RW_LOG_LEVEL, &loglevel, sizeof(loglevel));
    // Initialize device handle
    retCode = ncDeviceCreate(0, &m_device_handle);
    if(retCode != NC_OK)
    {
       //std::cout << "Movidius " << "Error - No NCS devices found.\n" << std::endl;
       //std::cout << "Movidius " << "    ncStatus value: " << retCode << std::endl;
        return;
    }
    
    // Try to open the NCS device via the device name
    retCode = ncDeviceOpen(m_device_handle);
    if (retCode != NC_OK)
    {   // failed to open the device.  
       //std::cout << "Movidius " << "Could not open NCS device" << std::endl;
        return;
    }
    
    // m_device_handle is ready to use now.  
    // Pass it to other NC API calls as needed and close it when finished.
   //std::cout << "Movidius " << "Successfully opened NCS device!" << std::endl;
    m_device_open = true;
    
}
void Movidius_SSD::load_graph()
{
    m_graph_load = false;
    if (m_device_open)
    {
        
        // Now read in a graph file
        
        unsigned int graphFileLen;
        void* m_graph_file_buf = load_file(m_graph_file_name.c_str(), graphFileLen);
        
        // Init graph handle
        auto retCode = ncGraphCreate("graph", &m_graph_handle);
        if (retCode != NC_OK)
        {
           //std::cout << "Movidius " << "Error - ncGraphCreate failed" << std::endl;
            return;
        }
        
        // Send graph to device
        retCode = ncGraphAllocate(m_device_handle, m_graph_handle, m_graph_file_buf, graphFileLen);
        if (retCode != NC_OK)
        {   // error allocating graph
           //std::cout << "Movidius " << "Could not allocate graph for file: " << m_graph_file_name << std::endl; 
           //std::cout << "Movidius " << "Error from ncGraphAllocate is: " << retCode << " " << NC_INVALID_PARAMETERS << std::endl;
        }
        else
        {
           //std::cout << "Movidius " << "Successfully allocated graph for " << m_graph_file_name << std::endl;
            
            // successfully allocated graph.  Now m_graph_handle is ready to go.  
            // use m_graph_handle for other API calls and call mvncDeallocateGraph
            // when done with it.
            
            // Read tensor descriptors
            struct ncTensorDescriptor_t inputTensorDesc;
            struct ncTensorDescriptor_t outputTensorDesc;
            unsigned int length = sizeof(struct ncTensorDescriptor_t);
            ncGraphGetOption(m_graph_handle, NC_RO_GRAPH_INPUT_TENSOR_DESCRIPTORS, &inputTensorDesc,  &length);
            ncGraphGetOption(m_graph_handle, NC_RO_GRAPH_OUTPUT_TENSOR_DESCRIPTORS, &outputTensorDesc,  &length);
            //int dataTypeSize = outputTensorDesc.totalSize/(outputTensorDesc.w* outputTensorDesc.h*outputTensorDesc.c*outputTensorDesc.n);
           //std::cout << "Movidius " << "inputTensorDesc " << inputTensorDesc.w << " " << inputTensorDesc.h << " " << inputTensorDesc.c << " " << inputTensorDesc.n << std::endl;
           //std::cout << "Movidius " << "outputTensorDesc " << outputTensorDesc.w << " " << outputTensorDesc.h << " " << outputTensorDesc.c << " " << outputTensorDesc.n << std::endl;
            m_tensor_dimension = inputTensorDesc.w;
            // Init & Create Fifos
            
            auto retCode = ncFifoCreate("FifoIn0", NC_FIFO_HOST_WO, &m_buffer_in);
            if (retCode != NC_OK)
            {
               //std::cout << "Movidius " << "Error - Input Fifo Initialization failed!" << std::endl;
                return;
            }
            retCode = ncFifoAllocate(m_buffer_in, m_device_handle, &inputTensorDesc, 20);
           //std::cout << "Movidius " << "inputTensorDesc " << inputTensorDesc.w << " " << inputTensorDesc.h << " " << inputTensorDesc.c << " " << inputTensorDesc.n << std::endl;
            
            if (retCode != NC_OK)
            {
               //std::cout << "Movidius " << "Error - Input Fifo allocation failed!" << std::endl;
                return;
            }
           //std::cout << "Movidius " << "Movidius_SSD::load_graph 100 "  << std::endl;
            
            retCode = ncFifoCreate("FifoOut0", NC_FIFO_HOST_RO, &m_buffer_out);
            if (retCode != NC_OK)
            {
               //std::cout << "Movidius " << "Error - Output Fifo Initialization failed!" << std::endl;
                return;
            }
           //std::cout << "Movidius " << "Movidius_SSD::load_graph 110 "  << std::endl;
            
            retCode = ncFifoAllocate(m_buffer_out, m_device_handle, &outputTensorDesc, 20);
            if (retCode != NC_OK)
            {
               //std::cout << "Movidius " << "Error - Output Fifo allocation failed!" << std::endl;
                return;
            }
           //std::cout << "Movidius " << "Movidius_SSD::load_graph 120 "  << std::endl;
            
            m_graph_load = true;
        }
    }
    
}

Movidius_SSD_status Movidius_SSD::init_async()
{
    
    if (m_number_of_waiting < 5)
    {
       std::this_thread::sleep_for(std::chrono::milliseconds(100));
    //if (!m_initializing)
    //{
        m_initializing = true;
        m_number_of_waiting++;
       //std::cout << "Movidius init waiting on mutex m_number_of_waiting " << m_number_of_waiting << std::endl;
        std::lock_guard<std::mutex> lock(m_mutex);
        std::thread init_thread(&Movidius_SSD::init_thr,this,true);
        init_thread.detach();
        if (m_number_of_waiting > 0)
            m_number_of_waiting--;
        return Movidius_SSD_INITIALIZING;
    //}
    }
    return is_init();
}

Movidius_SSD_status Movidius_SSD::init_sync(int type)
{
    
    if (m_number_of_waiting < 5)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        //if (!m_initializing)
        //{
        set_detector_type(type);
        m_initializing = true;
        m_number_of_waiting++;
        //std::cout << "Movidius init waiting on mutex m_number_of_waiting " << m_number_of_waiting << std::endl;
        std::lock_guard<std::mutex> lock(m_mutex);
        init_thr(true);
        if (m_number_of_waiting > 0)
            m_number_of_waiting--;
        return Movidius_SSD_INITIALIZING;
        //}
    }
    return is_init();
}

void Movidius_SSD::detect(SSD_detection_result_vec & det_res_vec)
{
    //t_point t0 = std::chrono::high_resolution_clock::now();
    write_tensor();
    //t_point t1 = std::chrono::high_resolution_clock::now();
    inference(det_res_vec);
    //t_point t2 = std::chrono::high_resolution_clock::now();
    ////std::cout << "Movidius " << " " << std::chrono::duration_cast<std::chrono::microseconds>(t1-t0).count()/1000.0;
    ////std::cout << " " << std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count()/1000.0;
    //std::cout << " miliseconds " << std::endl;
}

void Movidius_SSD::write_tensor()
{
    
    m_tensor_load = false;
    if (m_graph_load && m_float_image_fp32 != nullptr)
    {   
        // Write tensor to input fifo
        auto retCode = ncFifoWriteElem(m_buffer_in, m_float_image_fp32, &m_float_image_size, 0);
        if (retCode != NC_OK)
        {
           //std::cout << "Movidius " << "Error - Failed to write element to Fifo!" << std::endl;
            return;
        }
        
        retCode = ncGraphQueueInference(m_graph_handle, &m_buffer_in, 1, &m_buffer_out, 1);
        if (retCode != NC_OK)
        {
           //std::cout << "Movidius " << "Error - Failed to queue Inference!" << std::endl;
        }
        //std::cout << "Movidius " << "Movidius_SSD::inference 20 "  << std::endl;
        
        // Read output results
        unsigned int length = sizeof(unsigned int);
        retCode = ncFifoGetOption(m_buffer_out, NC_RO_FIFO_ELEMENT_DATA_SIZE, &m_outputDataLength, &length);
        if (retCode || length != sizeof(unsigned int)){
           //std::cout << "Movidius " << "ncFifoGetOption failed, rc=" <<  retCode << std::endl;
        }
        
        if (m_result_size != m_outputDataLength)
        {
            m_result_size = m_outputDataLength;
            if (m_result)
                free(m_result);
            m_result = malloc(m_outputDataLength);  // \TODO SSD has a fix output length, do not have to realocate and free result
        }
       //std::cout << "Movidius " << "Movidius_SSD::inference 30 "  << m_outputDataLength << " m_tensor_dimension " << m_tensor_dimension <<  std::endl;
        
        if (!m_result) {
            m_result_size = 0;
           //std::cout << "Movidius " << "malloc m_result failed!\n" << std::endl;
        }
        else
        {
           //std::cout << "Movidius " << "Successfully loaded the tensor " << std::endl;
            m_tensor_load = true;
        }
        
    }
}

void Movidius_SSD::inference(SSD_detection_result_vec & det_res_vec)
{
        
    if (m_graph_load && m_tensor_load)
    {
            
        
           // the inference has been started, now call mvncGetResult() for the
            // inference result
            
            // queue inference
            //std::cout << "Movidius " << "Movidius_SSD::inference 10 "  << std::endl;
           /* auto retCode = ncGraphQueueInference(m_graph_handle, &m_buffer_in, 1, &m_buffer_out, 1);
            if (retCode != NC_OK)
            {
               //std::cout << "Movidius " << "Error - Failed to queue Inference!" << std::endl;
                exit(-1);
            }
            //std::cout << "Movidius " << "Movidius_SSD::inference 20 "  << std::endl;
            
            // Read output results
            unsigned int outputDataLength;
            unsigned int length = sizeof(unsigned int);
            retCode = ncFifoGetOption(m_buffer_out, NC_RO_FIFO_ELEMENT_DATA_SIZE, &outputDataLength, &length);
            if (retCode || length != sizeof(unsigned int)){
               //std::cout << "Movidius " << "ncFifoGetOption failed, rc=" <<  retCode << std::endl;
                exit(-1);
            }
            
            if (m_result_size != outputDataLength)
            {
                m_result_size = outputDataLength;
                if (m_result)
                    free(m_result);
                m_result = malloc(outputDataLength);  // \TODO SSD has a fix output length, do not have to realocate and free result
            }
           //std::cout << "Movidius " << "Movidius_SSD::inference 30 "  << outputDataLength << " m_tensor_dimension " << m_tensor_dimension <<  std::endl;
            
            if (!m_result) {
                m_result_size = 0;
               //std::cout << "Movidius " << "malloc m_result failed!\n" << std::endl;
                exit(-1);
            }
            */
            
            
            /*unsigned int optionDataLen = sizeof(int);
            int fifoOutputSize = 0;
            while (fifoOutputSize == 0)
            {
                retCode = ncFifoGetOption(m_buffer_out, NC_RO_FIFO_READ_FILL_LEVEL, &fifoOutputSize, &optionDataLen);
               //std::cout << "Movidius " << "Movidius_SSD::inference 39 fifoOutputSize "  << fifoOutputSize << std::endl;
                std::this_thread::sleep_for(std::chrono::milliseconds(5));
                
            }*/
            
            
           //std::cout << "Movidius " << "Movidius_SSD::inference 40 fifoOutputSize " << std::endl;
            
            void *userParam;
            /*auto retCode = ncFifoReadElem(m_buffer_out, m_result, &m_outputDataLength, &userParam);
            static int test_count = 1;
            if (!(test_count%10))
            {
                //m_graph_load = false;
                init_thr(false);
                
            }
            ++test_count;*/
            
            //std::this_thread::sleep_for(std::chrono::milliseconds(50));
            //////std::cout << "Movidius  retCode " << retCode << std::endl;
            //retCode = ncFifoReadElem(m_buffer_out, m_result, &m_outputDataLength, &userParam);
            
            
            auto retCode = NC_TIMEOUT;
            bool do_async = false;
            if (do_async)
            {
                std::future<ncStatus_t> result( std::async(std::launch::async,ncFifoReadElem,m_buffer_out, m_result, &m_outputDataLength, &userParam));
                
                
                /*static int test_count = 1;
                if (!(test_count%20))
                    retCode = ncFifoReadElem(m_buffer_out, m_result, &m_outputDataLength, &userParam);
                ++test_count;*/
                
                std::future_status status;
                do
                {
                    status = result.wait_for(std::chrono::seconds(1));
                    if (status == std::future_status::deferred) {
                        std::this_thread::sleep_for(std::chrono::milliseconds(5));
                    } else if (status == std::future_status::timeout) {
                        std::cerr << "Movidius ncFifoReadElem timed_out, reinitializing !!!! ##################" << std::endl;
                        m_graph_load = false;
                        //if (m_device_handle != nullptr)
                        //{
                        //ncDeviceClose(m_device_handle);
                        //ncDeviceDestroy(&m_device_handle);
                            //m_device_handle = nullptr;
                        //}
                        //destroy();
                        ncDeviceClose(m_device_handle);
                        ncDeviceDestroy(&m_device_handle);
                    } else if (status == std::future_status::ready) {
                    //std::cout << "Movidius ncFifoReadElem ready " << std::endl;
                        retCode = result.get();
                    }
                } while  (status != std::future_status::ready && status != std::future_status::timeout);
            }
            else
            {
                retCode = ncFifoReadElem(m_buffer_out, m_result, &m_outputDataLength, &userParam);
            }
            
            //write_tensor();
            
            
            //std::cout << "Movidius " << "Movidius_SSD::inference 50 retCode "  << retCode << std::endl;
            
            if (retCode != NC_OK && retCode != NC_TIMEOUT)
            {
               //std::cout << "Movidius " << "Error - Read Inference result failed! #######" << std::endl;
                det_res_vec.clear();
            }
            else if (retCode == NC_TIMEOUT)
            {
               //std::cout << "Movidius " << "Error - Read Inference result timed_out" << std::endl;
                det_res_vec.clear();
            }
            else
            {   
                if (m_detector_type == 0)
                    parse_results_SSD(det_res_vec);
                else if (m_detector_type == 1)
                    parse_results_tinyYolo(det_res_vec);
                
                    
            }
        }
       //std::cout << "Movidius " << "Movidius_SSD::inference done " << std::endl;
        

    
    

}

void Movidius_SSD::parse_results_SSD(SSD_detection_result_vec & det_res_vec)
{

    // Successfully got the result.  The inference result is in the buffer pointed to by resultData
    //std::cout << "Movidius " << "Successfully got the inference result for image  " << std::endl;
    
    //unsigned int numResults =  outputDataLength / sizeof(float);
   //std::cout << "Movidius " << "parse_results_SSD "  << std::endl;
    
    float *fresult = (float*) m_result;
    
    //   a.	First fp16 value holds the number of valid detections = num_valid.
    //   b.	The next 6 values are unused.
    //   c.	The next (7 * num_valid) values contain the valid detections data
    //       Each group of 7 values will describe an object/box These 7 values in order.
    //       The values are:
    //         0: image_id (always 0)
    //         1: class_id (this is an index into labels)
    //         2: score (this is the probability for the class)
    //         3: box left location within image as number between 0.0 and 1.0
    //         4: box top location within image as number between 0.0 and 1.0
    //         5: box right location within image as number between 0.0 and 1.0
    //         6: box bottom location within image as number between 0.0 and 1.0
    if (fresult[0] > 0)
    {
        det_res_vec.resize(fresult[0]);
    //std::cout << "Movidius " << "number of valid result boxes: " <<  fresult[0] << std::endl;
    //std::cout << "Movidius " << "Movidius_SSD::inference 50 " << fresult[0] << std::endl;
       //std::cout << "Movidius " << "parse_results_SSD "  << fresult[0] << std::endl;
        
        for (unsigned int i = 7; i < (fresult[0] + 1)*7; i += 7)
        {
            //std::cout << "Movidius " << "Movidius_SSD::inference 51 " << i << " " << i/7 - 1 << std::endl;
            //std::cout << "Movidius " << "Movidius_SSD::inference 52 " << i << " " <<  fresult[i + 1] << std::endl;
            //std::cout << "Movidius " << "Movidius_SSD::inference 53 " << i << std::endl;
            
            /*//std::cout << "Movidius " << fresult[i + 0] << std::endl;
            *      //std::cout << "Movidius " << fresult[i + 1] << std::endl;
            *      //std::cout << "Movidius " << fresult[i + 2] << std::endl;
            *      //std::cout << "Movidius " << fresult[i + 3] << std::endl;
            *      //std::cout << "Movidius " << fresult[i + 4] << std::endl;
            *      //std::cout << "Movidius " << fresult[i + 5] << std::endl;
            *      //std::cout << "Movidius " << fresult[i + 6] << std::endl;*/
            
            if (fresult[i + 1] >= 0 && fresult[i + 1] < m_string_labels.size())
            {
                //std::cout << "Movidius " << "Movidius_SSD::inference 54 " << i << std::endl;
                det_res_vec[i/7 - 1].oclass = "SSD " + m_string_labels[fresult[i + 1]];
                //std::cout << "Movidius " << "Movidius_SSD::inference 55 " << i << std::endl;
            }
            else
            {
                //std::cout << "Movidius " << "Movidius_SSD::inference 56 " << i << std::endl;
                det_res_vec[i/7 - 1].oclass = "??";
                //std::cout << "Movidius " << "Movidius_SSD::inference 57 " << i << std::endl;
                
            }
            //std::cout << "Movidius " << "Movidius_SSD::inference 60 " << i << std::endl;
            
            det_res_vec[i/7 - 1].prob = fresult[i + 2];
            det_res_vec[i/7 - 1].bbox.resize(4);
            det_res_vec[i/7 - 1].bbox[0] = fresult[i + 3]*m_cv_orig_image.cols;
            det_res_vec[i/7 - 1].bbox[1] = fresult[i + 4]*m_cv_orig_image.rows;
            det_res_vec[i/7 - 1].bbox[2] = fresult[i + 5]*m_cv_orig_image.cols;
            det_res_vec[i/7 - 1].bbox[3] = fresult[i + 6]*m_cv_orig_image.rows;
            
        }
    }
    else
    {
        det_res_vec.clear();
    }
    

}

void Movidius_SSD::parse_results_tinyYolo(SSD_detection_result_vec & det_res_vec)
{
    
   //std::cout << "yolo fresult[0] start" << std::endl;
    det_res_vec.clear();
    float *fresult = (float*) m_result;
   //std::cout << "yolo fresult " << fresult << std::endl;
    
    
    //det_res_vec.resize(fresult[0]);
    
    // 7x7 x20 = first 980 values [0,980)
    
    // values from [980,1078) = 7x7x2 box_prob_scale_factor ?prob sacale for each box
    
    // values from [1078,5880) all boxes
    
    // 7x7 x2 x20 = all probability values ??
    const int grid_size = 7;
    const int boxes_per_grid_cell = 2;
    const int num_classifications = 20;
    
    size_t fresult_idx = 0;
    
    
    std::vector<float> prob_scale_for_each_box(grid_size*grid_size*boxes_per_grid_cell);
    fresult_idx =  grid_size*grid_size*num_classifications; //980
    for (int j = 0; j < grid_size*grid_size*boxes_per_grid_cell; ++j, ++fresult_idx)
    {
        prob_scale_for_each_box[j] = fresult[fresult_idx];
    }
    
    
    //std::vector<float> all_probabilities(grid_size*grid_size*boxes_per_grid_cell*num_classifications);
    
    std::vector< std::pair<int,float> >  best_class_per_box(grid_size*grid_size*boxes_per_grid_cell,{-1,-1.f});

    fresult_idx = 0;
    //all_prob_idx = 0;
    size_t each_box_idx = 0;
    for (int y = 0; y < grid_size; ++y)
    {
        for (int x = 0; x < grid_size; ++x)
        {
            
            for (int b = 0; b < boxes_per_grid_cell; ++b, ++each_box_idx)
            {
                
                best_class_per_box[each_box_idx].first = -1;
                best_class_per_box[each_box_idx].second = -1.f;
                float prob_scale = prob_scale_for_each_box[each_box_idx];
                int fresult_idx_class = fresult_idx;
                for (int c = 0; c < num_classifications; ++c, fresult_idx_class++)
                {
                    
                    float prob = fresult[fresult_idx_class]*prob_scale;
                    if (prob > 0.1 && prob > best_class_per_box[each_box_idx].second)
                    {
                        best_class_per_box[each_box_idx].first = c;
                        best_class_per_box[each_box_idx].second = prob;
                       //std::cout << "yolo probs fresult_idx " << fresult_idx << " fresult_idx_class " << fresult_idx_class << " " <<  y << " " << x << " " << c << " " << b << " " << fresult[fresult_idx_class] << " " << prob << " " << m_string_labels[c + 1] << std::endl;
                        //all_probabilities[all_prob_idx]
                    }
                }
            }
            fresult_idx += num_classifications;
        }
        
    }
    
    
    //grid_size, grid_size, boxes_per_grid_cell, 4
    fresult_idx =  grid_size*grid_size*num_classifications + grid_size*grid_size*boxes_per_grid_cell; //1078
    each_box_idx = 0;
    for (int y = 0; y < grid_size; ++y)
    {
        for (int x = 0; x < grid_size; ++x)
        {
            for (int c = 0; c < boxes_per_grid_cell; ++c ,++each_box_idx)
            {
                if (best_class_per_box[each_box_idx].first >= 0)
                {
                    det_res_vec.resize(det_res_vec.size() +1);
                    auto & res = det_res_vec.back();
                    res.prob = best_class_per_box[each_box_idx].second;
                    res.oclass = "Yolo " + m_string_labels[best_class_per_box[each_box_idx].first + 1];
                    res.bbox.resize(4);
                    float xc = ((fresult[fresult_idx] + x)/grid_size); 
                    float yc = ((fresult[fresult_idx+1] + y)/grid_size);
                    float w = (fresult[fresult_idx+2]);
                    float h = (fresult[fresult_idx+3]);
                    w = w*w;
                    h = h*h;
                    res.bbox[0] = (xc - w/2.0)*m_cv_orig_image.cols;
                    res.bbox[1] = (yc - h/2.0)*m_cv_orig_image.rows;
                    res.bbox[2] = (xc + w/2.0)*m_cv_orig_image.cols;
                    res.bbox[3] = (yc + h/2.0)*m_cv_orig_image.rows;
                   //std::cout << " yolo res " << res.prob << " " <<  res.oclass << " " << res.bbox[0] << " " << res.bbox[1] << " " << res.bbox[2] << " " << res.bbox[3] << std::endl;
                    
                    
                }
                fresult_idx += 4;
            }
            
        }
    }
    
    
    
    //std::cout << "Movidius " << "number of valid result boxes: " <<  fresult[0] << std::endl;
    //std::cout << "Movidius " << "Movidius_SSD::inference 50 " << fresult[0] << std::endl;
    
    
    
    
    
}
void Movidius_SSD::destroy()
{
   //std::cout << "Movidius " << "Movidius_SSD::destroy " << std::endl;
    if (m_graph_load)
    {
        auto retCode = ncGraphDestroy(&m_graph_handle);
        if (retCode != NC_OK)
        {
           //std::cout << "Movidius " << "Error - Failed to deallocate graph!" << std::endl;
        }
        retCode = ncFifoDestroy(&m_buffer_out);
        if (retCode != NC_OK)
        {
           //std::cout << "Movidius " << "Error - Failed to deallocate fifo!" << std::endl;
        }
        retCode = ncFifoDestroy(&m_buffer_in);
        if (retCode != NC_OK)
        {
           //std::cout << "Movidius " << "Error - Failed to deallocate fifo!" << std::endl;
        }
        if (m_result != nullptr)
        {
            free(m_result);
            m_result_size = 0;
        }
        m_result = nullptr;
        m_buffer_in = nullptr;
        m_buffer_out = nullptr;
        m_graph_handle = nullptr;
        m_graph_load = false;
    }
    m_float_image_size = 0;
    if (m_float_image_fp32 != nullptr)
    {
        free(m_float_image_fp32);
        m_float_image_fp32 = nullptr;
    }
    if (m_graph_file_buf != nullptr)
    {
        free(m_graph_file_buf);
        m_graph_file_buf = nullptr;
    }
    
    //if (m_device_handle != nullptr)
    //{
        ncDeviceClose(m_device_handle);
        ncDeviceDestroy(&m_device_handle);
        m_device_handle = nullptr;
    //}
    //std::this_thread::sleep_for(std::chrono::milliseconds(3000));
   //std::cout << "Movidius " << "Movidius_SSD::destroy done" << std::endl;
}
