#pragma once
#include "Definitions.h"

#include <mvnc.h>
#include <opencv2/opencv.hpp>

#include <iostream>
#include <thread>
#include <mutex>

enum Movidius_SSD_status { Movidius_SSD_INIT_UNKNOWN, Movidius_SSD_INIT_NOT_INITIALIZED, Movidius_SSD_INITIALIZED, Movidius_SSD_INITIALIZING, Movidius_SSD_INIT_ERROR };
class Movidius_SSD
{
 
public:
    
    Movidius_SSD();
    ~Movidius_SSD();
    void reset();
    void reset_to( int value);
    Movidius_SSD_status is_init();
    Movidius_SSD_status init_async();
    Movidius_SSD_status init_sync(int type = -1);
    
    template <typename Img_load_def>
    Movidius_SSD_status detect(Img_load_def const & img_def, SSD_detection_result_vec & det_res_vec)
    {
        auto res = is_init();
        if (res == Movidius_SSD_INITIALIZED)
        {
            std::lock_guard<std::mutex> lock(m_mutex);
            load_image(img_def);
            detect(det_res_vec);
        }
        return res;
    }
    
   int get_int_detector_type () {return m_detector_type;}
    
    
    
    
    
private:
    bool m_graph_load = false;
    bool m_device_open = false;
    bool m_tensor_load = false;
    //std::string m_graph_file_name = "/home/ribeiro/sharedev/ncsdk/examples/caffe/AlexNet/cpp/graph_from_ssd";
    std::string m_graph_file_name = "/home/ribeiro/sharedev/ncsdk/ncappzoo/caffe/TinyYolo/graph";
    
    struct ncGraphHandle_t *m_graph_handle = nullptr;
    struct ncDeviceHandle_t *m_device_handle = nullptr;
    struct ncFifoHandle_t * m_buffer_in = nullptr;
    struct ncFifoHandle_t * m_buffer_out = nullptr;
    void* m_graph_file_buf = nullptr;
    void * m_result = nullptr;
    unsigned int m_result_size = 0;
    unsigned int m_outputDataLength = 0;
    
    int m_tensor_dimension;
    
    float * m_float_image_fp32 = nullptr;
    unsigned int m_float_image_size = 0;
    
    cv::Mat m_cv_orig_image;
    
    int m_init_attempts = 1;
    int m_number_of_waiting = 0;
    bool m_initializing = false;
    int m_detector_type = -1;
    
    std::vector< std::string > m_string_labels = { "background",
        "aeroplane", "bicycle", "bird", "boat",
        "bottle", "bus", "car", "cat", "chair",
        "cow", "diningtable", "dog", "horse",
        "motorbike", "person", "pottedplant",
        "sheep", "sofa", "train", "tvmonitor"};
        
        
    
    
    std::mutex m_mutex;
    
    void * load_file(const char *path, unsigned int & length);
    void load_image(std::string const & file_name);
    void load_image(cv::Mat const & cv_img);
    
    void open_device();
    void load_graph();
    
    void detect(SSD_detection_result_vec & det_res_vec);
    void write_tensor();    
    void inference(SSD_detection_result_vec & det_res_vec);
    void parse_results_SSD(SSD_detection_result_vec & det_res_vec);
    void parse_results_tinyYolo(SSD_detection_result_vec & det_res_vec);
    
    void destroy();
    void init_thr(bool try_to_destroy = true);
    void init_pars();
    
    void set_detector_type(int type);
    
    
    //Movidius_SSD_status init_if_needed();
    
    
    
    
};