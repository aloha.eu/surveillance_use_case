#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <fstream>

typedef std::vector<float> FBounding_box;
typedef std::vector< FBounding_box > FBounding_box_vec;

struct SSD_detection_result
{
    std::string oclass;
    float prob = 0.f;
    FBounding_box bbox;
};

typedef std::vector< SSD_detection_result > SSD_detection_result_vec;


typedef std::vector<float> Simple_pol;
typedef std::vector<Simple_pol> Complex_pol;
typedef std::vector<Complex_pol> Complex_pol_vec;


struct Id_fshapes 
{ 
    std::string id;
    int object_class;
    FBounding_box bbox;
    Complex_pol pol;
};
typedef std::map< std::string , Id_fshapes > Shapes_id_vec;

struct Annotation_frame_data
{
    std::string image_id;
    std::string seq_id;
    Shapes_id_vec shapes_id_vec;
    
};



class pars_file_reader
{
public:
    std::map<std::string,std::string> operator () (std::string const & filename) const 
    { 
        std::map<std::string,std::string> ret_map;
        std::ifstream file_in(filename);
        std::string key,value;
        while (file_in >> key && file_in >> value){
            std::cout << "Read: " << key << " " << value << std::endl;
            ret_map[key] = value;
        }
        return ret_map;
    } 
};
