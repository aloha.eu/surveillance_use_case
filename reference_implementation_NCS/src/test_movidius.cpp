#include <iostream>
#include "Movidius_SSD.h"
#include <opencv2/opencv.hpp>


/*
struct SSD_detection_result
{
    std::string oclass;
    float prob = 0.f;
    FBounding_box bbox;
};

typedef std::vector< SSD_detection_result > SSD_detection_result_vec;
*/



int main()
{
    Movidius_SSD mov;
    std::cout << "initializing mov ..." << std::endl;
    mov.init_sync(0);
    //mov.reset_to(0);
    std::cout << "initializing mov ..." << std::endl;
    cv::VideoCapture cap(0);
    cv::Mat img;
    SSD_detection_result_vec det_res_vec;
    if (mov.is_init())
    {
        while (cap.read(img))
        {
            if (mov.is_init() == Movidius_SSD_INITIALIZED)
            {
                mov.detect(img,det_res_vec);
                for (auto & det_res : det_res_vec)
                {
                    auto & b = det_res.bbox;
                    cv::Rect ret(b[0],b[1],b[2] - b[0] + 1,b[3] - b[1] + 1);
                    cv::rectangle(img,ret,cv::Scalar(255,255,0),1,1);
                }
            }
            cv::imshow("img",img);
            cv::waitKey(10);
        }
    }
    else
    {
        std::cout << "Could not initialyse movidius " << std::endl;
    }
        
    
    std::cout << "start " << std::endl;
    return 0;
}