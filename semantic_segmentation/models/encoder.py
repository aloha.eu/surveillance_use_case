import torch.nn as nn
from torchvision import models
import pretrainedmodels
from .xception import Xception65


def se_net(name, pretrained=False):
    if name in ['se_resnet50', 'se_resnet101', 'se_resnet152',
                'se_resnext50_32x4d', 'se_resnext101_32x4d', 'senet154']:
        pretrained = 'imagenet' if pretrained else None
        senet = pretrainedmodels.__dict__[name](num_classes=1000, pretrained=pretrained)
    else:
        return NotImplemented

    layer0 = senet.layer0
    layer1 = senet.layer1
    layer2 = senet.layer2
    layer3 = senet.layer3
    layer4 = senet.layer4

    layer0.out_channels = senet.layer1[0].conv1.in_channels
    layer1.out_channels = senet.layer1[-1].conv3.out_channels
    layer2.out_channels = senet.layer2[-1].conv3.out_channels
    layer3.out_channels = senet.layer3[-1].conv3.out_channels
    layer4.out_channels = senet.layer4[-1].conv3.out_channels

    return [layer0, layer1, layer2, layer3, layer4]


def create_encoder(enc_type, output_stride=8, pretrained=True):
    if enc_type == 'xception65':
        return Xception65(output_stride)
    else:
        raise NotImplementedError
