import torch.nn.functional as F

class SegmentatorTTA(object):
    @staticmethod
    def hflip(x):
        return x.flip(3)

    @staticmethod
    def vflip(x):
        return x.flip(2)

    @staticmethod
    def trans(x):
        return x.transpose(2, 3)

    def pred_resize(self, x, size):
        h, w = size

        pred = self.forward(F.pad(x, (0, 1, 0, 1)))
        return F.interpolate(pred, size=(h+1, w+1), mode='bilinear', align_corners=True)[..., :h, :w]