import numpy as np
from pathlib import Path

from tqdm import tqdm
import os

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

import torch
from torch.utils.data import DataLoader

from models.net import SPPNet

import argparse
from dataset.cityscapes import CustomTestDataset
import json

from imantics import Mask
# https://github.com/jsbroks/imantics/

VALID_CLASSES = [7, 8, 11, 12, 13, 17, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 31, 32, 33]
N_CLASSES = len(VALID_CLASSES)
CLASS_NAMES = ['road', 'sidewalk', 'building', 'wall', 'fence', 'pole', 'traffic light', 'traffic sign', 'vegetation', 'terrain', 'sky', 'person', 'rider', 'car', 'truck', 'bus', 'train', 'motorcycle', 'bicycle']

model_path_input = 'model/model.pth'
id2cls_dict = dict(zip(range(N_CLASSES), VALID_CLASSES))
id2cls_func = np.vectorize(id2cls_dict.get)


def predict(batched, model, device):
    images, names, orig_img = batched
    images = images.to(device)
    preds = model.pred_resize(images, images.shape[2:])
    preds = preds.argmax(dim=1)
    preds_np = preds.detach().cpu().numpy().astype(np.uint8)
    torch.cuda.empty_cache()
    return  orig_img, preds_np, names



def demo_run(img_np, mask, unique_labels, output_dir, names):
    fig, ax = plt.subplots(1, 2, figsize=(18, 10))
    plt.tight_layout()
    im = ax[0].imshow(mask)
    ax[0].set_xticks([])
    ax[0].set_yticks([])
    ax[1].imshow(img_np)
    ax[1].set_xticks([])
    ax[1].set_yticks([])

    hand = []
    for id in unique_labels:
        hand.append(mpatches.Patch(color=im.cmap(im.norm(id)), label=CLASS_NAMES[VALID_CLASSES.index(id)]))
    plt.legend(handles=hand)
    plt.savefig(output_dir / f'{names}_s.png')
    plt.close()


def mask_run(mask, class_labels, unique_labels, output_dir, names):
    d = {'image_name': names,
         'classes': class_labels,
         'image_shape': mask.shape,
         'mask': mask.tolist()
         }
    with open(os.path.join(output_dir, f'{names}_mask.json'), 'w') as f:
        json.dump(d, f)

    d_poly = {}
    for ul in unique_labels:
        binary_mask = np.zeros([mask.shape[0], mask.shape[1]])
        row, clm = np.where(mask == ul)
        binary_mask[row, clm] = 1
        polygons = Mask(binary_mask).polygons()
        points = polygons.points
        d_poly[CLASS_NAMES[VALID_CLASSES.index(ul)]] = [lst.tolist() for lst in points]
    with open(os.path.join(output_dir, f'{names}_polygon_borders.json'), 'w') as f:
        json.dump(d_poly, f)


def full_run(img_np, mask, class_labels, unique_labels, output_dir, names):
    fig, ax = plt.subplots(1, 2, figsize=(18, 10))
    plt.tight_layout()
    im = ax[0].imshow(mask)
    ax[0].set_xticks([])
    ax[0].set_yticks([])
    ax[1].imshow(img_np)
    ax[1].set_xticks([])
    ax[1].set_yticks([])

    hand = []
    for id in np.unique(mask):
        hand.append(mpatches.Patch(color=im.cmap(im.norm(id)), label=CLASS_NAMES[VALID_CLASSES.index(id)]))
    plt.legend(handles=hand)
    plt.savefig(output_dir / f'{names}_s.png')
    plt.close()

    d = {'image_name': names,
         'classes': class_labels,
         'image_shape': mask.shape,
         'mask': mask.tolist()
         }
    with open(os.path.join(output_dir, f'{names}_mask.json'), 'w') as f:
        json.dump(d, f)

    fig, ax = plt.subplots(1, 2, figsize=(18, 10))
    plt.tight_layout()
    im = ax[0].imshow(mask)
    ax[0].set_xticks([])
    ax[0].set_yticks([])
    ax[1].imshow(img_np)
    ax[1].set_xticks([])
    ax[1].set_yticks([])

    d_poly = {}
    for ul in unique_labels:
        binary_mask = np.zeros([mask.shape[0], mask.shape[1]])
        row, clm = np.where(mask == ul)
        binary_mask[row, clm] = 1
        polygons = Mask(binary_mask).polygons()
        points = polygons.points
        d_poly[CLASS_NAMES[VALID_CLASSES.index(ul)]] = [lst.tolist() for lst in points]
        for poly in points:
            poly = poly.tolist()
            poly.append(poly[0])
            xs, ys = zip(*poly)
            ax[1].plot(xs, ys, '.-', alpha=0.65, label=CLASS_NAMES[VALID_CLASSES.index(ul)])
    with open(os.path.join(output_dir, f'{names}_polygon_borders.json'), 'w') as f:
        json.dump(d_poly, f)

    hand = []
    for id in np.unique(mask):
        hand.append(mpatches.Patch(color=im.cmap(im.norm(id)), label=CLASS_NAMES[VALID_CLASSES.index(id)]))
    plt.legend(handles=hand)
    plt.savefig(output_dir / f'{names}_s.png')
    plt.close()



def run_prediction(key, img_target_size, img_path_input, img_path_output, device):
    model_path = Path(model_path_input)
    model = SPPNet()
    model.to(device)
    model.update_bn_eps()

    param = torch.load(model_path)
    model.load_state_dict(param)
    del param
    model.eval()


    valid_dataset = CustomTestDataset(img_path_input, target_size=img_target_size)
    valid_loader = DataLoader(valid_dataset, batch_size=1, shuffle=False)

    output_dir = Path(img_path_output)
    if not os.path.exists(output_dir):
        output_dir.mkdir(parents=True)

    with torch.no_grad():
        for batched in tqdm(valid_loader):
            img_np, mask, names = predict(batched, model, device)
            img_np = img_np[0]
            names = names[0]
            mask = id2cls_func(mask).astype(np.uint8)[0]
            unique_labels = np.unique(mask)

            class_labels = [CLASS_NAMES[VALID_CLASSES.index(id)] for id in unique_labels]
            if key == 'demo':
                demo_run(img_np, mask, unique_labels, output_dir, names)
            elif key == 'masks':
                mask_run(mask, class_labels, unique_labels, output_dir, names)
            elif key == 'full':
                full_run(img_np, mask, class_labels, unique_labels, output_dir, names)
            else:
                raise ValueError("No key exists. Please use --help to see possible options.")



if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Process segmentation')
    parser.add_argument('-mode', '--mode', type=str,  help='prediction state: demo - pictures only, masks - masks stroed in json files, full - all above', required=True)
    parser.add_argument('-im_size', '--img_size', nargs='+',  help='image desired output size Ex.: 512 512 OR None to presrve origianl size', required=True)
    parser.add_argument('-in_path', '--in_path', type=str,  help='path to image folder', required=True)
    parser.add_argument('-out_path', '--out_path', type=str,  help='output folder', required=True)
    parser.add_argument('-device', '--device', type=str,  help='device to process: cpu or cuda:gpu_index', required=True)
    args = parser.parse_args()

    if len(args.img_size) == 2:
        img_size = tuple([int(args.img_size[0]), int(args.img_size[1])])
    else:
        img_size = None

    run_prediction(args.mode, img_size, args.in_path, args.out_path, args.device)
