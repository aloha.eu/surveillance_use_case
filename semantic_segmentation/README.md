## ALOHA WP5 - Image segmentaiton 

### Description

In order to enable fast and efficient segmentation results we used UNet [1] approach. 
As the encoder part ResNet-50 [2] backbone was chosen, while the decoder consists of Atrous Separable Convolutions [3].
Network was trained on Cityscapes Dataset [4] for 100 epochs using Lovasz-Softmax loss [5]. 
Final IoU score (62.6 %) is comparable with original DeepLabV1 [6] results (64.8 %).


[1] https://arxiv.org/abs/1505.04597
[2] https://arxiv.org/abs/1512.03385
[3] https://arxiv.org/abs/1802.02611
[4] https://www.cityscapes-dataset.com
[5] https://arxiv.org/abs/1705.08790
[6] https://arxiv.org/abs/1606.00915
### How to use
Usage: `python predict_masks.py -mode MODE -im_size IMG_SIZE -in_path IN_PATH -out_path OUT_PATH -device DEVICE`

- `MODE`: `demo` - saves images only, image contains mask image and original one; `masks` - saves masks and polygons border points in separate files; 
  `full` - saves all info as in demo and masks + draws polygons on original image 
- `IMG_SIZE`: `None` - full size image; `sizex sizey` - desired output size
- `IN_PATH` - path to the folder with images
- `OUT_PATH` - path to storage folder
- `DEVICE` - `cpu` - run segmentation on CPU; `cuda:X` - run segementaiton on GPU where `X` - id of GPU

### Output structure
All output will consist of original image name and prefix depending on the data will be stored. 

Mask output format:
mask stored per image. 
```
{
  'image_name': names of the original image
  'classes': names  of detected classes
  'image_shape': image shape
  'mask': mask in the list format
}
```

Polygon boundaries:
polygons stored per image. 
For each of the detected `X_n` class the list of points for each polygon detected withing this class. 
```
{
  'class_name_X_1': polygon_points_X_1, 
  'class_name_X_2': polygon_points_X_2, 
  ...
  'class_name_X_n': polygon_points_X_N, 
}
```

### Dependencies 

```
Python 3.8.5 

imantics==0.1.12
numpy==1.19.5
opencv-python==4.5.1.48
opencv-python-headless==4.5.1.48
torch==1.8.0+cu111
torchaudio==0.8.0
torchvision==0.9.0+cu111
tqdm==4.56.0
```

