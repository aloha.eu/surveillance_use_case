from functools import partial
import numpy as np
from PIL import Image
from pathlib import Path
import albumentations as albu

import torch
from torch.utils.data import Dataset

from utils.preprocess import minmax_normalize
from utils.custum_aug import PadIfNeededRightBottom


class CustomTestDataset(Dataset):
    n_classes = 19
    void_classes = [0, 1, 2, 3, 4, 5, 6, 9, 10, 14, 15, 16, 18, 29, 30, -1]
    valid_classes = [7, 8, 11, 12, 13, 17, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 31, 32, 33]

    class_map = dict(zip(valid_classes, range(n_classes)))

    def __init__(self, base_dir,target_size=None, ignore_index=255, debug=False):
        self.debug = debug
        self.base_dir = Path(base_dir)
        self.ignore_index = ignore_index

        types = ['*.png', '*.jpg']
        pth = []
        for type in types:
            pth += self.base_dir.glob(type)
        self.img_paths = sorted(pth)

        # Resize
        if isinstance(target_size, tuple):
            target_size = (target_size[0], target_size[1])
            self.resizer = albu.Compose([ albu.Resize(height=target_size[0], width=target_size[1], p=1.0)])
        else:
            self.resizer = None

        self.affine_augmenter = None
        self.image_augmenter = None

    def __len__(self):
        return len(self.img_paths)

    def __getitem__(self, index):
        img_path = self.img_paths[index]
        img_orig = np.array(Image.open(img_path))

        # Resize (Scale & Pad & Crop)
        img = minmax_normalize(img_orig, norm_range=(-1, 1))
        if self.resizer:
            resized = self.resizer(image=img)
            img = resized['image']
            resized_orig = self.resizer(image=img_orig)
            img_orig = resized_orig['image']
        img = img.transpose(2, 0, 1)
        img = torch.FloatTensor(img)
        return img, img_path.stem, img_orig
