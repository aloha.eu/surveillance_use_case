//g++ -o pke_dataset_converter -std=c++11  ../src/pke_dataset_converter.cpp   `pkg-config --cflags --libs opencv`  -lstdc++fs -Wall -Werror


//#include "trajectory_image_json_test.h"
#include "pke_data_set_manager.h"

#include "rapidjson/document.h"
#include <rapidjson/writer.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/filereadstream.h>

#include <opencv2/opencv.hpp>

#include <sys/stat.h>

#include <fstream>
#include <cstdio>
#include <iostream>
#include <map>
#include <set>
#include <experimental/filesystem>


std::vector<float> bb_ltwh_to_ltrb(std::vector<float> && v)
{
    v[2] += v[0];
    v[3] += v[1];
    return std::move(v);
}
struct Parameters
{
    std::string path_to_create_dataset;
    std::string path_to_input_annotation_file;
    std::string path_to_input_images_directory;
    std::string path_to_input_dataset;
    std::string sequence_name;
    std::set<std::string> supercategories;
    std::set<std::string> names;
    int max_number_of_images;
    float min_area_relative_to_image;
    int use_only_annotations_with_polygon;
    int display_images;
    bool needs_masks;

};

struct Category_info
{
    Category_info(std::string const & _name,
                  std::string const & _supercategory,
                  int _id) : name(_name),supercategory(_supercategory),
                  pke_int_label(_id) {}
    Category_info() {}
    std::string name;
    std::string supercategory;
    int pke_int_label = -1;
};

std::map<std::string,int> dataset_to_pke_label_translation =
{

    {"person",0},
    {"vehicle",1},
    {"other",2},

};

void parse_coco_category(std::map<int,Category_info> & category_map, rapidjson::Value & data)
{
    //const rapidjson::Value& categories = data["categories"];
    for (rapidjson::SizeType i = 0; i < data.Size(); i++) // Uses SizeType instead of size_t
    {
        Category_info cat_i;
        cat_i.supercategory = data[i]["supercategory"].GetString();
        cat_i.name = data[i]["name"].GetString();
        int id = data[i]["id"].GetInt();
        if (cat_i.supercategory == "person")
            cat_i.pke_int_label = dataset_to_pke_label_translation.at("person");
        else if (cat_i.supercategory == "vehicle")
            cat_i.pke_int_label = dataset_to_pke_label_translation.at("vehicle");
        else
            cat_i.pke_int_label = dataset_to_pke_label_translation.at("other");

        category_map.emplace(id,std::move(cat_i));
    }
}

void parse_city_category(std::map<std::string,Category_info> & category_map)
{
    /*category_map = {
{ 0 , {"unlabeled","void", dataset_to_pke_label_translation.at("other")}},
{ 1 , {"ego vehicle","void", dataset_to_pke_label_translation.at("other") }},
{ 2 , {"rectification border","void", dataset_to_pke_label_translation.at("other")}},
{ 3 , {"out of roi","void", dataset_to_pke_label_translation.at("other") }},
{ 4 , {"static","void", dataset_to_pke_label_translation.at("other") }},
{ 5 , {"dynamic","void", dataset_to_pke_label_translation.at("other") }},
{ 6 , {"ground","void", dataset_to_pke_label_translation.at("other") }},
{ 7 , {"road","flat", dataset_to_pke_label_translation.at("other") }},
{ 8 , {"sidewalk","flat", dataset_to_pke_label_translation.at("other") }},
{ 9 , {"parking","flat", dataset_to_pke_label_translation.at("other") }},
{ 10 , {"rail track","flat", dataset_to_pke_label_translation.at("other") }},
{ 11 , {"building","construction", dataset_to_pke_label_translation.at("other") }},
{ 12 , {"wall","construction", dataset_to_pke_label_translation.at("other") }},
{ 13 , {"fence","construction", dataset_to_pke_label_translation.at("other") }},
{ 14 , {"guard rail","construction", dataset_to_pke_label_translation.at("other") }},
{ 15 , {"bridge","construction", dataset_to_pke_label_translation.at("other") }},
{ 16 , {"tunnel","construction", dataset_to_pke_label_translation.at("other") }},
{ 17 , {"pole","object", dataset_to_pke_label_translation.at("other") }},
{ 18 , {"polegroup","object", dataset_to_pke_label_translation.at("other") }},
{ 19 , {"traffic light","object", dataset_to_pke_label_translation.at("other") }},
{ 20 , {"traffic sign","object", dataset_to_pke_label_translation.at("other") }},
{ 21 , {"vegetation","nature", dataset_to_pke_label_translation.at("other") }},
{ 22 , {"terrain","nature", dataset_to_pke_label_translation.at("other") }},
{ 23 , {"sky","sky", dataset_to_pke_label_translation.at("other") }},
{ 24 , {"person","human", dataset_to_pke_label_translation.at("person") }},
{ 25 , {"rider","human", dataset_to_pke_label_translation.at("person") }},
{ 26 , {"car","vehicle", dataset_to_pke_label_translation.at("vehicle") }},
{ 27 , {"truck","vehicle", dataset_to_pke_label_translation.at("vehicle") }},
{ 28 , {"bus","vehicle", dataset_to_pke_label_translation.at("vehicle") }},
{ 29 , {"caravan","vehicle", dataset_to_pke_label_translation.at("vehicle") }},
{ 30 , {"trailer","vehicle", dataset_to_pke_label_translation.at("vehicle") }},
{ 31 , {"train","vehicle", dataset_to_pke_label_translation.at("vehicle") }},
{ 32 , {"motorcycle","vehicle", dataset_to_pke_label_translation.at("vehicle") }},
{ 33 , {"bicycle","vehicle", dataset_to_pke_label_translation.at("vehicle") }}

    };*/
std::cout << "--dataset_to_pke_label_translation " << dataset_to_pke_label_translation.at("other")  << std::endl;
std::cout << "--dataset_to_pke_label_translation " << dataset_to_pke_label_translation.at("person")  << std::endl;
std::cout << "--dataset_to_pke_label_translation " << dataset_to_pke_label_translation.at("vehicle")  << std::endl;
  category_map = {
{ "unlabeled" , {"unlabeled","void", dataset_to_pke_label_translation.at("other")  }},
{ "ego vehicle", {"ego vehicle","void", dataset_to_pke_label_translation.at("other")  }},
{ "rectification border" , {"rectification border","void", dataset_to_pke_label_translation.at("other")  }},
{ "out of roi" , {"out of roi","void", dataset_to_pke_label_translation.at("other")  }},
{ "static" , {"static","void", dataset_to_pke_label_translation.at("other")  }},
{ "dynamic" , {"dynamic","void", dataset_to_pke_label_translation.at("other")  }},
{ "ground" , {"ground","void", dataset_to_pke_label_translation.at("other")  }},
{ "road" , {"road","flat", dataset_to_pke_label_translation.at("other")  }},
{ "sidewalk" , {"sidewalk","flat", dataset_to_pke_label_translation.at("other")  }},
{ "parking" , {"parking","flat", dataset_to_pke_label_translation.at("other")  }},
{ "rail track" , {"rail track","flat", dataset_to_pke_label_translation.at("other")  }},
{ "building" , {"building","construction", dataset_to_pke_label_translation.at("other")  }},
{ "wall" , {"wall","construction", dataset_to_pke_label_translation.at("other")  }},
{ "fence" , {"fence","construction", dataset_to_pke_label_translation.at("other")  }},
{ "guard rail" , {"guard rail","construction", dataset_to_pke_label_translation.at("other")  }},
{ "bridge" , {"bridge","construction", dataset_to_pke_label_translation.at("other")  }},
{ "tunnel" , {"tunnel","construction", dataset_to_pke_label_translation.at("other")  }},
{ "pole" , {"pole","object", dataset_to_pke_label_translation.at("other")  }},
{ "polegroup" , {"polegroup","object", dataset_to_pke_label_translation.at("other")  }},
{ "traffic light" , {"traffic light","object", dataset_to_pke_label_translation.at("other")  }},
{ "traffic sign" , {"traffic sign","object", dataset_to_pke_label_translation.at("other")  }},
{ "vegetation" , {"vegetation","nature", dataset_to_pke_label_translation.at("other")  }},
{ "terrain" , {"terrain","nature", dataset_to_pke_label_translation.at("other")  }},
{ "sky" , {"sky","sky", dataset_to_pke_label_translation.at("other")  }},
{ "person" , {"person","human", dataset_to_pke_label_translation.at("person")  }},
{ "rider" , {"rider","human", dataset_to_pke_label_translation.at("person")  }},
{ "car" , {"car","vehicle", dataset_to_pke_label_translation.at("vehicle")  }},
{ "truck" , {"truck","vehicle", dataset_to_pke_label_translation.at("vehicle")  }},
{ "bus" , {"bus","vehicle", dataset_to_pke_label_translation.at("vehicle")  }},
{ "caravan" , {"caravan","vehicle", dataset_to_pke_label_translation.at("vehicle")  }},
{ "trailer" , {"trailer","vehicle", dataset_to_pke_label_translation.at("vehicle")  }},
{ "train" , {"train","vehicle", dataset_to_pke_label_translation.at("vehicle")  }},
{ "motorcycle" , {"motorcycle","vehicle", dataset_to_pke_label_translation.at("vehicle")  }},
{ "bicycle" , {"bicycle","vehicle", dataset_to_pke_label_translation.at("vehicle")  }},
{ "license plate" , {"license plate","form_vehicle", dataset_to_pke_label_translation.at("other")  }}
 };

}

void coco_to_pke_images(PKEdataset::Dataset_data_type & pke_dataset,
                        rapidjson::Document & d, Parameters const & pars)
{

    std::map<int,Category_info> category_map;
    parse_coco_category(category_map, d["categories"]);

    const rapidjson::Value& images = d["images"];
    pke_dataset.set_level_name("meta");
    std::map<std::string,bool> images_with_annoatations;
    for (rapidjson::SizeType i = 0; i < images.Size(); i++) // Uses SizeType instead of size_t
    {
        std::string image_string_id = std::to_string(PKEdataset::rapid_to_val<int>(images[i],"id"));
        pke_dataset(image_string_id).set_level_name("objects");
        pke_dataset(image_string_id)
                    .insert("frame","coco_" + PKEdataset::rapid_to_val<std::string>(images[i],"file_name"))
                    .insert("tmp_orig_frame", pars.path_to_input_images_directory + "/" + PKEdataset::rapid_to_val<std::string>(images[i],"file_name"))
                    .insert("width",PKEdataset::rapid_to_val<int>(images[i],"width"))
                    .insert("height",PKEdataset::rapid_to_val<int>(images[i],"height"))
                    .insert("mask","coco_" + std::to_string(PKEdataset::rapid_to_val<int>(images[i],"id")) + ".png")
                    .insert("description",std::vector<std::string>());


        images_with_annoatations.emplace(image_string_id,false);
        if (pke_dataset.size() != images_with_annoatations.size())
        {
            std::cout << "###" << image_string_id << " " << pke_dataset.size()  << " " <<  images_with_annoatations.size() << std::endl;
            exit(-1);
        }
    }
    const rapidjson::Value& annotations = d["annotations"];
    for (rapidjson::SizeType i = 0; i < annotations.Size(); i++) // Uses SizeType instead of size_t
    {
        auto image_string_id = std::to_string(PKEdataset::rapid_to_val<int>(annotations[i],"image_id"));
        if (pke_dataset.has(image_string_id))
        {
            int category_id =  PKEdataset::rapid_to_val<int>(annotations[i],"category_id");
            int iscrowd = PKEdataset::rapid_to_val<int>(annotations[i],"iscrowd");
            //std::cout << " image_id " << image_id << " " << iscrowd << " " << std::endl;

            float area = PKEdataset::rapid_to_val<float>(annotations[i],"area");
            //float image_area = image_data_manager.get_element(image_string_id).area();
            float image_area = pke_dataset(image_string_id).get_value<int>("width")*pke_dataset(image_string_id).get_value<int>("height");
            std::string cat_name = category_map.at(category_id).name;
            std::string cat_super = category_map.at(category_id).supercategory;
            if (!iscrowd &&
                area > image_area*pars.min_area_relative_to_image &&
                (pars.supercategories.count("all") ||
                pars.supercategories.count(cat_super) ||
                pars.names.count(cat_name))

            )
            {
                images_with_annoatations.at(image_string_id) = true;

                pke_dataset(image_string_id,std::to_string(i))
                             .insert("object_class",int(category_map.at(category_id).pke_int_label))
                             .insert("bb",bb_ltwh_to_ltrb(PKEdataset::rapid_to_val<std::vector<float>>(annotations[i],"bbox")))
                             .insert("polygon",PKEdataset::rapid_to_val<std::vector<std::vector<float>>>(annotations[i],"segmentation"))
                             ;

            }
        }

    }
    int data_size = 0;
    std::cout << "images_with_annoatations " <<  images_with_annoatations.size() << " " << pke_dataset.size() << std::endl;

    for (auto & img_info : images_with_annoatations)
    {
        if (!img_info.second || data_size >= pars.max_number_of_images)
        {
            pke_dataset.erase(img_info.first);
        }
        else
            ++data_size;

    }
    if (pke_dataset.has_data("data_size"))
    {
        data_size += pke_dataset.get_value<int>("data_size");
    }
    pke_dataset.insert("data_size",data_size);


}
std::vector<std::vector<cv::Point>> int_to_point_polygon(std::vector<std::vector<float>> const & i_pol)
{
    std::vector<std::vector<cv::Point>> p_pol(i_pol.size());
    for (size_t i = 0; i < i_pol.size(); ++i)
    {
        for (size_t j = 0; j < i_pol[i].size() - 1; j += 2)
        {
            p_pol[i].push_back(cv::Point(i_pol[i][j],i_pol[i][j + 1]));
        }
    }
    return std::move(p_pol);
}

void draw_poly(cv::Mat & img, std::vector<std::vector<cv::Point>> const & p_poly)
{
    for (auto & poly : p_poly)
    {
        for (int i = 0; i < static_cast<int>(poly.size()); ++i)
        {
            cv::line(img,poly[i],poly[(i+1)%static_cast<int>(poly.size())],cv::Scalar(255,0,0),2);
        }
    }
}


struct Cityscape_data
{
    std::vector<std::vector<float>> pol;
    std::vector<float> bb;
    std::string label;
};

struct Cityscape_orig_file
{
    std::string city;
    std::string filename;
    std::string basename;
};


bool cityscape_get_detections_for_image(std::vector<Cityscape_data> & cityscape_data_vec, Cityscape_orig_file const & cityscape_orig_file,
                                        Parameters const & pars, int & imgWidth, int & imgHeight)
{


   std::string json_filename = pars.path_to_input_annotation_file + "/" +
                        cityscape_orig_file.city + "/" + cityscape_orig_file.basename + "gtFine_polygons.json";
   std::cout << " json_filename " << json_filename << std::endl;
   if (!std::experimental::filesystem::is_regular_file(json_filename))
      return false;

    std::cout << " json_filename " << json_filename << std::endl;
    FILE* pFile = fopen(json_filename.c_str(), "r");
    char buffer[65536];
    rapidjson::FileReadStream is(pFile, buffer, sizeof(buffer));
    rapidjson::Document data;
    std::cout << "Parsing... " << std::endl;
    data.ParseStream(is);
    fclose(pFile);

    imgWidth = PKEdataset::rapid_to_val<int>(data,"imgWidth");
    imgHeight = PKEdataset::rapid_to_val<int>(data,"imgHeight");
    //cv::Mat timg(imgHeight,imgWidth,CV_8UC3,cv::Scalar(0));
    //cv::Mat timg = cv::imread(pars.path_to_input_images_directory + "/"+cityscape_orig_file.city+"/"+ cityscape_orig_file.filename);
    if (data.HasMember("objects"))
    {
        const rapidjson::Value& objs = data["objects"];
        for (rapidjson::SizeType i = 0; i < objs.Size(); i++)
        {

            cityscape_data_vec.emplace_back();
            std::string label = PKEdataset::rapid_to_val<std::string>(objs[i],"label");
            //check if is a group
            if (label.size() > 4 && label.substr(label.size() - 5,5) == "group")
                cityscape_data_vec.back().label = label.substr(0,label.size() - 5);
            else
                cityscape_data_vec.back().label = label;

            std::cout << "label " << label << " " << cityscape_data_vec.back().label << std::endl;

            auto pol = PKEdataset::rapid_to_val<std::vector<std::vector<float>>>(objs[i],"polygon");

            auto & p = cityscape_data_vec.back().pol;
            auto & b = cityscape_data_vec.back().bb;
            b = {static_cast<float>(imgWidth),static_cast<float>(imgHeight),-1.f,-1.f};
            p = std::vector<std::vector<float>>(1);
            p[0] = std::vector<float>(pol.size()*2);
            int k = 0;
            for (auto & v : pol)
            {
                if (v.size() == 2)
                {
                    p[0][k] = v[0];
                    p[0][k+1] = v[1];
                    b[0] = std::min(v[0],b[0]);
                    b[1] = std::min(v[1],b[1]);
                    b[2] = std::max(v[0],b[2]);
                    b[3] = std::max(v[1],b[3]);
                    k+=2;
                }
                else
                {
                    std::cerr << "v size is not 2 " << std::endl;
                    exit(-1);
                }
            }
            auto p_pol = int_to_point_polygon(p);
        }
    }

    //cv::imshow("test",timg);
    //cv::waitKey(10);
    return !cityscape_data_vec.empty();
    /*
    //cv::floodFill(gtimg_orig, cv::Point(x,y),1, &(rec_vet.back()), 100, 100,8);
    */
    /*cv::Mat maskorig = imread(mask_filename);
    cv::Mat mask(maskorig.size(),CV_8UC1);
    for (int y = 0; y < maskorig.rows; ++y)
    {
        unsigned char *maskorig_ptr = maskorig.ptr<unsigned char>(y);
        unsigned char *mask_ptr = maskorig.ptr<unsigned char>(y);

        for (int x = 0; x < maskorig.cols; ++x, ++maskorig_ptr, ++mask_ptr)
        {

            if (*maskorig_ptr)
                *mask_ptr = label_converter[*maskorig_ptr];
            else
                *mask_ptr = 0;
        }
    }*/

    //cv::findContours   (   InputOutputArray    image,
  //  OutputArrayOfArrays    contours,
  //  OutputArray    hierarchy,
  //  int    mode,
  //  int    method,
  //  Point    offset = Point()
  //)
  //for (each_contour)
    //    calc_bounding_box();

}


void cityscape_get_list_of_images(std::string const & orig_images_split_dir,
                                  std::vector<Cityscape_orig_file> & cityscape_orig_files
                                 )
{
    cityscape_orig_files.clear();
    for(auto& p: std::experimental::filesystem::directory_iterator(orig_images_split_dir))
    {
        if (std::experimental::filesystem::is_directory(p))
        {

            for(auto& i: std::experimental::filesystem::directory_iterator(p))
            {

                if (std::experimental::filesystem::is_regular_file(i))
                {
                    cityscape_orig_files.emplace_back();
                    cityscape_orig_files.back().city = p.path().filename().string();
                    cityscape_orig_files.back().filename = i.path().filename().string();

                    std::cout << "file " << cityscape_orig_files.back().filename << std::endl;
                    size_t pos = 0;
                    int counter = 0;
                    while (pos != std::string::npos && counter < 3)
                    {
                        pos = cityscape_orig_files.back().filename.find("_",pos+1);
                       ++counter;
                    }
                    if (counter == 3)
                    {
                        cityscape_orig_files.back().basename = cityscape_orig_files.back().filename.substr(0,pos+1);
                    }
                    else
                    {
                        std::cerr << "could not extract basename " << pos << " " << counter << " " << cityscape_orig_files.back().filename << std::endl;
                    }
                }
            }
        }
    }
}


void cityscape_to_pke_images(PKEdataset::Dataset_data_type & pke_dataset,Parameters const & pars,
                        std::vector<Cityscape_orig_file> const & cityscape_orig_files
)
{

    int data_size = 0;
    std::map<std::string,Category_info> category_map;
    parse_city_category(category_map);

    pke_dataset.set_level_name("meta");
    for (size_t j = 0; j < cityscape_orig_files.size() && static_cast<int>(j) < pars.max_number_of_images; ++j) // image list already looped the directories
    {
        auto & cityscape_orig_file = cityscape_orig_files[j];
        std::string image_string_id = cityscape_orig_file.filename;
        std::string tmp_input_subdir = cityscape_orig_file.city;

        bool first_object_of_image = true;
        std::vector<Cityscape_data> cityscape_data_vec;
        int imgWidth = 0;
        int imgHeight = 0;
        cityscape_get_detections_for_image(cityscape_data_vec,cityscape_orig_file,pars,imgWidth,imgHeight);
        float image_area = imgWidth*imgHeight;
        int i = 0;
        for (auto & cityscape_data : cityscape_data_vec)
        {

            std::cout << "cityscape_to_pke_images label " << cityscape_data.label << std::endl;
            
            float area = (cityscape_data.bb.size() != 4?0:cityscape_data.bb[2]*cityscape_data.bb[3]);
            std::string cat_name = category_map.at(cityscape_data.label).name;
            std::string cat_super = category_map.at(cityscape_data.label).supercategory;
            //std::cout << "cat_super " << cat_super << " " << pars.supercategories.count(cat_super) <<  " " << category_map.at(cityscape_data.label).pke_int_label << std::endl;
            if (area > image_area*pars.min_area_relative_to_image &&
                (pars.supercategories.count("all") ||
                pars.supercategories.count(cat_super) ||
                pars.names.count(cat_name))

            )
            {
                if (first_object_of_image)
                {
                    pke_dataset(image_string_id).set_level_name("objects");
                    pke_dataset(image_string_id)
                    .insert("frame","city_" + std::string(image_string_id))
                    .insert("tmp_orig_frame",pars.path_to_input_images_directory + "/" + std::string(tmp_input_subdir) + "/" + std::string(image_string_id))
                    .insert("tmp_input_subdir",std::string(tmp_input_subdir))
                    .insert("width",imgWidth)
                    .insert("height",imgHeight)
                    .insert("mask","city_" + std::string(cityscape_orig_file.basename + "mask.png"))
                    .insert("description",std::vector<std::string>());
                    first_object_of_image = false;
                    ++data_size;
                }
                pke_dataset(image_string_id,std::to_string(i))
                            .insert("object_class",category_map.at(cityscape_data.label).pke_int_label)
                            .insert("bb",cityscape_data.bb)
                            .insert("polygon",cityscape_data.pol);
                ++i;
            }
        }

    }
    if (pke_dataset.has_data("data_size"))
    {
        data_size += pke_dataset.get_value<int>("data_size");
    }
    pke_dataset.insert("data_size",data_size);

}

void get_coco_parameters(Parameters & pars, std::string const & config_file_name)
{
    FILE* pFile = fopen(config_file_name.c_str(), "r");
    char buffer[65536];
    rapidjson::FileReadStream is(pFile, buffer, sizeof(buffer));
    rapidjson::Document data;
    data.ParseStream(is);
    fclose(pFile);

    pars.path_to_input_annotation_file = data["path_to_input_annotation_file"].GetString();
    pars.path_to_input_images_directory = data["path_to_input_images_directory"].GetString();
    pars.path_to_create_dataset = data["path_to_create_dataset"].GetString();
    pars.sequence_name = data["sequence_name"].GetString();
    pars.max_number_of_images = data["max_number_of_images"].GetInt();
    pars.min_area_relative_to_image = data["min_area_relative_to_image"].GetFloat();
    pars.use_only_annotations_with_polygon = data["use_only_annotations_with_polygon"].GetInt();
    pars.display_images = data["display_images"].GetInt();
    pars.needs_masks = data["needs_masks"].GetBool();

    const rapidjson::Value& supercategories = data["supercategories"];
    for (rapidjson::SizeType i = 0; i < supercategories.Size(); i++)
        pars.supercategories.emplace(supercategories[i].GetString());


    const rapidjson::Value& names = data["names"];
    for (rapidjson::SizeType i = 0; i < names.Size(); i++)
        pars.names.emplace(names[i].GetString());

}

void get_cityscape_parameters(Parameters & pars, std::string const & config_file_name)
{
    FILE* pFile = fopen(config_file_name.c_str(), "r");
    char buffer[65536];
    rapidjson::FileReadStream is(pFile, buffer, sizeof(buffer));
    rapidjson::Document data;
    data.ParseStream(is);
    fclose(pFile);

    pars.path_to_input_annotation_file = data["path_to_input_annotation_file"].GetString();
    pars.path_to_input_images_directory = data["path_to_input_images_directory"].GetString();
    pars.path_to_create_dataset = data["path_to_create_dataset"].GetString();
    pars.sequence_name = data["sequence_name"].GetString();
    pars.max_number_of_images = data["max_number_of_images"].GetInt();
    pars.min_area_relative_to_image = data["min_area_relative_to_image"].GetFloat();
    pars.use_only_annotations_with_polygon = data["use_only_annotations_with_polygon"].GetInt();
    pars.display_images = data["display_images"].GetInt();
    pars.needs_masks = data["needs_masks"].GetBool();

    const rapidjson::Value& supercategories = data["supercategories"];
    for (rapidjson::SizeType i = 0; i < supercategories.Size(); i++)
        pars.supercategories.emplace(supercategories[i].GetString());


    const rapidjson::Value& names = data["names"];
    for (rapidjson::SizeType i = 0; i < names.Size(); i++)
        pars.names.emplace(names[i].GetString());

}
void get_pke_parameters(Parameters & pars, std::string const & config_file_name)
{
    FILE* pFile = fopen(config_file_name.c_str(), "r");
    char buffer[65536];
    rapidjson::FileReadStream is(pFile, buffer, sizeof(buffer));
    rapidjson::Document data;
    data.ParseStream(is);
    fclose(pFile);

    pars.path_to_create_dataset = data["path_to_create_dataset"].GetString();
    pars.path_to_input_dataset = data["path_to_input_dataset"].GetString();
    pars.path_to_input_images_directory = "nodirneeded?";

    pars.sequence_name = "no_name_needed";
    pars.max_number_of_images = data["max_number_of_images"].GetInt();
    pars.min_area_relative_to_image = data["min_area_relative_to_image"].GetFloat();
    pars.use_only_annotations_with_polygon = data["use_only_annotations_with_polygon"].GetInt();
    pars.display_images = data["display_images"].GetInt();
    pars.needs_masks = data["needs_masks"].GetBool();

    const rapidjson::Value& supercategories = data["supercategories"];
    for (rapidjson::SizeType i = 0; i < supercategories.Size(); i++)
        pars.supercategories.emplace(supercategories[i].GetString());


    const rapidjson::Value& names = data["names"];
    for (rapidjson::SizeType i = 0; i < names.Size(); i++)
        pars.names.emplace(names[i].GetString());

}



void to_pke_write_data(PKEdataset::Dataset_data_type & pke_dataset,
                            Parameters const & pars)
{
    std::string dst_images_dir = pars.path_to_create_dataset + "/data_files/" + pars.sequence_name;
    std::string dst_masks_dir = pars.path_to_create_dataset + "/data_files/masks/" + pars.sequence_name;
    std::string dst_json_dir = pars.path_to_create_dataset + "/data_files/json";
    std::experimental::filesystem::create_directories(dst_images_dir);
    std::experimental::filesystem::create_directories(dst_masks_dir);
    std::experimental::filesystem::create_directories(dst_json_dir);
    

    for (auto  & data_seq : pke_dataset )
    {
        std::cout << data_seq.first << std::endl;
        int counter = 0;
        for (auto  & data_img : data_seq.second )
        {
            auto image_filename = data_img.second.get_value<std::string>("frame");
            auto orig_filename = image_filename;
            /*
            if (data_img.second.has_data("tmp_orig_frame"))
            {
                orig_filename = data_img.second.get_value<std::string>("tmp_orig_frame");
                data_img.second.erase_data("tmp_orig_frame");
            }



            std::string input_sub_dir = "";
            if (data_img.second.has_data("tmp_input_subdir"))
            {
                input_sub_dir = data_img.second.get_value<std::string>("tmp_input_subdir");
                data_img.second.erase_data("tmp_input_subdir");
            }

            auto orig_image_full_path = pars.path_to_input_images_directory + "/" + input_sub_dir + "/" + orig_filename;
            */
            auto orig_image_full_path = data_img.second.get_value<std::string>("tmp_orig_frame");
            std::cout << "counter " << counter++ << " data_img.first " << data_img.first << " " << image_filename << " " << orig_image_full_path << std::endl;
            data_img.second.erase_data("tmp_orig_frame");

            std::cout <<  "orig_image_full_path" << orig_image_full_path << std::endl;
            cv::Mat img = cv::imread(orig_image_full_path, cv::IMREAD_COLOR);
            std::experimental::filesystem::copy(orig_image_full_path,
                                                dst_images_dir + "/" + image_filename,
                                                std::experimental::filesystem::copy_options::overwrite_existing);
            cv::Mat mask(img.rows,img.cols,CV_8UC1,cv::Scalar(0));

            for (auto const & obj : data_img.second )
            {
                //std::cout << "  " << obj.first << std::endl;
                auto bbec = obj.second.get_value<std::vector<float>>("bb");
                cv::Rect rec(bbec[0],bbec[1],std::max(1.f,bbec[2] - bbec[0] + 0.5f),std::max(1.f,bbec[3] - bbec[1] + 0.5f));
                
                cv::rectangle(img,rec,cv::Scalar(0,0,255),2);

                auto seg_poly = obj.second.get_value<std::vector<std::vector<float>>>("polygon");
                auto p_pol = int_to_point_polygon(seg_poly);
                cv::fillPoly(mask,p_pol,cv::Scalar(obj.second.get_value<int>("object_class") + 1));
                draw_poly(img,p_pol);

            }
            if (pars.needs_masks)
                cv::imwrite(dst_masks_dir + "/" + data_img.second.get_value<std::string>("mask"),mask);

            if (pars.display_images)
            {
                cv::imshow("img",img);
                cv::Mat masktmp = mask.clone();
                for (int y = 0; y < masktmp.rows; ++y)
                {
                    unsigned char * masktmp_ptr = masktmp.ptr<unsigned char>(y);
                    for (int x = 0; x < masktmp.cols; ++x, ++masktmp_ptr)
                    {
                        if (*masktmp_ptr)
                            *masktmp_ptr = 255;
                    }
                }
                cv::imshow("mask",masktmp);
                cv::waitKey(pars.display_images>1?0:1);
            }
        }
    }
    pke_dataset.serialize(dst_json_dir + "/" +  pars.sequence_name + ".json");
    //pke_dataset.serialize();

}

void serialize_pke_labels(std::string const & file_name)
{
    std::ofstream ofs(file_name);
    ofs << "[";
    size_t c = 0;
    for (auto const & elem : dataset_to_pke_label_translation)
        ofs << "{\"" << elem.first << "\" : " << elem.second << "}" << (c++ <  dataset_to_pke_label_translation.size() -1?",":"");
    ofs << "]" << std::endl;
    ofs.close();

}
void serialize_category(rapidjson::Value const & data, std::string const & file_name)
{


    FILE* fp = fopen(file_name.c_str(), "w");
    char writeBuffer[65536];
    rapidjson::FileWriteStream os(fp, writeBuffer, sizeof(writeBuffer));
    rapidjson::Writer<rapidjson::FileWriteStream> writer(os);
    data.Accept(writer);
    fclose(fp);
}

void write_and_serialize_labels(PKEdataset::Dataset_data_type & pke_dataset,
                        Parameters const & pars)
{
    to_pke_write_data(pke_dataset,pars);
    serialize_pke_labels(pars.path_to_create_dataset + "/data_files/class_labels.json");
}

void insert_sequence_info(PKEdataset::Dataset_data_type & pke_dataset,
                         Parameters const & pars)
{
    auto path = std::experimental::filesystem::path(pars.path_to_create_dataset);
    std::string dir = path.filename().string();
    //std::cout << "testdir " << std::experimental::filesystem::path("/foo/bar/").filename() << std::endl;
    if (dir == "" || dir == "." || dir == "..")
        dir = path.parent_path().stem().string();

    std::cout << "dataset last dir " << dir << std::endl;
     
    pke_dataset
    .insert("name",std::string(pars.sequence_name))
    .insert("source",std::string("image"))
    .insert("source_path","/opt/www/shared_data/" + dir + "/data_files/" + pars.sequence_name)
    .insert("rel_source_path","data_files/" + pars.sequence_name)
    .insert("source_md5",std::string())
    .insert("description",std::vector<std::string>())
    .insert("info",std::string("subset of coco val2017"))
    .insert("width",0)
    .insert("height",0)
    .insert("mask_path",(pars.needs_masks?"data_files/masks/" + pars.sequence_name:""));
    std::cout << "pars.sequence_name " << pars.sequence_name << std::endl;
}


void cityscape_to_pke_image_dataset(PKEdataset::Dataset_data_type & pke_dataset,
                               Parameters const & pars)
{
    



    
    std::vector<Cityscape_orig_file> cityscape_orig_files;
    cityscape_get_list_of_images(pars.path_to_input_images_directory,cityscape_orig_files);

    cityscape_to_pke_images(pke_dataset,pars,cityscape_orig_files);
    
    
    

}



void coco_to_pke_image_dataset(PKEdataset::Dataset_data_type & pke_dataset,
                                Parameters const & pars)
{
   


    


    FILE* pFile = fopen(pars.path_to_input_annotation_file.c_str(), "r");
    char buffer[65536];
    rapidjson::FileReadStream is(pFile, buffer, sizeof(buffer));
    rapidjson::Document data;
    std::cout << "Parsing... " << std::endl;
    data.ParseStream(is);
    fclose(pFile);



std::cout << "pke_dataset(seq1) " << pke_dataset.size() << std::endl;
    coco_to_pke_images(pke_dataset,data,pars);





}
void coco_citiy_to_pke_image_dataset(PKEdataset::Dataset_data_type & pke_dataset,
                                    std::string const & coco_config_file_name,
                                    std::string const & city_config_file_name)
{
    Parameters coco_pars;
    get_coco_parameters(coco_pars,coco_config_file_name);
    Parameters city_pars;
    get_cityscape_parameters(city_pars,city_config_file_name);
    
    insert_sequence_info(pke_dataset.set_level_name("sequences")("seq1"),coco_pars);

    coco_to_pke_image_dataset(pke_dataset("seq1"),coco_pars);
    cityscape_to_pke_image_dataset(pke_dataset("seq1"),city_pars);
    write_and_serialize_labels(pke_dataset,coco_pars);

}



void new_view_images_dataset(PKEdataset::Dataset_data_type  & pke_dataset,
                             Parameters const & pars)
{


    for (auto const & data_seq : pke_dataset )
    {
        std::cout << data_seq.first << " " << data_seq.second.next_level_size() << " " << data_seq.second.data_size() << std::endl;
        std::string seq_name = data_seq.second.get_value<std::string>("name");

        std::string mask_directory = (pars.needs_masks?data_seq.second.get_value<std::string>("mask_path"):"no_mask");    
        std::string video = data_seq.second.get_value<std::string>("rel_source_path");
        for (auto const & data_img : data_seq.second )
        {
            auto image_filename = data_img.second.get_value<std::string>("frame");
            auto mask_filename = data_img.second.get_value<std::string>("mask");


            cv::Mat img = cv::imread(pars.path_to_input_dataset + "/" + video + "/" + image_filename, cv::IMREAD_COLOR );
            cv::Mat mask;
            if (pars.needs_masks)
                mask = cv::imread(pars.path_to_input_dataset + "/" + mask_directory + "/" + mask_filename,  cv::IMREAD_GRAYSCALE);

            std::cout << pars.path_to_input_dataset + "/" + video + "/" + image_filename << " " << img.rows << std::endl;
            std::cout << pars.path_to_input_dataset + "/" + mask_directory + "/" + mask_filename  << " " << mask.rows << std::endl;


            for (auto const & obj : data_img.second)
            {
                std::cout << "  " << obj.first << std::endl;
                auto bbec = obj.second.get_value<std::vector<float>>("bb");
                cv::Rect rec(bbec[0],bbec[1],std::max(1.f,bbec[2] - bbec[0] + 0.5f),std::max(1.f,bbec[3] - bbec[1] + 0.5f));
                cv::rectangle(img,rec,cv::Scalar(0,0,255),2);

                auto seg_poly = obj.second.get_value<std::vector<std::vector<float>>>("polygon");
                std::cout << "seg_poly" << seg_poly.size() << std::endl;;
                auto p_pol = int_to_point_polygon(seg_poly);
                if (pars.needs_masks)
                    cv::fillPoly(mask,p_pol,cv::Scalar(obj.second.get_value<int>("object_class")));
                draw_poly(img,p_pol);
                cv::putText(img, std::to_string(obj.second.get_value<int>("object_class")), rec.tl(),
                            cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, cv::Scalar(255), 1, cv::LINE_AA);


            }

            if (pars.display_images)
            {
                cv::imshow("img",img);
                if (pars.needs_masks)
                {
                    cv::Mat masktmp = mask.clone();
                    for (int y = 0; y < masktmp.rows; ++y)
                    {
                        unsigned char * masktmp_ptr = masktmp.ptr<unsigned char>(y);
                        for (int x = 0; x < masktmp.cols; ++x, ++masktmp_ptr)
                        {
                            if (*masktmp_ptr)
                                *masktmp_ptr = 255;
                        }
                    }
                    cv::imshow("mask",masktmp);
                }
                cv::waitKey(pars.display_images>1?0:1);

            }
        }
    }


}

void draw_vec_bb(cv::Mat & img, std::vector<int> const & bb_vec, cv::Scalar const & color = cv::Scalar(0,0,255))
{
    cv::Rect rec(bb_vec[0],bb_vec[1],bb_vec[2],bb_vec[3]);
    cv::rectangle(img,rec,color,2);
}

void new_view_trajectory_dataset(PKEdataset::Dataset_data_type const & pke_dataset,
                                 Parameters const & pars)
{
    cv::Mat img;


    for (auto const & data_seq : pke_dataset )
    {
        auto const & trj_seq = data_seq.second;

        //std::map<frame_number,std::set<trajectory_id> >
        std::map<std::string,std::set<std::string> > frame_annot_ref;
        for (auto const & data_tra : trj_seq )
        {
            for (auto const & obs : data_tra.second)
            {
                auto frame_number = obs.second.get_value<std::string>("frame");
                frame_annot_ref[frame_number].emplace(data_tra.first);
            }

        }


        std::cout << "pars.path_to_input_dataset + traj_seq.get_video() " 
                  << pars.path_to_input_dataset + "/" + trj_seq.get_value<std::string>("rel_source_path") << std::endl;
        cv::VideoCapture cap(pars.path_to_input_dataset + "/" + trj_seq.get_value<std::string>("rel_source_path"));
        if (cap.isOpened())
        {


            int frame = 1;
            while (cap.read(img))
            {
                std::string string_frame_number = std::to_string(frame);

                bool got_one = false;
                if (frame_annot_ref.count(string_frame_number))
                {
                    for (auto & str_trajectory_id : frame_annot_ref.at(string_frame_number))
                    {

                        std::cout << "str_trajectory_id " << str_trajectory_id << std::endl;
                        PKEdataset::Dataset_data_type const & obs = trj_seq(str_trajectory_id,string_frame_number);
                        got_one = true;

                        draw_vec_bb(img,trj_seq(str_trajectory_id).get_value<std::vector<int>>("trajectory_bounding_box"),cv::Scalar(0,255,0));
                        draw_vec_bb(img,obs.get_value<std::vector<int>>("bb"));


                        cv::Point p1(-1,-1);
                        for (auto const & iobs : trj_seq(str_trajectory_id))
                        {

                            cv::Point p2 (iobs.second.get_value<std::vector<int>>("position_image")[0],
                                          iobs.second.get_value<std::vector<int>>("position_image")[1]);

                            if (p1.x > 0)
                                cv::line(img,p1,p2,cv::Scalar(255,0,0),2);
                            std::swap(p1,p2);
                        }


                    }
                }



                std::cout << "frame " << std::endl;
                cv::imshow("img",img);
                cv::waitKey(got_one?0:1);
                ++frame;
            }
        }

    }
}



void new_parse_pke_image_dataset(PKEdataset::Dataset_data_type & pke_dataset,
                                Parameters & pars, std::string const & config_file_name)
{




    get_pke_parameters(pars,config_file_name);
    std::string json_dir = pars.path_to_input_dataset + "/data_files/json/";
    int idx = 0;
    for(auto& json_file_name: std::experimental::filesystem::directory_iterator(json_dir))
    {
        PKEdataset::deserialize_pke_image_dataset(pke_dataset,std::to_string(idx),json_file_name.path().string());
        ++idx;
    }

}




void new_parse_pke_trajectory_dataset(PKEdataset::Dataset_data_type & pke_dataset,
                                     Parameters & pars, std::string const & config_file_name)
{

    get_pke_parameters(pars,config_file_name);
    std::string json_dir = pars.path_to_input_dataset + "/data_files/json/";
    int idx = 0;
    for(auto& json_file_name: std::experimental::filesystem::directory_iterator(json_dir))
    {
        PKEdataset::deserialize_pke_trajectory_dataset(pke_dataset,std::to_string(idx),json_file_name.path().string());
        ++idx;
    }

}
int main( int argc, char ** argv)
{

    if (argc > 1)
    {

        

        if (argc == 3 && std::string(argv[2]) == std::string("coco"))
        {
            Parameters pars;
            get_coco_parameters(pars,argv[1]);
            PKEdataset::Dataset_data_type pke_dataset;
            insert_sequence_info(pke_dataset.set_level_name("sequences")("seq1"),pars);
            coco_to_pke_image_dataset(pke_dataset("seq1"),pars);
            write_and_serialize_labels(pke_dataset,pars);

        }
        else if (argc == 3 && std::string(argv[2]) == std::string("cityscape"))
        {
            Parameters pars;
            get_cityscape_parameters(pars,argv[1]);
            PKEdataset::Dataset_data_type pke_dataset;
            insert_sequence_info(pke_dataset.set_level_name("sequences")("seq1"),pars);
            cityscape_to_pke_image_dataset(pke_dataset("seq1"),pars);
            write_and_serialize_labels(pke_dataset,pars);

        }
        else if (argc == 3 && std::string(argv[2]) == std::string("img_vis"))
        {
            Parameters pars;
            PKEdataset::Dataset_data_type pke_dataset;
            new_parse_pke_image_dataset(pke_dataset,pars,argv[1]);
            new_view_images_dataset(pke_dataset,pars);

        }
        else if (argc == 3 && std::string(argv[2]) == std::string("trj_vis"))
        {
            Parameters pars;
            PKEdataset::Dataset_data_type pke_dataset;
            new_parse_pke_trajectory_dataset(pke_dataset,pars,argv[1]);
            new_view_trajectory_dataset(pke_dataset,pars);
        }
        else if (argc == 4 && std::string(argv[3]) == std::string("coco_city"))
        {
            PKEdataset::Dataset_data_type pke_dataset;
            coco_citiy_to_pke_image_dataset(pke_dataset,argv[1],argv[2]);
        }
        else
        {
            std::cout << "error usage " << argv[0] << " parfile <coco | img_vis | trj_vis | cityscape >" << std::endl;
            std::cout << "           " << argv[0] << " parfile1 parfile2 coco_city" << std::endl;
        }

        return 0;


    }
    else
    {
        std::cout << "usage: " << argv[0] << " >coco img_vis trj_vis>  configfile.json" << std::endl;
    }
    return 0;

}
