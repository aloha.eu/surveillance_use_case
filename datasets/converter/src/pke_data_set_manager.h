#pragma once

#include "rapidjson/document.h"
#include <rapidjson/writer.h>
#include "rapidjson/filewritestream.h"
#include "rapidjson/filereadstream.h"
#include <rapidjson/prettywriter.h>
#include <rapidjson/stringbuffer.h>

#include <iostream>
#include <vector>
#include <map>
#include <limits>
#include <memory>
#include <cmath>



namespace PKEdataset
{

    template< typename T >
    std::string to_json(std::string const & name, T const & v)
    {
        return "\"" + name + "\":" + std::to_string(v);
    }

    template< typename T , typename std::enable_if<std::is_class<T>::value >::type* = nullptr>
    std::string to_json_v(std::string const & name, std::vector <T> const & v)
    {
        /*
         *    file << "[" << std::endl;
         *    for (auto & v : m_trajectories)
         *    {
         *        //file << trajectory.deserialize();
    }
    file << "]" << std::endl;
    */
        std::cout << "#############is_class " << std::endl;
        return "";
    }

    template< typename T , typename std::enable_if<!std::is_class<T>::value >::type* = nullptr>
    std::string to_json_v(std::string const & name, std::vector <T> const & v)
    {
        /*
         *    file << "[" << std::endl;
         *    for (auto & v : m_trajectories)
         *    {
         *        //file << trajectory.deserialize();
    }
    file << "]" << std::endl;
    */
        std::cout << "#############no_class " << std::endl;
        return "";


    }

    /*template< typename J, typename V >
     * V rapid_to_val(J const & d, std::string const & name)
     * {
     *    std::cout << "rapid_to_val no specialization for type" << std::endl;
     *    exit(1);
}*/
    template< typename V , typename J >
    inline typename std::enable_if<std::is_same<V, std::string>::value, std::string>::type
    rapid_to_val(J const & d, std::string const & name)
    {
        if (d.HasMember(name.c_str()) && d[name.c_str()].IsString())
            return d[name.c_str()].GetString();
        else
        {
            std::cout << "error rapid_to_val d has no element called " << name << " or it has a not expected type std::string" << std::endl;
            exit(1);
        }
        return std::string();
    }

    template< typename V , typename J >
    inline typename std::enable_if<std::is_same<V, bool>::value, bool>::type
    rapid_to_val(J const & d, std::string const & name)
    {
        if (d.HasMember(name.c_str()) && d[name.c_str()].IsBool())
            return d[name.c_str()].GetBool();
        else
        {
            std::cout << "error rapid_to_val d has no element called " << name  << " or it has a not expected type bool" << std::endl;
            exit(1);
        }
        return false;
    }


    template< typename V , typename J >
    inline typename std::enable_if<std::is_same<V, int>::value, int>::type
    rapid_to_val(J const & d, std::string const & name)
    {
        if (d.HasMember(name.c_str()) && d[name.c_str()].IsInt())
            return d[name.c_str()].GetInt();
        else
        {
            std::cout << "error rapid_to_val d has no element called " << name << " or it has a not expected type int " << d.HasMember(name.c_str()) << std::endl;
            exit(1);
        }
        return 0;
    }

    template< typename V , typename J >
    inline typename std::enable_if<std::is_same<V, uint64_t>::value, uint64_t>::type
    rapid_to_val(J const & d, std::string const & name)
    {
        if (d.HasMember(name.c_str()))
            return d[name.c_str()].GetUint64();
        else
        {
            std::cout << "error rapid_to_val d has no element called " << name << " or it has a not expected type uint64_t " << std::endl;
            exit(1);
        }
        return 0;
    }
    template< typename V , typename J >
    inline typename std::enable_if<std::is_same<V, int64_t>::value, int64_t>::type
    rapid_to_val(J const & d, std::string const & name)
    {
        if (d.HasMember(name.c_str()))
            return d[name.c_str()].GetInt64();
        else
        {
            std::cout << "error rapid_to_val d has no element called " << name << " or it has a not expected type int64_t "  << std::endl;
            exit(1);
        }
        return 0;
    }

    /*template< typename V , typename J >
    inline typename std::enable_if<std::is_same<V, long unsigned int>::value, long unsigned int>::type
    rapid_to_val(J const & d, std::string const & name)
    {
        if (d.HasMember(name.c_str()))
            return d[name.c_str()].GetUint64();
        else
        {
            std::cout << "error rapid_to_val d has no element called " << name << std::endl;
            exit(1);
        }
        return 0;
    }*/

    template< typename V , typename J >
    inline typename std::enable_if<std::is_same<V, float>::value, float>::type
    rapid_to_val(J const & d, std::string const & name)
    {
        if (d.HasMember(name.c_str()) && (d[name.c_str()].IsFloat() || d[name.c_str()].IsInt()))
            return d[name.c_str()].GetFloat();
        else
        {
            std::cout << "error rapid_to_val d has no element called " << name << " or it has a not expected type float "
            << d[name.c_str()].IsFloat()
            << d.HasMember(name.c_str())
            << d[name.c_str()].IsInt()
            << std::endl;
            exit(1);
        }
        return 0.f;
    }

    template< typename V , typename J >
    inline typename std::enable_if<std::is_same<V, std::vector<int>>::value, V>::type
    rapid_to_val(J const & d, std::string const & name)
    {
        if (d.HasMember(name.c_str()) && d[name.c_str()].IsArray())
        {
            const rapidjson::Value& arr = d[name.c_str()];

            V vec(arr.Size());
            for (rapidjson::SizeType i = 0; i < arr.Size(); i++) // Uses SizeType instead of size_t
            {
                if (arr[i].IsInt())
                    vec[i] = arr[i].GetInt();
                else
                {
                    std::cout << "wrong array type for " << name  << std::endl;
                }
            }
            return vec;


        }
        else
        {
            std::cout << "error rapid_to_val d has no element called " << name << " or it has a not expected type " << std::endl;
            exit(1);
        }
        return V();
    }

    /*template< typename V , typename J >
    inline typename std::enable_if<std::is_same<V, std::vector<float>>::value, std::vector<int>>::type
    rapid_to_val(J const & d, std::string const & name)
    {
        if (d.HasMember(name.c_str()) && d[name.c_str()].IsArray())
        {
            const rapidjson::Value& arr = d[name.c_str()];

            std::vector<int> vec(arr.Size());
            for (rapidjson::SizeType i = 0; i < arr.Size(); i++) // Uses SizeType instead of size_t
            {
                if (arr[i].IsFloat())
                    vec[i] = arr[i].GetFloat();
                else
                {
                    std::cout << "wrong array type for " << name  << std::endl;
                }
            }
            return vec;


        }
        else
        {
            std::cout << "error rapid_to_val d has no element called " << name << " or it has a not expected type " << std::endl;
            exit(1);
        }
        return std::vector<int>();
    }*/

    template< typename V , typename J >
    inline typename std::enable_if<std::is_same<V, std::vector<float>>::value, V>::type
    rapid_to_val(J const & d, std::string const & name)
    {
        if (d.HasMember(name.c_str()) && d[name.c_str()].IsArray())
        {
            const rapidjson::Value& arr = d[name.c_str()];

            V vec(arr.Size());
            for (rapidjson::SizeType i = 0; i < arr.Size(); i++) // Uses SizeType instead of size_t
            {
                if (arr[i].IsFloat())
                    vec[i] = arr[i].GetFloat();
                else if (arr[i].IsInt())
                    vec[i] = arr[i].GetInt();
                else
                {
                    std::cout << "wrong array type for " << name  << std::endl;
                }
            }
            return vec;


        }
        else
        {
            std::cout << "error rapid_to_val d has no element called " << name << " or it has a not expected type " << std::endl;
            exit(1);
        }
        return V();
    }

    template< typename V , typename J >
    inline typename std::enable_if<std::is_same<V, std::vector<std::string>>::value, V>::type
    rapid_to_val(J const & d, std::string const & name)
    {
        if (d.HasMember(name.c_str()) && d[name.c_str()].IsArray())
        {
            const rapidjson::Value& arr = d[name.c_str()];

            V vec(arr.Size());
            for (rapidjson::SizeType i = 0; i < arr.Size(); i++) // Uses SizeType instead of size_t
            {
                if (arr[i].IsString())
                    vec[i] = arr[i].GetString();
                else
                {
                    std::cout << "wrong array type for " << name  << std::endl;
                }
            }
            return vec;


        }
        else
        {
            std::cout << "error rapid_to_val d has no element called " << name << " or it has a not expected type " << std::endl;
            exit(1);
        }
        return V();
    }


    template< typename V , typename J >
    inline typename std::enable_if<std::is_same<V, std::vector<std::vector<int>>>::value, V>::type
    rapid_to_val(J const & d, std::string const & name)
    {
        if (d.HasMember(name.c_str()) && d[name.c_str()].IsArray())
        {
            const rapidjson::Value& arr = d[name.c_str()];

            V vecvec(arr.Size());
            for (rapidjson::SizeType i = 0; i < arr.Size(); i++) // Uses SizeType instead of size_t
            {
                for (rapidjson::SizeType j = 0; j < arr[i].Size(); j++) // Uses SizeType instead of size_t
                    vecvec[i].emplace_back(arr[i][j].GetInt());

            }
            return vecvec;


        }
        else
        {
            std::cout << "error rapid_to_val d has no element called " << name << " or it has a not expected type. is array " << d[name.c_str()].IsArray() << std::endl;
            exit(1);
        }
        return V();
    }

    template< typename V , typename J >
    inline typename std::enable_if<std::is_same<V, std::vector<std::vector<float>>>::value, std::vector<std::vector<float>> >::type
    rapid_to_val(J const & d, std::string const & name)
    {
        if (d.HasMember(name.c_str()) && d[name.c_str()].IsArray())
        {
            const rapidjson::Value& arr = d[name.c_str()];

            std::vector<std::vector<float>> vecvec(arr.Size());
            for (rapidjson::SizeType i = 0; i < arr.Size(); i++) // Uses SizeType instead of size_t
            {
                for (rapidjson::SizeType j = 0; j < arr[i].Size(); j++) // Uses SizeType instead of size_t
                {
                    float val = 0;
                    if (arr[i][j].IsFloat())
                        val = arr[i][j].GetFloat();
                    else if (arr[i][j].IsInt())
                        val = arr[i][j].GetInt();
                    else
                    {
                        std::cout << "wrong array type for " << name  << std::endl;
                    }
                    vecvec[i].emplace_back(val);
                }

            }
            return vecvec;


        }
        else
        {
            std::cout << "error rapid_to_val d has no element called " << name << " or it has a not expected type. is array " << d[name.c_str()].IsArray() << std::endl;
            exit(1);
        }
        return std::vector<std::vector<float>>();
    }


    template< typename JsonT,  typename VecT >
    inline typename std::enable_if<std::is_same<VecT, std::vector<int>>::value,void >::type
    add_vector(JsonT & d, VecT const & vec, std::string const & name,rapidjson::Document::AllocatorType& allocator)
    {
        d.AddMember(rapidjson::Value(name.c_str(),allocator).Move(),rapidjson::Value(rapidjson::kArrayType),allocator);
        for (auto const & e : vec)
        {
            d[name.c_str()].PushBack(e,allocator);
        }

    }
    template< typename JsonT,  typename VecT >
    inline typename std::enable_if<std::is_same<VecT, std::vector<float>>::value,void >::type
    add_vector(JsonT & d, VecT const & vec, std::string const & name,rapidjson::Document::AllocatorType& allocator)
    {
        d.AddMember(rapidjson::Value(name.c_str(),allocator).Move(),rapidjson::Value(rapidjson::kArrayType),allocator);
        for (auto const & e : vec)
        {
            d[name.c_str()].PushBack(e,allocator);
        }

    }

    template< typename JsonT,  typename VecT >
    inline typename std::enable_if<std::is_same<VecT, std::vector<std::string>>::value,void >::type
    add_vector(JsonT & d, VecT const & vec, std::string const & name, rapidjson::Document::AllocatorType& allocator)
    {
        d.AddMember(rapidjson::Value(name.c_str(),allocator).Move(),rapidjson::Value(rapidjson::kArrayType),allocator);
        for (auto const & e : vec)
        {
            d[name.c_str()].PushBack(rapidjson::Value(e.c_str(),allocator).Move(),allocator);
        }

    }

    template< typename JsonT,  typename VecT >
    inline typename std::enable_if<std::is_same<VecT, std::vector<std::vector<int>>>::value,void >::type
    add_vector(JsonT & d, VecT const & vec, std::string const & name, rapidjson::Document::AllocatorType& allocator)
    {
        d.AddMember(rapidjson::Value(name.c_str(),allocator).Move(),rapidjson::Value(rapidjson::kArrayType),allocator);
        for (auto const & e : vec)
        {
            rapidjson::Value v(rapidjson::kArrayType);
            for (auto const & ie : e)
            {
                v.PushBack(ie,allocator);
            }
            d[name.c_str()].PushBack(v,allocator);
        }

    }

    template< typename JsonT,  typename VecT >
    inline typename std::enable_if<std::is_same<VecT, std::vector<std::vector<float>>>::value,void >::type
    add_vector(JsonT & d, VecT const & vec, std::string const & name, rapidjson::Document::AllocatorType& allocator)
    {
        d.AddMember(rapidjson::Value(name.c_str(),allocator).Move(),rapidjson::Value(rapidjson::kArrayType),allocator);
        for (auto const & e : vec)
        {
            rapidjson::Value v(rapidjson::kArrayType);
            for (auto const & ie : e)
            {
                v.PushBack(ie,allocator);
            }
            d[name.c_str()].PushBack(v,allocator);
        }

    }

    enum Data_type_name { NOTYPE, INT, DOUBLE, FLOAT, UINT64, INT64, STRING, INTVEC, FLOATVEC, STRVEC, INTVECVEC, FLOATVECVEC };

    template<typename T >
    inline Data_type_name Get_type_name()
    {
        return NOTYPE;
    };

    template< >
    inline Data_type_name Get_type_name<int>()
    {
        return INT;
    };
    template< >
    inline Data_type_name Get_type_name<float>()
    {
        return FLOAT;
    };
    template< >
    inline Data_type_name Get_type_name<uint64_t>()
    {
        return UINT64;
    };
    template< >
    inline Data_type_name Get_type_name<int64_t>()
    {
        return INT64;
    };

    template< >
    inline Data_type_name Get_type_name<std::vector<int>>()
    {
        return INTVEC;
    };

    template< >
    inline Data_type_name Get_type_name<std::vector<float>>()
    {
        return FLOATVEC;
    }


    template< >
    inline Data_type_name Get_type_name<std::vector<std::vector<int>>>()
    {
        return INTVECVEC;
    };

    template< >
    inline Data_type_name Get_type_name<std::vector<std::vector<float>>>()
    {
        return FLOATVECVEC;
    };


    template< >
    inline Data_type_name Get_type_name<std::vector<std::string>>()
    {
        return STRVEC;
    };

    template< >
    inline Data_type_name Get_type_name<double>()
    {
        return DOUBLE;
    };

    template< >
    inline Data_type_name Get_type_name<std::string>()
    {
        return STRING;
    };


    struct Data_element
    {
        Data_element(Data_type_name _type) : type(_type) {}
        Data_type_name type = NOTYPE;

    };



    template<typename T>
    struct Data_instance : public Data_element
    {
        Data_instance(T && _val) : Data_element(Get_type_name<T>()), val(std::move(_val)) {};
        static std::shared_ptr<Data_element> make_data_instance(T && val) { return std::make_shared<Data_instance<T>>(std::move(val));}
        T val;
    };




    struct Dataset_data_type
    {


        std::map<std::string, std::shared_ptr<Data_element> >  m_data;
        struct cmpLess {
            bool operator()(const std::string& a,
                            const std::string& b) const {
                try
                {
                    size_t pos = 0;
                    float af = std::stof(a, &pos);
                    if (pos >= a.length())
                    {
                        float bf = std::stof(b, &pos);
                        if (pos >= b.length())
                        {
                            return af < bf;
                        }
                    }

                }
                catch( ... )
                {

                }
                return a < b;
            }
        };
        std::map<std::string, Dataset_data_type, cmpLess >  m_next_level_data;
        std::string m_name = "test";

        typedef std::map<std::string, Dataset_data_type > Map_type;
        typedef Map_type::const_iterator Const_iter_type;
        typedef Map_type::iterator Iter_type;

        Const_iter_type begin() const { return m_next_level_data.begin();}
        Const_iter_type end() const { return m_next_level_data.end();}
        Iter_type begin()  { return m_next_level_data.begin();}
        Iter_type end()  { return m_next_level_data.end();}

        size_t size() const { return m_next_level_data.size(); }

        Iter_type find(std::string const & key) { return m_next_level_data.find(key); }

        size_t next_level_size() const { return m_next_level_data.size();}
        size_t data_size() const { return m_data.size();}


        template<typename T>
        T get_value(std::shared_ptr<Data_element> data) const
        {

            auto base = std::static_pointer_cast<Data_instance<T>>(data);
            if (!base)
            {
                std::cout << "get_value: wrong type" << std::endl;
                exit(1);
            }
            return base->val;

        }

        template<typename T>
        T get_value(std::string const & key) const
        {
            if (!m_data.count(key))
            {
                std::cout << "error get_value: no element " << key << std::endl;
                exit(1);
            }

            auto base = std::static_pointer_cast <Data_instance<T>>(m_data.at(key));
            if (!base)
            {
                std::cout << "get_value: wrong type" << std::endl;
                exit(1);
            }
            return base->val;


        }


        template<typename T>
        void set_value( T && val, std::string const & key)
        {
            if (!m_data.count(key))
            {
                m_data.emplace(key,Data_instance<T>::make_data_instance(std::move(val)));
            }
            else
            {
                auto base = std::static_pointer_cast <Data_instance<T>>(m_data[key]);
                if (!base)
                {
                    std::cout << "set_value: wrong type" << std::endl;
                    exit(1);
                }
                base->val = std::move(val);
            }
        }



        Dataset_data_type & set_level_name(std::string const & name)
        {
            m_name = name;
            return *this;
        }
        std::string  get_level_name()
        {
            return m_name;
        }

        template<typename Tkey, typename Tval>
        Dataset_data_type & insert(  Tkey const & key, Tval && val)
        {
            set_value(std::move(val),key);
            return *this;
        }


        template<typename Tkey>
        Dataset_data_type & erase(Tkey const & key)
        {
            m_next_level_data.erase(key);
            return *this;
        }
        
        template<typename Tkey>
        Dataset_data_type & erase_data(Tkey const & key)
        {
            m_data.erase(key);
            return *this;
        }
        template<typename Tkey>
        bool has(Tkey const & key) const
        {
            return m_next_level_data.count(key);
        }

        template<typename Tkey>
        bool has_data(Tkey const & key) const
        {
            return m_data.count(key);
        }

        Dataset_data_type & operator()()
        {
            return *this;
        }

        template<typename Tkey >
        Dataset_data_type & operator()(Tkey const & key)
        {
            return m_next_level_data[key];
        }
        template<typename Tkey, typename... Args >
        Dataset_data_type & operator()(Tkey const & key,Args... args)
        {
            return m_next_level_data[key](args...);
        }


        template<typename Tkey >
        const Dataset_data_type  & operator()(Tkey const & key) const
        {
            if (!m_next_level_data.count(key))
            {
                std::cout << " no element with key = " << key << std::endl;
                exit(1);
            }
            return m_next_level_data.at(key);
        }
        template<typename Tkey, typename... Args >
        const Dataset_data_type  & operator()(Tkey const & key,Args... args) const
        {
            if (!m_next_level_data.count(key))
            {
                std::cout << " no element with key = " << key << std::endl;
                exit(1);
            }
            return m_next_level_data.at(key)(args...);
        }


        template<typename Tval, typename Tkey>
        Tval get(Tkey const & key)
        {
            return get_value<Tval>(key);
        }

        template<typename Tval, typename Tkey, typename... Args >
        Tval get(Tkey const & key, Args... args)
        {
            return m_next_level_data[key].get<Tval>(args...);
        }

        void serialize_elem(rapidjson::Document & object, std::pair<std::string,std::shared_ptr<Data_element> > const & data,  rapidjson::Document::AllocatorType& allocator)
        {
            switch(data.second->type)
            {
                case INT:
                    object.AddMember(rapidjson::Value(data.first.c_str(),allocator).Move(),get_value<int>(data.first),allocator);
                    break;
                case DOUBLE:
                    object.AddMember(rapidjson::Value(data.first.c_str(),allocator).Move(),get_value<double>(data.first),allocator);
                    break;
                case FLOAT:
                    object.AddMember(rapidjson::Value(data.first.c_str(),allocator).Move(),get_value<float>(data.first),allocator);
                    break;
                case INT64:
                    object.AddMember(rapidjson::Value(data.first.c_str(),allocator).Move(),get_value<int64_t>(data.first),allocator);
                    break;
                case UINT64:
                    object.AddMember(rapidjson::Value(data.first.c_str(),allocator).Move(),get_value<uint64_t>(data.first),allocator);
                    break;
                case STRING:
                    object.AddMember(rapidjson::Value(data.first.c_str(),allocator).Move(),
                                     rapidjson::Value(get_value<std::string>(data.first).c_str(),allocator).Move(),allocator);
                    break;
                case INTVEC:
                    add_vector(object,get_value<std::vector<int>>(data.first),data.first,allocator);
                    break;
                case FLOATVEC:
                    add_vector(object,get_value<std::vector<float>>(data.first),data.first,allocator);
                    break;
                case STRVEC:
                    add_vector(object,get_value<std::vector<std::string>>(data.first),data.first,allocator);
                    break;
                case INTVECVEC:
                    add_vector(object,get_value<std::vector<std::vector<int>>>(data.first),data.first,allocator);
                    break;
                case FLOATVECVEC:
                    add_vector(object,get_value<std::vector<std::vector<float>>>(data.first),data.first,allocator);
                    break;
                case NOTYPE:
                    std::cout << "error serialize_elem, got a NOTYPE" << std::endl;
                    exit(1);
                    break;
            }
        }
        void serialize_data(rapidjson::Document & object,rapidjson::Document::AllocatorType& allocator)
        {
            if (object.IsObject())
            {
                for (auto & val : m_data)
                {
                    serialize_elem(object,val,allocator);
                }
            }
            else
            {
                std::cout << "error serialize_data expect object" << std::endl;

            }
        }
        void serialize_next_level(rapidjson::Value & arr_obj,rapidjson::Document::AllocatorType& allocator)
        {
            for (auto & val : m_next_level_data)
            {
                rapidjson::Document object(rapidjson::kObjectType);
                val.second.serialize(object,allocator);
                arr_obj.PushBack(object,allocator);
            }
        }

        void serialize(rapidjson::Document & object, rapidjson::Document::AllocatorType& allocator)
        {
            serialize_data(object,allocator);
            if (!m_next_level_data.empty())
            {
                object.AddMember(rapidjson::Value(m_name.c_str(),allocator).Move(),rapidjson::Value(rapidjson::kArrayType),allocator);
                serialize_next_level(object[m_name.c_str()],allocator);
            }

        }
        void serialize()
        {

            rapidjson::Document d(rapidjson::kArrayType);

            rapidjson::StringBuffer buffer;
            rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);

            if (m_next_level_data.size() == 1)
            {
                rapidjson::Document object(rapidjson::kObjectType);
                m_next_level_data.begin()->second.serialize(object,d.GetAllocator());
                object.Accept(writer);
            }
            else
            {
                rapidjson::Value doc_array(rapidjson::kArrayType);
                serialize_next_level(doc_array,d.GetAllocator());
                doc_array.Accept(writer);
            }






            std::cout << buffer.GetString() << std::endl;

        }
        void serialize(std::string const & file_name)
        {

            rapidjson::Document d(rapidjson::kArrayType);

            FILE* fp = fopen(file_name.c_str(), "w");
            char writeBuffer[65536];
            rapidjson::FileWriteStream os(fp, writeBuffer, sizeof(writeBuffer));
            rapidjson::Writer<rapidjson::FileWriteStream> writer(os);

            if (m_next_level_data.size() == 1)
            {
                rapidjson::Document object(rapidjson::kObjectType);
                m_next_level_data.begin()->second.serialize(object,d.GetAllocator());
                object.Accept(writer);

            }
            else
            {
                rapidjson::Value doc_array(rapidjson::kArrayType);
                serialize_next_level(doc_array,d.GetAllocator());
                doc_array.Accept(writer);
            }




            fclose(fp);

        }



    };


    inline void deserialize_image_sequence(PKEdataset::Dataset_data_type & pke_dataset,
                                           rapidjson::Value & document)
    {
        pke_dataset.set_level_name("meta")
        .insert("name",PKEdataset::rapid_to_val<std::string>(document,"name"))
        .insert("source",PKEdataset::rapid_to_val<std::string>(document,"source"))
        .insert("source_md5",PKEdataset::rapid_to_val<std::string>(document,"source_md5"))
        .insert("source_path",PKEdataset::rapid_to_val<std::string>(document,"source_path"))
        .insert("rel_source_path",PKEdataset::rapid_to_val<std::string>(document,"rel_source_path"))
        .insert("info",PKEdataset::rapid_to_val<std::string>(document,"info"))
        .insert("description",PKEdataset::rapid_to_val<std::vector<std::string>>(document,"description"))
        .insert("width",PKEdataset::rapid_to_val<int>(document,"width"))
        .insert("height",PKEdataset::rapid_to_val<int>(document,"height"))
        .insert("mask_path",PKEdataset::rapid_to_val<std::string>(document,"mask_path"));

        if (document.HasMember("meta"))
        {
            auto & dimgs = document["meta"];
            for (rapidjson::SizeType i = 0; i < dimgs.Size(); i++) //data + list of images
            {
                auto image_name = PKEdataset::rapid_to_val<std::string>(dimgs[i],"frame");
                auto imgid = image_name;


                std::cout << "frame " << image_name << std::endl;
                pke_dataset(imgid)
                .set_level_name("objects")
                .insert("frame",image_name)
                .insert("width",PKEdataset::rapid_to_val<int>(dimgs[i],"width"))
                .insert("height",PKEdataset::rapid_to_val<int>(dimgs[i],"height"))
                .insert("mask",PKEdataset::rapid_to_val<std::string>(dimgs[i],"mask"))
                .insert("description",PKEdataset::rapid_to_val<std::vector<std::string>>(dimgs[i],"description"));

                std::cout << image_name << " " <<  imgid << "has objects " <<  dimgs[i].HasMember("objects") << " " << dimgs[i].HasMember("test") << std::endl;
                if (dimgs[i].HasMember("objects"))
                {

                    auto & dobjs = dimgs[i]["objects"];
                    for (rapidjson::SizeType o = 0; o < dobjs.Size(); o++) //data + list of objects
                    {
                        std::string objid;
                        if (dobjs[o].HasMember("id"))
                        {
                            objid = PKEdataset::rapid_to_val<std::string>(dobjs[o],"id");
                        }
                        else
                            objid = std::to_string(o);
                        std::cout << "  objid " << objid << std::endl;
                        std::cout << "objid__  " << objid << " " << dobjs[o].HasMember("id") << std::endl;
                        pke_dataset(imgid,objid).set_level_name("objects")
                        .insert("id",std::string(objid))
                        .insert("bb",PKEdataset::rapid_to_val<std::vector<float>>(dobjs[o],"bb"))
                        .insert("polygon",PKEdataset::rapid_to_val<std::vector<std::vector<float>>>(dobjs[o],"polygon"))
                        .insert("object_class",PKEdataset::rapid_to_val<int>(dobjs[o],"object_class"));
                        if (dobjs[o].HasMember("box_type"))
                        {
                            pke_dataset(imgid,objid)
                            .insert("box_type",PKEdataset::rapid_to_val<int>(dobjs[o],"box_type"));
                        }
                    }
                }
            }
        }
    }

    inline void deserialize_pke_image_dataset(PKEdataset::Dataset_data_type & pke_dataset, std::string const & idx, std::string const & file_name)
    {
        FILE* fp = fopen(file_name.c_str(), "r"); // non-Windows use "r"
        char readBuffer[65536];
        rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));
        rapidjson::Document d;
        d.ParseStream(is);

        std::cout << "deserialize_pke_image_dataset parsed "  << file_name << std::endl;

        pke_dataset.set_level_name("sequences");
        if (d.IsArray())
        {
            std::cout << "deserialize_pke_image_dataset " << d.Size() << std::endl;
            for (rapidjson::SizeType s = 0; s < d.Size(); s++) //list of sequences
            {
                auto seqid = idx + "_" + std::to_string(s); //one json file may have more than one sequence and there may be different json files
                deserialize_image_sequence(pke_dataset(seqid),d[s]);
            }
        }
        else
        {
            deserialize_image_sequence(pke_dataset("0"),d);
        }
        std::cout << "parse done " << std::endl;
        fclose(fp);

    }
    inline void deserialize_trajectory_sequence(PKEdataset::Dataset_data_type & pke_dataset,
                                            rapidjson::Value & document)
    {
        pke_dataset .set_level_name("meta")
            .insert("name",PKEdataset::rapid_to_val<std::string>(document,"name"))
            .insert("source",PKEdataset::rapid_to_val<std::string>(document,"source"))
            .insert("source_md5",PKEdataset::rapid_to_val<std::string>(document,"source_md5"))
            .insert("source_path",PKEdataset::rapid_to_val<std::string>(document,"source_path"))
            .insert("rel_source_path",PKEdataset::rapid_to_val<std::string>(document,"rel_source_path"))
            .insert("info",PKEdataset::rapid_to_val<std::string>(document,"info"))
            .insert("description",PKEdataset::rapid_to_val<std::vector<std::string>>(document,"description"))
            .insert("width",PKEdataset::rapid_to_val<int>(document,"width"))
            .insert("height",PKEdataset::rapid_to_val<int>(document,"height"))
            .insert("reference_image",PKEdataset::rapid_to_val<int>(document,"reference_image"));

        auto & dtra = document["meta"];
        for (rapidjson::SizeType i = 0; i < dtra.Size(); i++) //data + list of images
        {
            auto traj_id = PKEdataset::rapid_to_val<std::string>(dtra[i],"id");



            pke_dataset(traj_id)
            .set_level_name("observations")
            .insert("id",PKEdataset::rapid_to_val<std::string>(dtra[i],"id"))
            . insert("object_class",PKEdataset::rapid_to_val<int>(dtra[i],"object_class"))
            .insert("reference_observation_idx",PKEdataset::rapid_to_val<int>(dtra[i],"reference_observation_idx"))
            .insert("level",PKEdataset::rapid_to_val<std::string>(dtra[i],"level"))
            .insert("trajectory_bounding_box",PKEdataset::rapid_to_val<std::vector<int>>(dtra[i],"trajectory_bounding_box"));

            auto & dobs = dtra[i]["observations"];
            for (rapidjson::SizeType o = 0; o < dobs.Size(); o++) //data + list of objects
            {
                std::cout << "PKEdataset::rapid_to_val<int64_t>(dobs[o],timestamp) " << PKEdataset::rapid_to_val<int64_t>(dobs[o],"timestamp") << std::endl;
                auto str_image = PKEdataset::rapid_to_val<std::string>(dobs[o],"frame");
                auto obsid = std::to_string(o);
                pke_dataset(traj_id,str_image)
                .insert("frame",str_image)
                .insert("position_image",PKEdataset::rapid_to_val<std::vector<int>>(dobs[o],"position_image"))
                .insert("position_world",PKEdataset::rapid_to_val<std::vector<float>>(dobs[o],"position_world"))
                .insert("timestamp",PKEdataset::rapid_to_val<int64_t>(dobs[o],"timestamp"))
                .insert("bb",PKEdataset::rapid_to_val<std::vector<float>>(dobs[o],"bb"))
                .insert("speed",PKEdataset::rapid_to_val<float>(dobs[o],"speed"))
                .insert("background",PKEdataset::rapid_to_val<std::string>(dobs[o],"background"));

            }
        }
    }
    inline void deserialize_pke_trajectory_dataset(PKEdataset::Dataset_data_type & pke_dataset, std::string const & idx, std::string const & file_name)
    {
        FILE* fp = fopen(file_name.c_str(), "r"); // non-Windows use "r"
        char readBuffer[65536];
        rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));
        rapidjson::Document d;
        d.ParseStream(is);

        std::cout << "deserialize_pke_trajectory_dataset parsed "  << file_name << std::endl;

        pke_dataset.set_level_name("sequences");
        if (d.IsArray())
        {
            std::cout << "deserialize_pke_trajectory_dataset " << d.Size() << std::endl;
            for (rapidjson::SizeType s = 0; s < d.Size(); s++) //list of sequences
            {
                auto seqid = idx + "_" + std::to_string(s); //one json file may have more than one sequence and there may be different json files
                deserialize_trajectory_sequence(pke_dataset(seqid),d[s]);
            }
        }
        else
        {
            deserialize_trajectory_sequence(pke_dataset("0"),d);
        }
        std::cout << "parse done " << std::endl;
        fclose(fp);


    }

}//PKEdataset
