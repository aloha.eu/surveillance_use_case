from flask import Flask
from celer.tasks import *
from celer.cc import celery
from threading import Thread
from models.models import *
from db.mongo import MongoConnector
from settings import settings

app = Flask(__name__)
settings.get_settings(app)

connection_config = app.config['MONGODB_SETTINGS']

mongo = MongoConnector(app)

def pipeline_simulation(mongo):
    """
    pipeline simulation of previous steps
    :param mongo:
    :return:
    """

    mongo.db.drop_collection('User')
    mongo.db.drop_collection('Dataset')
    mongo.db.drop_collection('Project')
    mongo.db.drop_collection('StatusTE')
    mongo.db.drop_collection('AlgorithmConfiguration')
    mongo.db.drop_collection('ConfigurationFile')

    usr = User()
    usr.email = "dummy@mail.at"
    usr.save()

    #
    prj = Project()
    prj.creator = usr.get_id()
    prj.save()

    dsp = AlgorithmConfiguration()
    dsp.project = prj.get_id()
    dsp.onnx = r"/opt/www/architectures/mnist_V13.onnx"
    dsp.save()

    conf = ConfigurationFile()
    conf.project = prj.get_id()
    conf.data_set_name = 'pkev3'
    conf.mode = 'full'
    conf.data_folder = r"/opt/www/shared_data/pkev3/data_files"
    conf.data_file = r"/opt/www/shared_data/pkev3/data_files/json/test_coco_city.json"
    conf.lr = 0.001
    conf.lr_decay = 0.1
    conf.ref_steps = 4
    conf.ref_patience = 15
    conf.batch_size = 128
    conf.num_epochs = 250
    conf.loss = 'softmax'
    conf.optimizer = 'sgd'
    conf.image_dimensions = [640, 480, 3]
    conf.num_classes = 2
    conf.num_filters = 16
    conf.cross_val = 0
    conf.task_type = 'detection'
    conf.accuracy = 'percent'
    conf.augmentation = {'flip_hor': False,
                         'flip_vert': False}
    conf.data_split = 0.7
    conf.normalize = False
    conf.zero_center = False
    conf.save()

    print("config saved")

    return prj.get_id(), dsp.get_id()

callback_id = 112
projectid, jobid = pipeline_simulation(mongo)
conf = ConfigurationFile.objects.get(project=projectid)
from api.api.training_call import *
from api.engine.network.TrainRunner import TrainRunner

data_saver = DataParser(conf.data_set_name, conf.num_classes, conf.image_dimensions,conf.normalize, conf.zero_center,
                        conf.data_split, conf.cross_val, conf.task_type, conf.mode, conf.project, conf.data_file,
                        conf.data_folder)
